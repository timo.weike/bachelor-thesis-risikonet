/*
 *     bachelorthesis-risikonet
 *     Copyright (C) 2019  Timo Weike
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package de.uni_bielefeld.timo_weike.bachelorarbeit.training;

import org.nd4j.linalg.activations.IActivation;
import org.nd4j.linalg.api.ndarray.INDArray;
import org.nd4j.linalg.lossfunctions.impl.LossMCXENT;

/**
 * CrossEntropyLoss for my use of output masks
 */
public class CrossEntropyLoss extends LossMCXENT {
    @Override
    public INDArray computeGradient(INDArray labels, INDArray preOutput, IActivation activationFn, INDArray mask) {
        // since LossMCXENT does not accept masks that apply a masking for all output individually,
        // this class converters the mask to a per entry mask.
        // We could change how the masks are stored in the Datasets but this would lead to incompatibilities with old
        // Datasets.
        if (!mask.isColumnVectorOrScalar()) {
            mask = mask.getColumn(0);
        }
        return super.computeGradient(labels, preOutput, activationFn, mask);
    }
}
