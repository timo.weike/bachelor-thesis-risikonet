/*
 *     bachelorthesis-risikonet
 *     Copyright (C) 2019  Timo Weike
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package de.uni_bielefeld.timo_weike.bachelorarbeit.util;


import java.util.LinkedList;
import java.util.List;

/**
 * Buffers a specific amount of data.
 *
 * @param <T>
 */
public class Buffer<T> {
    private int count = 0;
    private int maxCount;
    private Node firstNode = null;
    private Node lastNode = null;

    /**
     * Creates a buffer that can hold maxCount element at maximum.
     *
     * @param maxCount
     */
    public Buffer(int maxCount) {
        this.maxCount = maxCount;
    }

    /**
     * Duplicates this buffer.
     * This is a shallow copy, that means only the references are copy but no containing element is copied.
     * @return
     */
    public Buffer<T> dup() {
        Buffer<T> newBuffer = new Buffer<>(maxCount);
        Node tmp = this.firstNode;

        while (tmp != null) {
            newBuffer.push(tmp.value);
            tmp = tmp.next;
        }

        return newBuffer;
    }

    /**
     * Pushes an element inside the buffer.
     * @param element
     */
    public void push(T element) {
        if (maxCount != 0) {
            Node newNode = new Node();
            newNode.value = element;

            if (count >= maxCount) {
                Node oldFirstNote = firstNode;
                firstNode = oldFirstNote.next;
                oldFirstNote.value = null;
                oldFirstNote.next = null;
                count--;
            }

            if (count == 0) {
                firstNode = newNode;
                lastNode = newNode;
            } else {
                lastNode.next = newNode;
                lastNode = newNode;
            }

            count++;
        }
    }

    /**
     * Gets a list of all elements in this buffer.
     * Where at index 0 is the newest element.
     * @return
     */
    public List<T> getElements() {
        List<T> elements = new LinkedList<>();

        if (count > 0) {
            Node tmp = firstNode;
            int i = count - 1;

            while (tmp != null && 0 <= i) {
                elements.add(0, tmp.value);
                tmp = tmp.next;
                i--;
            }
        }

        return elements;
    }

    private class Node {
        T value = null;
        Node next = null;
    }
}
