package de.uni_bielefeld.timo_weike.bachelorarbeit.programs;

import java.io.File;
import java.io.IOException;

import org.apache.commons.io.FilenameUtils;
import org.deeplearning4j.nn.conf.ConvolutionMode;
import org.deeplearning4j.nn.graph.ComputationGraph;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import de.uni_bielefeld.timo_weike.bachelorarbeit.game.GameBoard;
import de.uni_bielefeld.timo_weike.bachelorarbeit.training.NeuralNetBuilder;
import de.uni_bielefeld.timo_weike.bachelorarbeit.util.MyFileUtils;

import static de.uni_bielefeld.timo_weike.bachelorarbeit.game.GlobalGameConfig.getColCount;
import static de.uni_bielefeld.timo_weike.bachelorarbeit.game.GlobalGameConfig.getRowCount;
import static de.uni_bielefeld.timo_weike.bachelorarbeit.training.Utils.*;

public class ThreeTowerModelCreator {
    private static final Logger log = LoggerFactory.getLogger(SingleTowerModelCreator.class);

    public static void main(String[] args) throws IOException {
        int numResidualBlocks = 5;

        NeuralNetBuilder builder = new NeuralNetBuilder(GameBoard.getLayerCount(), getRowCount(),getColCount(), new int[]{5,5}, new int[]{1,1}, ConvolutionMode.Same);
        String input = "in";

        builder.addInputs(input);

        String convOut_value = builder.addConvBatchNormBlock("init", input, GameBoard.getLayerCount(),
                true, "v");
        String towerOut_value = builder.addResidualTower(numResidualBlocks, convOut_value, "v");

        String valueOut = builder.addValueHead(towerOut_value);

        String convOut_attack = builder.addConvBatchNormBlock("init", input, GameBoard.getLayerCount(),
                true, "a");
        String towerOut_attack = builder.addResidualTower(numResidualBlocks, convOut_attack, "a");

        String attackOut = builder.addAttackHead(towerOut_attack);

        String convOut_reinforcement = builder.addConvBatchNormBlock("init", input, GameBoard.getLayerCount(),
                true, "r");
        String towerOut_reinforcement = builder.addResidualTower(numResidualBlocks, convOut_reinforcement, "r");

        String reinforcementOut = builder.addReinforcementHead(towerOut_reinforcement);

        builder.addOutputs(valueOut, attackOut, reinforcementOut);

        ComputationGraph model = new ComputationGraph(builder.buildAndReturn());
        model.init();

        final File file = saveComputationGraph(model);

        String fileName = MyFileUtils.getTimeStampedFile(MODEL_BASE_NAME, BEST_MODEL_EXTENSION);
        String filePath = FilenameUtils.concat(MODELS_DIR, fileName);

        makeLatestBest(new File(filePath), file);
    }
}
