/*
 *     bachelorthesis-risikonet
 *     Copyright (C) 2019  Timo Weike
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package de.uni_bielefeld.timo_weike.bachelorarbeit.game;

import java.util.*;
import java.util.function.IntSupplier;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

import org.apache.commons.lang3.StringUtils;
import org.jetbrains.annotations.NotNull;
import org.nd4j.linalg.api.ndarray.INDArray;
import org.nd4j.linalg.factory.Nd4j;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import de.uni_bielefeld.timo_weike.bachelorarbeit.util.AnsiColors;
import de.uni_bielefeld.timo_weike.bachelorarbeit.util.AnsiFormatter;
import de.uni_bielefeld.timo_weike.bachelorarbeit.util.Buffer;
import de.uni_bielefeld.timo_weike.bachelorarbeit.util.Cache;

import static de.uni_bielefeld.timo_weike.bachelorarbeit.game.GlobalGameConfig.getColCount;
import static de.uni_bielefeld.timo_weike.bachelorarbeit.game.GlobalGameConfig.getRowCount;
import static de.uni_bielefeld.timo_weike.bachelorarbeit.game.IsOverState.of;
import static de.uni_bielefeld.timo_weike.bachelorarbeit.game.IsOverState.over;

/**
 * A state of the GameBoard.
 * This class is immutable and the methods changing the state of the GameBoard return an new instance.
 */
public class GameBoard implements Cloneable {
    private static final Logger log = LoggerFactory.getLogger(GameBoard.class);

    private BoardConfig boardConfig; // 7 layer

    private PlayerArmys currentPlayerArmysState; // 5 layer

    private Random random;

    private Buffer<PlayerArmys> pastArmyStates; // 5 * 4 layers = 20

    private TurnState turnState;

    public static int getLayerCount() {
        return 7 + 5 + 5 * GlobalGameConfig.getBackView();
    }

    public int remainingReinforcementPoints;
    // we can do some caching of value since this class is not publicly mutable
    private Cache<IsOverState> isOverCache = null;
    private Cache<Boolean> currentPlayerCanMakeActionCache = null;
    private Cache<Map<PlayerId, Double>> areaPercentMapCache = null;
    private Cache<Map<PlayerId, Double>> strengthPercentMapCache = null;

    public GameBoard() {
        random = new Random();

        boardConfig = new BoardConfig();
        currentPlayerArmysState = new PlayerArmys();

        pastArmyStates = new Buffer<>(GlobalGameConfig.getBackView());

        IntStream.range(0, GlobalGameConfig.getBackView()).forEach(i -> pastArmyStates.push(new PlayerArmys()));

        this.turnState = new TurnState(PlayerId.FirstPlayer, TurnPhase.Attack);
    }

    private GameBoard(Object a) {
        random = null;
        boardConfig = null;
        currentPlayerArmysState = null;
        pastArmyStates = null;
        turnState = null;
        remainingReinforcementPoints = 0;
    }

    /**
     * Gets the cell for the given position.
     *
     * @param position
     * @return
     */
    public Cell getCell(Position position) {
        return this.currentPlayerArmysState.getValueFor(position);
    }

    /**
     * Calculates the player with the highest relative area.
     *
     * @return
     */
    public PlayerId getHighestArea() {
        return this.getAreaPercentMap()
                       .entrySet()
                       .stream()
                       .max(Comparator.comparingDouble(t -> t.getValue()))
                       .map(t -> t.getKey())
                       .orElse(null);
    }

    /**
     * Calculates the relative area for each player.
     *
     * @return a mapping between PlayerId and the relative area
     */
    private Map<PlayerId, Double> getAreaPercentMap() {
        if (this.areaPercentMapCache == null) {
            this.areaPercentMapCache = new Cache<>(() -> {
                Map<PlayerId, Double> map = new HashMap<>();
                Stream.of(PlayerId.FirstPlayer, PlayerId.SecondPlayer, PlayerId.ThirdPlayer, PlayerId.FourthPlayer)
                        .forEach(pid -> map.put(pid, currentPlayerArmysState.getAreaPercentFor(pid)));
                return map;
            });
        }

        return this.areaPercentMapCache.get();
    }

    /**
     * Gets the relative area for the given player.
     * @param playerId
     * @return
     */
    public double getAreaPercentFor(PlayerId playerId) {
        return this.getAreaPercentMap().get(playerId);
    }

    public boolean canMove(Position position, MoveDirection direction) {
        return this.boardConfig.canMove(position, direction);
    }

    /**
     * Creates a wrapper that holds the information if the game is over and if is is which player won the game.
     * @return
     */
    public IsOverState isOver() {
        if (isOverCache == null) {
            isOverCache = new Cache<>(() -> {
                boolean p1Alive = currentPlayerArmysState.isPlayerAlive(PlayerId.FirstPlayer);
                boolean p2Alive = currentPlayerArmysState.isPlayerAlive(PlayerId.SecondPlayer);
                boolean p3Alive = currentPlayerArmysState.isPlayerAlive(PlayerId.ThirdPlayer);
                boolean p4Alive = currentPlayerArmysState.isPlayerAlive(PlayerId.FourthPlayer);

                if (p1Alive && !(p2Alive || p3Alive || p4Alive))
                    return over(PlayerId.FirstPlayer);
                else if (p2Alive && !(p1Alive || p3Alive || p4Alive))
                    return over(PlayerId.SecondPlayer);
                else if (p3Alive && !(p1Alive || p2Alive || p4Alive))
                    return over(PlayerId.ThirdPlayer);
                else if (p4Alive && !(p2Alive || p3Alive || p1Alive))
                    return over(PlayerId.FourthPlayer);
                else {
                    final PlayerId playerId = this.getAreaPercentMap().entrySet().stream()
                                                      .max(Comparator.comparingDouble(t -> t.getValue()))
                                                      .map(t -> t.getKey())
                                                      .orElse(PlayerId.Neutral);
                    return of(false, playerId);
                }
            });
        }

        return isOverCache.get();
    }

    /**
     * Initializes the GameBoard.
     */
    public void init() {
        initFilledRandomDistType();
    }

    private void initFilledRandomDistType() {
        FillWithRandom(() -> random.nextInt(PlayerId.values().length));
    }

    private void FillWithRandom(IntSupplier generator) {
        for (int row = 0; row < getRowCount(); row++) {
            for (int col = 0; col < getColCount(); col++) {
                Position position = new Position(row, col);

                if (boardConfig.isCellPresent(position)) {
                    int i = generator.getAsInt();

                    currentPlayerArmysState.setValueForPlayerAt(random.nextInt(9) + 1, PlayerId.values()[i], position);
                }
            }
        }
    }

    /**
     * Performes the given Attack-Action on this GameBoard and returns a AttackResult.
     * @param attack the attack to perform
     * @return
     */
    public AttackResult makeAttackFor(@NotNull IAttack attack) {
        if (canDo(attack)) {
            GameBoard newBoard = this.dup();
            boolean result = newBoard.internalMakeAttackFor(attack);

            return new AttackResult(result, newBoard);
        } else {
            throw new IllegalStateException(String.format("Player %d can not make this attack: %s", getCurrentPlayer().ordinal(), attack));
        }
    }

    /**
     * Determines if the given attack can be performed on this GameBoard.
     *
     * @param attack
     * @return
     */
    public boolean canDo(IAttack attack) {
        PlayerId attacker = getCurrentPlayer();

        if (attack.isPhaseEnding()) {
            return true;
        } else {
            Position neighbor = attack.getAttackedPosition();

            if (this.boardConfig.isCellPresent(neighbor) && boardConfig.canMove(attack.getPosition(), attack.getAttackDirection())) {
                Cell attackingCell = currentPlayerArmysState.getValueFor(attack.getPosition());
                Cell attackedCell = currentPlayerArmysState.getValueFor(neighbor);

                if (attackingCell.getValue() >= 2) {
                    final PlayerId attackedPlayer = attackedCell.getOwner();
                    final PlayerId owner = attackingCell.getOwner();

                    return attacker == owner && attackedPlayer != attacker;
                } else {
                    return false;
                }
            } else {
                return false;
            }
        }
    }

    /**
     * Deletes the cached values.
     */
    private void invalidateCache() {
        this.currentPlayerCanMakeActionCache = null;
        this.isOverCache = null;
        this.areaPercentMapCache = null;
        this.strengthPercentMapCache = null;
    }

    /**
     * Calcualtes a list for the possible Attack-Actions.
     *
     * @return
     */
    public List<IAttack> possibleAttacks() {
        List<IAttack> maybePossibleAttacks = this.currentPlayerArmysState.possibleAttacksFor(getCurrentPlayer());

        return maybePossibleAttacks.stream()
                       .filter(a -> canDo(a))
                       .collect(Collectors.toList());
    }

    /**
     * Performes the given Reinforcement-Action and return the resulting GameBoard.
     *
     * @param reinforcement
     * @return
     */
    public GameBoard makeReinforcement(IReinforcement reinforcement) {
        GameBoard newBoard = this.dup();

        newBoard.internalMakeReinforcement(reinforcement);

        return newBoard;
    }

    private void internalMakeReinforcement(IReinforcement reinforcement) {
        PlayerId playerId = getCurrentPlayer();
        pastArmyStates.push(currentPlayerArmysState);
        currentPlayerArmysState = currentPlayerArmysState.dup();

        if (!reinforcement.isPhaseEnding()) {
            Cell c = this.currentPlayerArmysState.getValueFor(reinforcement.getPosition());

            if (!this.canPerform(reinforcement)) {
                log.info(String.format("Player to play %s and cell is owned by %s%n", this.getTurnState().getPlayerId(), c.getOwner()));
                log.info("FirstPlayer \n" + this.currentPlayerArmysState.printFor(PlayerId.FirstPlayer));
                log.info("SecondPlayer \n" + this.currentPlayerArmysState.printFor(PlayerId.SecondPlayer));
                log.info("ThirdPlayer \n" + this.currentPlayerArmysState.printFor(PlayerId.ThirdPlayer));
                log.info("FourthPlayer \n" + this.currentPlayerArmysState.printFor(PlayerId.FourthPlayer));
                System.out.println(this.print(reinforcement.getPosition(), true));
                throw new IllegalStateException(String.format("The reinforcement action [%s] is not possible", reinforcement));
            }

            if (c.getOwner() != playerId) {
                log.info(String.format("Player %s tries to reinforce a cell that's is owned by %s%n", playerId, c.getOwner()));
                log.info((String.format("%s%n%s", playerId, this.currentPlayerArmysState.printFor(playerId))));
                log.info((String.format("%s%n%s", c.getOwner(), this.currentPlayerArmysState.printFor(c.getOwner()))));
                System.out.println(this.print(reinforcement.getPosition(), true));
            }

            this.currentPlayerArmysState.setValueForPlayerAt(c.getValue() + 1, playerId, reinforcement.getPosition());

            this.remainingReinforcementPoints--;
        } else {
            this.reinforceRandomForPlayer(playerId);
        }

        this.forwardTurn(reinforcement.isPhaseEnding());
    }

    private GameBoard dup() {
        GameBoard newBoard = new GameBoard(null);

        newBoard.boardConfig = this.boardConfig;
        newBoard.currentPlayerArmysState = this.currentPlayerArmysState;
        newBoard.random = this.random;
        newBoard.pastArmyStates = this.pastArmyStates.dup();
        newBoard.turnState = this.turnState.dup();
        newBoard.remainingReinforcementPoints = this.remainingReinforcementPoints;

        return newBoard;
    }

    private boolean internalMakeAttackFor(@NotNull IAttack attack) {
        pastArmyStates.push(currentPlayerArmysState);
        currentPlayerArmysState = currentPlayerArmysState.dup();

        PlayerId attacker = getCurrentPlayer();

        boolean result = true;

        if (!attack.isPhaseEnding()) {

            Position neighbor = attack.getAttackedPosition();

            Cell attackingCell = currentPlayerArmysState.getValueFor(attack.getPosition());
            Cell defendingCell = currentPlayerArmysState.getValueFor(neighbor);

            PlayerId defendingPlayer = defendingCell.getOwner();

            int attackerValue = attackingCell.getValue() - 1; // m - 1
            int defendingValue = defendingCell.getValue();    // n

            int newDefenderValue;

            boolean wasTakenOver = false;

            int diff = attackerValue - defendingValue;

            if (diff >= 1) // attacker hat 2 oder mehr, mehr Value
            { // 100% gewinn chance
                newDefenderValue = diff;
                wasTakenOver = true;
            } else if (diff == 0) // attacker hat 1 mehr als verteidiger
            { //  75% gewinn chance
                wasTakenOver = 0 != random.nextInt(4);
                newDefenderValue = 1;
            } else if (diff == -1) { //  50% gewinn chance
                newDefenderValue = 1;
                wasTakenOver = 0 == random.nextInt(2);
            } else if (diff == -2) { //  25% gewinn chance
                wasTakenOver = 0 == random.nextInt(4);
                newDefenderValue = wasTakenOver ? 1 : 2;
            } else {//   0% gewinn chance
//            wasTakenOver = false;
                newDefenderValue = -diff;
            }

            currentPlayerArmysState.setValueForPlayerAt(1, attacker, attack.getPosition());

            if (wasTakenOver) {
                currentPlayerArmysState.setValueForPlayerAt(0, defendingPlayer, neighbor);
                currentPlayerArmysState.setValueForPlayerAt(newDefenderValue, attacker, neighbor);
                result = true;
            } else {
                currentPlayerArmysState.setValueForPlayerAt(newDefenderValue, defendingPlayer, neighbor);
                result = false;
            }
        }

        this.forwardTurn(attack.isPhaseEnding());

        return result;
    }

    private void forwardTurn(boolean phaseEnding) {
        if (phaseEnding) {
            forwardTurnInternal();
        }

        while (!this.currentPlayerCanMakeAction()) {
            forwardTurnInternal();
        }
    }

    private void forwardTurnInternal() {
        this.turnState.forward();

        switch (this.turnState.getTurnPhase()) {
            case Attack:
                this.remainingReinforcementPoints = 0;
                break;
            case Reinforce:
                this.remainingReinforcementPoints = this.reinforcementPoints();
                break;
        }

        this.invalidateCache();
    }

    /**
     * Indicates if the given Reinforcement-Action can be performed on this state of the game.
     *
     * @param reinforcement
     * @return true if the given action can be performed
     */
    public boolean canPerform(IReinforcement reinforcement) {
        return this.currentPlayerArmysState.canPlayerReinforce(getCurrentPlayer(), reinforcement);
    }

    /**
     * Gets the state of the turn.
     *
     * @return
     */
    public TurnState getTurnState() {
        return turnState.dup();
    }

    /**
     * Creates a printable string representation of the GameBoard.
     * A position can only be highlighted is useColor is set to true.
     *
     * @param highlightPos position to highlight
     * @param useColor if ansi-colors should be used
     * @return
     */
    public String print(Position highlightPos, boolean useColor) {
        FormatterFunc formatterFunc;

        if (useColor) {
            formatterFunc = AnsiFormatter::Format;
        } else {
            formatterFunc = (str, colors) -> str;
        }

        String[] lines = new String[getRowCount() * 2];
        IntStream x = IntStream.range(0, getColCount());
        lines[0] = "   " + StringUtils.join(x.boxed().map(i -> String.format("%2d", i)).toArray(String[]::new), "   ");

        for (int i = 1; i < lines.length; i += 2) {
            int row = (i - 1) / 2;
            StringBuilder strB = new StringBuilder();
            strB.append(String.format("%2d ", row));

            for (int col = 0; col < getColCount(); col++) {
                Position pos = new Position(row, col);
                Cell c = currentPlayerArmysState.getValueFor(pos);

                if (c != null && boardConfig.isCellPresent(pos)) {
                    switch (c.getOwner()) {
                        case Neutral:
                            strB.append(formatterFunc.Format("n" + c.getValue()));
                            break;
                        case FirstPlayer:
                            strB.append(formatterFunc.Format("a" + c.getValue(), pos.equals(highlightPos) ? AnsiColors.YELLOW_BOLD : AnsiColors.BLUE));
                            break;
                        case SecondPlayer:
                            strB.append(formatterFunc.Format("b" + c.getValue(), pos.equals(highlightPos) ? AnsiColors.YELLOW_BOLD : AnsiColors.RED));
                            break;
                        case ThirdPlayer:
                            strB.append(formatterFunc.Format("c" + c.getValue(), pos.equals(highlightPos) ? AnsiColors.YELLOW_BOLD : AnsiColors.GREEN));
                            break;
                        case FourthPlayer:
                            strB.append(formatterFunc.Format("d" + c.getValue(), pos.equals(highlightPos) ? AnsiColors.YELLOW_BOLD : AnsiColors.MAGENTA_BRIGHT));
                            break;
                    }
                } else
                    strB.append("  ");

                if (boardConfig.canMove(pos, MoveDirection.Right))
                    strB.append(" - ");
                else
                    strB.append("   ");
            }

            lines[i] = strB.toString();
        }

        for (int i = 2; i < lines.length; i += 2) {
            int row = (i - 2) / 2;
            StringBuilder strB = new StringBuilder();
            strB.append("   ");

            for (int col = 0; col < getColCount(); col++) {
                Position pos = new Position(row, col);

                if (boardConfig.canMove(pos, MoveDirection.Bottom))
                    strB.append(" |");
                else
                    strB.append("  ");

                if (boardConfig.canMove(pos, MoveDirection.BottomRight))
                    strB.append(" \\ ");
                else
                    strB.append("   ");
            }

            lines[i] = strB.toString();
        }
        return StringUtils.join(lines, '\n');
    }

    private boolean currentPlayerCanMakeAction() {
        if (this.currentPlayerCanMakeActionCache == null) {
            this.currentPlayerCanMakeActionCache = new Cache<>(() -> {
                switch (this.turnState.getTurnPhase()) {
                    case Attack:
                        return this.canPlayerAttack();
                    case Reinforce:
                        return this.remainingReinforcementPoints > 0;
                }

                throw new IllegalStateException("Control should not be here");
            });
        }

        return this.currentPlayerCanMakeActionCache.get();
    }

    private boolean canPlayerAttack() {
        return currentPlayerArmysState.canPlayerAttack(getCurrentPlayer());
    }

    private int reinforcementPoints() {
        return this.currentPlayerArmysState.reinforcementPointsForPlayer(getCurrentPlayer());
    }

    /**
     * Convertes this GameBoard into an representation from the view of the given player.
     * @param playerId
     * @return
     */
    public INDArray packFor(PlayerId playerId) {
        List<PlayerArmys> pastStates = pastArmyStates.getElements();
        pastStates.add(0, currentPlayerArmysState);

        INDArray[] allStates = new INDArray[pastStates.size()];
        int i = 0;
        for (PlayerArmys a : pastStates) {
            allStates[i] = a.packFor(playerId);
            i++;
        }

        final INDArray stack = Nd4j.concat(0, allStates);

        return Nd4j.concat(0, boardConfig.getRawData(), stack);

    }

    private void reinforceRandomForPlayer(PlayerId playerId) {
        this.currentPlayerArmysState.reinforceRandomForPlayer(playerId, remainingReinforcementPoints);

        remainingReinforcementPoints = 0;
    }

    /**
     * Calculates a Set of possible Reinforcement-Actions.
     * @return
     */
    public Set<IReinforcement> possibleReinforcements() {
        return this.currentPlayerArmysState.possibleReinforcementsFor(getCurrentPlayer());
    }

    /**
     * Gets the relative area for the current player.
     *
     * @return
     */
    public double getAreaPercentForCurrentPlayer() {
        return getAreaPercentMap().get(getCurrentPlayer());
    }

    /**
     * Gets the relative strenght for the current player.
     *
     * @return
     */
    public double getStrengthPercentForCurrentPlayer() {
        return this.getStrengthPercent(getCurrentPlayer());
    }

    /**
     * Gets the relative strength for the given player.
     * @param playerId
     * @return
     */
    public double getStrengthPercent(PlayerId playerId) {
        return this.getStrengthPercentMap().get(playerId);
    }

    /**
     * Calculates the relative strength for each player.
     *
     * @return a mapping between PlayerId and the relative strength
     */
    private Map<PlayerId, Double> getStrengthPercentMap() {
        if (this.strengthPercentMapCache == null) {
            this.strengthPercentMapCache = new Cache<>(() -> {
                Map<PlayerId, Double> map = new HashMap<>();
                Stream.of(PlayerId.FirstPlayer, PlayerId.SecondPlayer, PlayerId.ThirdPlayer, PlayerId.FourthPlayer)
                        .forEach(pid -> map.put(pid, currentPlayerArmysState.getRelativeStrengthFor(pid)));
                return map;
            });
        }

        return this.strengthPercentMapCache.get();
    }

    /*
     _    0    1    2    3
     _ 0 xx - xx - xx - xx
     _    | \  | \  | \  |
     _ 1 xx - xx - xx - xx
     _    | \  | \  | \  |
     _ 2 xx - xx - xx - xx
    */
    private interface FormatterFunc {
        String Format(String str, AnsiColors... colors);
    }

    private PlayerId getCurrentPlayer() {
        return this.turnState.getPlayerId();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        GameBoard gameBoard = (GameBoard) o;

        INDArray packedBoard = this.packFor(this.turnState.getPlayerId());
        INDArray indArray = gameBoard.packFor(gameBoard.turnState.getPlayerId());

        return remainingReinforcementPoints == gameBoard.remainingReinforcementPoints
                       && getTurnState().equals(gameBoard.getTurnState())
                       && packedBoard.equals(indArray);
    }

    @Override
    public int hashCode() {
        return 0;
    }
}
























