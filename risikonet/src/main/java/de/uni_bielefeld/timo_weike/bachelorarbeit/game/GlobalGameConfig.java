/*
 *     bachelorthesis-risikonet
 *     Copyright (C) 2019  Timo Weike
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package de.uni_bielefeld.timo_weike.bachelorarbeit.game;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.Scanner;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * This class manages global configuration for specific aspects of the project.
 * The settings can be changed either by using the setters of by creating an file named "config".
 * The following entries can be used to changed settings:
 * <p>
 * RowCount: d
 * ColCount: d
 * BackView: d
 * AttackMixTerm: f
 * ReinforcementMixTerm: f
 * UseNoneBinaryEvaluation: b
 * c_puct_constant: f
 * SimulationCount: d
 * <p>
 * where d can be any natural number which fits in the range of an java int, b can be true of false
 * and f can be any floating point number which fits in the range of an java double.
 */
public class GlobalGameConfig {
    private static final Logger log = LoggerFactory.getLogger(GlobalGameConfig.class);
    private static final String FILE_NAME = "config";

    private static int rowCount = 10;

    private static int colCount = 10;

    private static int backView = 0;

    private static boolean useNoneBinaryEvaluation = true;

    private static double attackMixTerm = 1.0;

    private static double reinforcementMixTerm = 0.0;

    private static double c_puctConstant = 1.0;

    private static int simulationCount = 100;

    /*
     * Initializes the settings with the content of the config-file.
     */
    static {
        File file = new File(FILE_NAME);

        if (file.exists()) {
            FileReader fileReader;
            try {
                fileReader = new FileReader(file);

                Scanner scanner = new Scanner(fileReader);

                while (scanner.hasNext()) {
                    String line = scanner.nextLine().trim();

                    if (!line.isEmpty()) {
                        String[] split = line.split(": ");
                        String name = split[0].trim();
                        String value = split[1].trim();
                        parse(name, value);
                    }
                }

                fileReader.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        log.info(String.format("RowCount = %d", getRowCount()));
        log.info(String.format("ColCount = %d", getColCount()));
        log.info(String.format("BackView = %d", getBackView()));
        log.info(String.format("AttackMixTerm = %g", getAttackMixTerm()));
        log.info(String.format("ReinforcementMixTerm = %g", getReinforcementMixTerm()));
        log.info(String.format("UseNoneBinaryEvaluation = %b", isUseNoneBinaryEvaluation()));
        log.info(String.format("c_puct_constant = %g", getC_puctConstant()));
        log.info(String.format("SimulationCount = %d", getSimulationCount()));
    }

    public static int getRowCount() {
        return rowCount;
    }

    public static void setRowCount(int rowCount) {
        GlobalGameConfig.rowCount = rowCount;
    }

    public static int getColCount() {
        return colCount;
    }

    public static void setColCount(int colCount) {
        GlobalGameConfig.colCount = colCount;
    }

    public static int getBackView() {
        return backView;
    }

    public static void setBackView(int backView) {
        GlobalGameConfig.backView = backView;
    }

    private static void parse(String name, String value) {
        if ("RowCount".equals(name)) {
            setRowCount(Integer.parseInt(value));
        } else if ("ColCount".equals(name)) {
            setColCount(Integer.parseInt(value));
        } else if ("BackView".equals(name)) {
            setBackView(Integer.parseInt(value));
        } else if ("UseNoneBinaryEvaluation".equals(name)) {
            setUseNoneBinaryEvaluation(Boolean.parseBoolean(value));
        } else if ("AttackMixTerm".equals(name)) {
            setAttackMixTerm(Double.parseDouble(value));
        } else if ("ReinforcementMixTerm".equals(name)) {
            setReinforcementMixTerm(Double.parseDouble(value));
        } else if ("c_puct_constant".equals(name)) {
            c_puctConstant = Double.parseDouble(value);
        } else if ("SimulationCount".equals(name)) {
            simulationCount = Integer.parseInt(value);
        }
    }

    public static boolean isUseNoneBinaryEvaluation() {
        return useNoneBinaryEvaluation;
    }

    public static void setUseNoneBinaryEvaluation(boolean useNoneBinaryEvaluation) {
        GlobalGameConfig.useNoneBinaryEvaluation = useNoneBinaryEvaluation;
    }

    public static double getAttackMixTerm() {
        return attackMixTerm;
    }

    public static void setAttackMixTerm(double attackMixTerm) {
        GlobalGameConfig.attackMixTerm = attackMixTerm;
    }

    public static double getReinforcementMixTerm() {
        return reinforcementMixTerm;
    }

    public static void setReinforcementMixTerm(double reinforcementMixTerm) {
        GlobalGameConfig.reinforcementMixTerm = reinforcementMixTerm;
    }

    public static double getC_puctConstant() {
        return c_puctConstant;
    }

    public static int getSimulationCount() {
        return simulationCount;
    }

    public static void setSimulationCount(int simulationCount) {
        log.info(String.format("SimulationCount was changed to %d", simulationCount));
        GlobalGameConfig.simulationCount = simulationCount;
    }
}
