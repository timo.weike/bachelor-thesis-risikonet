/*
 *     bachelorthesis-risikonet
 *     Copyright (C) 2019  Timo Weike
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package de.uni_bielefeld.timo_weike.bachelorarbeit.game;

import java.util.Objects;

/**
 * Wrapper class for the result of an attack action.
 */
public class AttackResult {
    private final boolean successful;
    private final GameBoard resultingBoard;

    AttackResult(boolean successful, GameBoard resultingBoard) {
        this.successful = successful;
        this.resultingBoard = resultingBoard;
    }

    /**
     * Indicates if the player captured the attacked cell
     *
     * @return return true of the attacking player captured the attacked cell
     */
    public boolean isSuccessful() {
        return successful;
    }

    /**
     * Gets the GameBoard that resulted from the action.
     *
     * @return the resulting GameBoard
     */
    public GameBoard getResultingBoard() {
        return resultingBoard;
    }

    @Override
    public String toString() {
        return "AttackResult{" +
                       "successful=" + successful +
                       ", resultingBoard=" + resultingBoard +
                       '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        AttackResult that = (AttackResult) o;
        return isSuccessful() == that.isSuccessful() &&
                       Objects.equals(getResultingBoard(), that.getResultingBoard());
    }

    @Override
    public int hashCode() {
        return Objects.hash(isSuccessful(), getResultingBoard());
    }
}
