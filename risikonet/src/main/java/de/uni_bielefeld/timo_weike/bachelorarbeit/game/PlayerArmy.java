/*
 *     bachelorthesis-risikonet
 *     Copyright (C) 2019  Timo Weike
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package de.uni_bielefeld.timo_weike.bachelorarbeit.game;

import java.util.*;

import org.nd4j.linalg.api.ndarray.INDArray;
import org.nd4j.linalg.factory.Nd4j;
import org.nd4j.linalg.indexing.conditions.GreaterThanOrEqual;
import org.nd4j.linalg.indexing.conditions.LessThan;

import de.uni_bielefeld.timo_weike.bachelorarbeit.util.Cache;

import static de.uni_bielefeld.timo_weike.bachelorarbeit.game.GlobalGameConfig.getColCount;
import static de.uni_bielefeld.timo_weike.bachelorarbeit.game.GlobalGameConfig.getRowCount;
import static de.uni_bielefeld.timo_weike.bachelorarbeit.util.INDArrayUtil.indexer;

/**
 * This class represents the strength of all the cells a player controls.
 */
public class PlayerArmy {
    private INDArray board;
    private Random random = new Random();

    private Cache<Boolean> isAliveCache = null;
    private Cache<List<IAttack>> calcPossibleMovesCache = null;
    private Cache<Integer> reinforcementPointsCache = null;

    @SuppressWarnings("CopyConstructorMissesField")
    PlayerArmy(PlayerArmy another) {
        board = another.board.dup();
    }

    PlayerArmy() {
        board = Nd4j.create(getRowCount(), getColCount());
    }

    void setValueAt(Position position, int value) {
        throwIfNotInBounds(position);
        int currentValue = getValueFor(position);

        if (currentValue != value) {
            board.putScalar(indexer(position.getRow(), position.getCol()), value);
        }
    }

    private void throwIfNotInBounds(Position position) {
        if (!position.isInBounds()) {
            throw new IllegalArgumentException("This Position is not valid");
        }
    }

    int getValueFor(Position position) {
        throwIfNotInBounds(position);

        return board.getInt(position.getRow(), position.getCol());

    }

    /**
     * Determines if the player of this Army has any cells with strength 1 or greater.
     *
     * @return
     */
    boolean isAlive() {
        if (this.isAliveCache == null) {
            this.isAliveCache = new Cache<>(() -> 0 < board.sumNumber().doubleValue());
        }

        return this.isAliveCache.get();
    }

    boolean canAttack() {
        return calcPossibleMoves().size() > 0;
    }

    List<IAttack> calcPossibleMoves() {
        if (this.calcPossibleMovesCache == null) {
            this.calcPossibleMovesCache = new Cache<>(() -> {
                LinkedList<IAttack> possibleMoves = new LinkedList<>();

                for (int row = 0; row < getRowCount(); row++) {
                    for (int col = 0; col < getColCount(); col++) {
                        Position pos = new Position(row, col);

                        if (getValueFor(pos) >= 2) {
                            for (MoveDirection dir : MoveDirection.values()) {
                                Position neighbor = pos.getNeighborInDirection(dir);
                                if (neighbor.isInBounds() && getValueFor(neighbor) == 0) {
                                    possibleMoves.add(Action.getAttack(pos, dir));
                                }
                            }
                        }
                    }
                }

                if (possibleMoves.size() > 0) {
                    possibleMoves.add(Action.getPassAttack());
                }

                return possibleMoves;
            });
        }

        return this.calcPossibleMovesCache.get();
    }

    /**
     * Calculates the amount of reinforcement points the player gets at the beginning of the reinforcement-phase.
     *
     * @return
     */
    int reinforcementPoints() {
        if (this.reinforcementPointsCache == null) {
            this.reinforcementPointsCache = new Cache<>(() -> board.cond(new GreaterThanOrEqual(1)).sumNumber().intValue());
        }
        return this.reinforcementPointsCache.get();
    }

    /**
     * Randomly (but unformly) reinforces cells with the given reinforcement points.
     *
     * @param reinforcementPoints
     */
    void reinforceRandom(int reinforcementPoints) {
        if (reinforcementPoints <= 0)
            return;

        INDArray possiblePos = possibleReinforcementsRaw();

        int possiblePosCount = possiblePos.sumNumber().intValue();

        if (possiblePosCount == 0)
            return;

        if (possiblePosCount > reinforcementPoints) {
            while (possiblePosCount > reinforcementPoints) {
                final int r = random.nextInt(getRowCount());
                final int c = random.nextInt(getColCount());
                if (possiblePos.getInt(indexer(r, c)) == 1) {
                    possiblePos.putScalar(indexer(r, c), 0);
                    possiblePosCount--;
                }
            }

            board.addi(possiblePos);
        } else if (possiblePosCount < reinforcementPoints) {
            board.addi(possiblePos);
            reinforceRandom(reinforcementPoints - possiblePosCount);
        } else {
            board.addi(possiblePos);
        }
    }

    INDArray getRawData() {
        return board.dup();
    }

    Set<IReinforcement> possibleReinforcements() {
        Set<IReinforcement> reinforcements = new HashSet<>();

        reinforcements.add(Action.getPassReinforcement());

        for (int row = 0; row < getRowCount(); row++) {
            for (int col = 0; col < getColCount(); col++) {
                IReinforcement reinforcement = Action.getReinforcement(new Position(row, col));
                if (canReinforce(reinforcement)) {
                    reinforcements.add(reinforcement);
                }
            }
        }

        return reinforcements;
    }

    private INDArray possibleReinforcementsRaw() {
        INDArray a = board.cond(new LessThan(9));
        INDArray b = board.cond(new GreaterThanOrEqual(1));
        return a.muli(b);
    }

    private boolean canReinforce(IReinforcement reinforcement) {
        if (!reinforcement.isPhaseEnding())
            return this.getValueFor(reinforcement.getPosition()) > 0 && this.getValueFor(reinforcement.getPosition()) < 9;
        else
            return true;
    }

    /**
     * Calculates the sum of the strength of all cells.
     *
     * @return
     */
    int getTotalStrength() {
        return board.sumNumber().intValue();
    }

    @Override
    public String toString() {
        return this.board.toString();
    }
}
