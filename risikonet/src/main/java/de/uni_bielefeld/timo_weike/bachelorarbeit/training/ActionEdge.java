/*
 *     bachelorthesis-risikonet
 *     Copyright (C) 2019  Timo Weike
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package de.uni_bielefeld.timo_weike.bachelorarbeit.training;

import static de.uni_bielefeld.timo_weike.bachelorarbeit.util.Numbers.oneOr;

/**
 * The edge of the search-tree for a specific action.
 */
class ActionEdge {
    private final double priorProbability;
    private SimulationTree childForTrue;
    private SimulationTree childForFalse;

    // are calculated
    private int visitCount;
    private double totalActionValue;
    private double meanActionValue;

    ActionEdge(double priorProbability) {
        this.priorProbability = priorProbability;
        visitCount = 0;
        totalActionValue = 0;
        meanActionValue = 0;
    }

    private void reCalcStats() {
        synchronized (this) {
            int newVisitCount = 0;
            double newTotalActionValue = 0.0;

            if (childForTrue != null) {
                //noinspection SynchronizeOnNonFinalField
                synchronized (childForTrue) {
                    newVisitCount += childForTrue.getVisitCount();
                    newTotalActionValue += childForTrue.getTotalActionValue();
                }
            }
            if (childForFalse != null) {
                //noinspection SynchronizeOnNonFinalField
                synchronized (childForFalse) {
                    newVisitCount += childForFalse.getVisitCount();
                    newTotalActionValue += childForFalse.getTotalActionValue();
                }
            }

            visitCount = newVisitCount;
            totalActionValue = newTotalActionValue;
            meanActionValue = totalActionValue / oneOr(visitCount);
        }
    }

    synchronized SimulationTree getChildTreeFor(boolean result) {
        if (result) {
            return childForTrue;
        } else {
            return childForFalse;
        }
    }

    synchronized void addChildTree(boolean result, SimulationTree newTree) {
        if (result) {
            childForTrue = newTree;
        } else {
            childForFalse = newTree;
        }
    }

    synchronized double getPriorProbability() {
        return priorProbability;
    }
    synchronized int getVisitCount() {
        reCalcStats();
        return visitCount;
    }

    synchronized double getMeanActionValue() {
        reCalcStats();
        return meanActionValue;
    }
}
