package de.uni_bielefeld.timo_weike.bachelorarbeit.programs;

import java.io.File;
import java.io.IOException;

import org.apache.commons.io.FilenameUtils;
import org.deeplearning4j.nn.conf.ConvolutionMode;
import org.deeplearning4j.nn.graph.ComputationGraph;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import de.uni_bielefeld.timo_weike.bachelorarbeit.game.GameBoard;
import de.uni_bielefeld.timo_weike.bachelorarbeit.training.NeuralNetBuilder;
import de.uni_bielefeld.timo_weike.bachelorarbeit.util.MyFileUtils;

import static de.uni_bielefeld.timo_weike.bachelorarbeit.game.GlobalGameConfig.getColCount;
import static de.uni_bielefeld.timo_weike.bachelorarbeit.game.GlobalGameConfig.getRowCount;
import static de.uni_bielefeld.timo_weike.bachelorarbeit.training.Utils.*;

public class SingleTowerModelCreator {
    private static final Logger log = LoggerFactory.getLogger(SingleTowerModelCreator.class);

    public static void main(String[] args) throws IOException {
        int numResidualBlocks = 15;

        NeuralNetBuilder builder = new NeuralNetBuilder(GameBoard.getLayerCount(), getRowCount(),getColCount(), new int[]{5,5}, new int[]{1,1}, ConvolutionMode.Same);
        String input = "in";

        builder.addInputs(input);

        String initBlock = "init";
        String convOut = builder.addConvBatchNormBlock(initBlock, input, GameBoard.getLayerCount(), true,
                "n");
        String towerOut = builder.addResidualTower(numResidualBlocks, convOut, "n");
        String valueOut = builder.addValueHead(towerOut);
        String attackOut = builder.addAttackHead(towerOut);
        String reinforcementOut = builder.addReinforcementHead(towerOut);

        builder.addOutputs(valueOut, attackOut, reinforcementOut);

        ComputationGraph model = new ComputationGraph(builder.buildAndReturn());
        model.init();

        final File file = saveComputationGraph(model);

        String fileName = MyFileUtils.getTimeStampedFile(MODEL_BASE_NAME, BEST_MODEL_EXTENSION);
        String filePath = FilenameUtils.concat(MODELS_DIR, fileName);

        makeLatestBest(new File(filePath), file);
    }
}
