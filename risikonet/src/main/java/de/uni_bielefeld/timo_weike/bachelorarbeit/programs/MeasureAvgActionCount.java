package de.uni_bielefeld.timo_weike.bachelorarbeit.programs;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.util.LinkedList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import de.uni_bielefeld.timo_weike.bachelorarbeit.game.GameBoard;
import de.uni_bielefeld.timo_weike.bachelorarbeit.game.PlayerId;
import de.uni_bielefeld.timo_weike.bachelorarbeit.training.AiPlayer;
import de.uni_bielefeld.timo_weike.bachelorarbeit.training.DesignedEvaluator;
import de.uni_bielefeld.timo_weike.bachelorarbeit.training.TrainingGame;
import de.uni_bielefeld.timo_weike.bachelorarbeit.util.MyFileUtils;

public class MeasureAvgActionCount {
    private static final Logger log = LoggerFactory.getLogger(MeasureAvgActionCount.class);

    private final static int GAME_COUNT = 10000;

    public static void main(String[] args) {
        try {
            DesignedEvaluator designedEvaluator = new DesignedEvaluator();

            long totalActionCount = 0;
            List<Long> actionCounts = new LinkedList<>();
            for (int i = 0; i < GAME_COUNT; i++) {
                long actionCount = playGame(designedEvaluator, i);
                actionCounts.add(actionCount);
                totalActionCount += actionCount;
            }

            String fileName = MyFileUtils.getTimeStampedFile("actionCountData", ".dat");
            try (BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(fileName))) {
                for (long l : actionCounts) {
                    bufferedWriter.write(l + "\n");
                }
                bufferedWriter.flush();
            }
            log.info(String.format("Wrote data to %s", fileName));

            double avgActionCount = totalActionCount / ((double)GAME_COUNT);

            log.info(String.format("RES:Avg action count per game: %g", avgActionCount));
        } catch (Exception e) {
            e.printStackTrace();
        }

        System.exit(0);
    }

    private static long playGame(DesignedEvaluator designedEvaluator, int gameNumber) {

        AiPlayer p1 = new AiPlayer(PlayerId.FirstPlayer, designedEvaluator);
        AiPlayer p2 = new AiPlayer(PlayerId.SecondPlayer, designedEvaluator);
        AiPlayer p3 = new AiPlayer(PlayerId.ThirdPlayer, designedEvaluator);
        AiPlayer p4 = new AiPlayer(PlayerId.FourthPlayer, designedEvaluator);
        GameBoard gb = new GameBoard();

        gb.init();

        TrainingGame tg = new TrainingGame(p1, p2, p3, p4, gb);

        tg.measurePlay(null, gameNumber);

        return tg.getActionCount();
    }
}
