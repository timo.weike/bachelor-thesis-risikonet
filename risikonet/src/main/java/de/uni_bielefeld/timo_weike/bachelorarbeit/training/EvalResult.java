/*
 *     bachelorthesis-risikonet
 *     Copyright (C) 2019  Timo Weike
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package de.uni_bielefeld.timo_weike.bachelorarbeit.training;

import java.util.Set;

import de.uni_bielefeld.timo_weike.bachelorarbeit.game.IAction;

/**
 * Wrapper class for the result of an evaluation of an IEvaluator
 *
 * @param <T> the type of Actions
 */
public class EvalResult<T extends IAction> {
    private Set<ActionProbability<T>> actionProbs;
    private double actionValue;

    public EvalResult(Set<ActionProbability<T>> actionProbs, double actionValue) {
        this.actionProbs = actionProbs;
        this.actionValue = actionValue;
    }

    public Set<ActionProbability<T>> getActionProbabilitySet() {
        return actionProbs;
    }

    public double getActionValue() {
        return actionValue;
    }
}
