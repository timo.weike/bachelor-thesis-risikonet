/*
 *     bachelorthesis-risikonet
 *     Copyright (C) 2019  Timo Weike
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package de.uni_bielefeld.timo_weike.bachelorarbeit.training;

import java.util.HashMap;

import org.nd4j.linalg.primitives.ImmutableTriple;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import de.uni_bielefeld.timo_weike.bachelorarbeit.game.*;

public class TrainingGame {
    public static final int MAX_TURN_COUNT = 250;
    private static final Logger log = LoggerFactory.getLogger(TrainingGame.class);
    private final boolean GAME_OUTPUT = false;

    private final HashMap<PlayerId, AiPlayer> players;
    private GameBoard board;
    private long actionCount = 0;

    public TrainingGame(AiPlayer p1, AiPlayer p2, AiPlayer p3, AiPlayer p4, GameBoard board) {
        this.board = board;

        players = new HashMap<>();
        players.put(PlayerId.FirstPlayer, p1);
        players.put(PlayerId.SecondPlayer, p2);
        players.put(PlayerId.ThirdPlayer, p3);
        players.put(PlayerId.FourthPlayer, p4);
    }

    public PlayerId play(int gameNumber) {
        mainPlayLoop(null, gameNumber);

        return board.isOver().getWinner();
    }

    private void mainPlayLoop(SimulationGame simulationGame, int gameNumber) {
        if (GAME_OUTPUT) {
            System.out.printf("%s%n%n", board.print(null, true));
        }

        boolean tauChanged = false;
        int lastPrinted = -1;

        TurnState turnState = board.getTurnState();
        int turnCount = turnState.getTurnCount();
        while (!board.isOver().isOver()) {
            if (!(turnCount < MAX_TURN_COUNT)) break;
            AiPlayer activePlayer = players.get(turnState.getPlayerId());

            switch (turnState.getTurnPhase()) {
                case Attack:
                    IAttack attack = activePlayer.nextMove(board);

                    if (!board.canDo(attack)) {
                        log.info(String.format("Player %d tried to make the invalid turn %s", activePlayer.getPlayerId().ordinal(), attack));
                        log.info("\n" + board.print(attack.getPosition(), true));
                        attack = Action.getPassAttack();
                    }

                    AttackResult boardAndResult = board.makeAttackFor(attack);

//                    GameBoard tmp = board;

                    board = boardAndResult.getResultingBoard();

//                    board.validate(tmp, attack);

                    if (simulationGame != null)
                        simulationGame.followTreeEdgeFor(attack, boardAndResult.isSuccessful());

                    if (GAME_OUTPUT)
                        System.out.printf("%s: %s%n%s%n%n", activePlayer.getPlayerId(), attack, board.print(attack.getPosition(), true));
                    break;
                case Reinforce:
                    IReinforcement reinforcement = activePlayer.whereToReinforce(board);

                    if (!board.canPerform(reinforcement)) {
                        log.info(String.format("Player %d tried to make the invalid turn %s", activePlayer.getPlayerId().ordinal(), reinforcement));
                        log.info("\n" + board.print(reinforcement.getPosition(), true));
                        reinforcement = Action.getPassReinforcement();
                    }


//                    GameBoard tmp2 = board;

                    board = board.makeReinforcement(reinforcement);

//                    board.validate(tmp2, reinforcement);

                    if (simulationGame != null)
                        simulationGame.followTreeEdgeFor(reinforcement, true);

                    if (GAME_OUTPUT)
                        System.out.printf("%s: %s%n%s%n%n", activePlayer.getPlayerId(), reinforcement, board.print(reinforcement.getPosition(), true));
                    break;
            }

            turnState = board.getTurnState();
            turnCount = turnState.getTurnCount();

            if (!tauChanged && turnCount == 30) {
                if (simulationGame != null)
                    simulationGame.setTauToInfinitesimal();
                log.info(String.format("Game %d: Tau was set to infinitesimal", gameNumber));
                tauChanged = true;
            }

            if (turnCount % 25 == 0 && lastPrinted != turnCount) {
                log.info(String.format("Game %d: Finished turn %d%n%s", gameNumber, turnCount, board.print(null, true)));
                lastPrinted = turnCount;
            }
            actionCount++;
        }

        if (GAME_OUTPUT) {
            System.out.println(board.print(null, true) + "\n");

            System.out.printf("Game %d: TurnCounter = %d\n", gameNumber, turnCount);
        }
    }

    public ImmutableTriple<Integer, PlayerId, GameBoard> play(SimulationGame simulationGame, int gameNumber) {
        mainPlayLoop(simulationGame, gameNumber);

        this.players.get(PlayerId.FirstPlayer).saveTrainingData(board);
        this.players.get(PlayerId.SecondPlayer).saveTrainingData(board);
        this.players.get(PlayerId.ThirdPlayer).saveTrainingData(board);
        this.players.get(PlayerId.FourthPlayer).saveTrainingData(board);

        return ImmutableTriple.of(board.getTurnState().getTurnCount(), board.isOver().getWinner(), board);
    }

    public long getActionCount() {
        return actionCount;
    }

    public void measurePlay(SimulationGame simulationGame, int gameNumber) {
        mainPlayLoop(simulationGame, gameNumber);
    }
}
