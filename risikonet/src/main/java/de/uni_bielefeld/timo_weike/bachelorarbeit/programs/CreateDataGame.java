package de.uni_bielefeld.timo_weike.bachelorarbeit.programs;

import java.util.concurrent.*;

import org.jetbrains.annotations.NotNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import de.uni_bielefeld.timo_weike.bachelorarbeit.game.GameBoard;
import de.uni_bielefeld.timo_weike.bachelorarbeit.game.PlayerId;
import de.uni_bielefeld.timo_weike.bachelorarbeit.training.AiPlayer;
import de.uni_bielefeld.timo_weike.bachelorarbeit.training.DesignedEvaluator;
import de.uni_bielefeld.timo_weike.bachelorarbeit.training.TrainingGame;
import de.uni_bielefeld.timo_weike.bachelorarbeit.util.DaemonThreadFactory;

public class CreateDataGame {
    private static final Logger log = LoggerFactory.getLogger(CreateDataGame.class);

    private static final int NUM_THREADS = 5;

    private final static int GAME_COUNT = 50;

    public static void main(String[] args) {
        ExecutorService threadPool = null;
        try {
            threadPool = Executors.newFixedThreadPool(NUM_THREADS, DaemonThreadFactory.instance);
            final CompletionService<Void> service = new ExecutorCompletionService<>(threadPool);

            for (int i = 0; i < GAME_COUNT; i++) {
                int finalI = i;
                service.submit(() -> playGame(finalI), null);
            }

            for (int i = 0; i < GAME_COUNT; i++) {
                try {
                    service.take().get();
                } catch (InterruptedException | ExecutionException e) {
                    e.printStackTrace();
                }
            }
        } finally {
            if (threadPool != null)
                threadPool.shutdown();
        }

    }

    @NotNull
    private static void playGame(int gameNumber) {
        DesignedEvaluator designedEvaluator = new DesignedEvaluator();
        AiPlayer p1 = new AiPlayer(PlayerId.FirstPlayer, designedEvaluator);
        AiPlayer p2 = new AiPlayer(PlayerId.SecondPlayer, designedEvaluator);
        AiPlayer p3 = new AiPlayer(PlayerId.ThirdPlayer, designedEvaluator);
        AiPlayer p4 = new AiPlayer(PlayerId.FourthPlayer, designedEvaluator);

        GameBoard gb = new GameBoard();

        gb.init();

        TrainingGame tg = new TrainingGame(p1, p2, p3, p4, gb);

        log.info(String.format("Starting the game %d", gameNumber));
        tg.play(null, gameNumber);
    }
}
