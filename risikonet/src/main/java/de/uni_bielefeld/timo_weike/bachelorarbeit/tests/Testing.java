package de.uni_bielefeld.timo_weike.bachelorarbeit.tests;

import de.uni_bielefeld.timo_weike.bachelorarbeit.game.*;
import de.uni_bielefeld.timo_weike.bachelorarbeit.training.*;

public class Testing {
    private Testing() {
    }

    public static void main(String[] args) {
        HumanChooser humanChooser = new HumanChooser();

        AiPlayer p1 = new AiPlayer(PlayerId.FirstPlayer, humanChooser);
        AiPlayer p2 = new AiPlayer(PlayerId.SecondPlayer, humanChooser);
        AiPlayer p3 = new AiPlayer(PlayerId.ThirdPlayer, humanChooser);
        AiPlayer p4 = new AiPlayer(PlayerId.FourthPlayer, humanChooser);

        GameBoard gameBoard = new GameBoard();

        gameBoard.init();

        TrainingGame trainingGame = new TrainingGame(p1, p2, p3, p4, gameBoard);

        trainingGame.play(0);

    }
}
