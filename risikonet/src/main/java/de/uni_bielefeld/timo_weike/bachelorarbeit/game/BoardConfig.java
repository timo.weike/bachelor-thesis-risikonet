/*
 *     bachelorthesis-risikonet
 *     Copyright (C) 2019  Timo Weike
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package de.uni_bielefeld.timo_weike.bachelorarbeit.game;

import java.util.Random;

import org.nd4j.linalg.api.ndarray.INDArray;
import org.nd4j.linalg.factory.Nd4j;

import static de.uni_bielefeld.timo_weike.bachelorarbeit.game.GlobalGameConfig.getColCount;
import static de.uni_bielefeld.timo_weike.bachelorarbeit.game.GlobalGameConfig.getRowCount;
import static de.uni_bielefeld.timo_weike.bachelorarbeit.util.INDArrayUtil.indexer;

/**
 * Models the configuration of the board.
 * This includes which cells of the graph are present and to which neighbors a cell is connected.
 */
class BoardConfig {
    private static final int CELL_PRESENT_INDICATOR = 1;

    private static final int LAYER_CELL_PRESENT = 0;

    private static final int LAYER_CAN_MOVE_TOP_LEFT = 1;
    private static final int LAYER_CAN_MOVE_TOP = 2;
    private static final int LAYER_CAN_MOVE_LEFT = 3;
    private static final int LAYER_CAN_MOVE_RIGHT = 4;
    private static final int LAYER_CAN_MOVE_BOTTOM = 5;
    private static final int LAYER_CAN_MOVE_BOTTOM_RIGHT = 6;

    private static final int LAYER_COUNT = LAYER_CAN_MOVE_BOTTOM_RIGHT + 1;

    //each layer is m*n
    //  1 2 o
    //  3 x 4
    //  o 5 6
    // is cell x part of board       => n*m
    // is cell x connected to cell 1 => n*m
    // is cell x connected to cell 2 => n*m
    // is cell x connected to cell 3 => n*m
    // is cell x connected to cell 4 => n*m
    // is cell x connected to cell 5 => n*m
    // is cell x connected to cell 6 => n*m
    //
    // => n*m*7
    private INDArray boardConfig;

    BoardConfig() {
        boardConfig = Nd4j.create(LAYER_COUNT, getRowCount(), getColCount());

        initRandom();
    }

    /**
     * Initializes the configuration with at least getRowCount()*getColCount()/2 cells.
     */
    private void initRandom() {
        Position position = new Position(getRowCount()/2, getColCount()/2);
        Random r = new Random();
        int min_count = getRowCount()*getColCount()/2;
        int count = 0;

        while (count < min_count) {
            if (!isCellPresent(position)) {
                setCellAsPresent(position);
                count++;
            }
            while (position.isInBounds()) {

                int i = r.nextInt(6);
                MoveDirection dir = MoveDirection.values()[i];

                Position neighbor = position.getNeighborInDirection(dir);
                if (neighbor.isInBounds()) {

                    connect(position, dir, neighbor);

                    if (!isCellPresent(neighbor)) {
                        count++;
                        setCellAsPresent(neighbor);
                    }
                }

                position = neighbor;
            }

            position = new Position((int)boardConfig.size(1)/2, (int)boardConfig.size(2)/2);
        }
    }

    /**
     * Indicates if the cell at the given position is present.
     *
     * @param position the position
     * @return true if the cell at the given position is present
     */
    boolean isCellPresent(Position position)
    {
        return CELL_PRESENT_INDICATOR == getValueFromBoardConfig(LAYER_CELL_PRESENT, position);
    }

    private void setCellAsPresent(Position position) {
        setValueInBoardConfig(LAYER_CELL_PRESENT, position, CELL_PRESENT_INDICATOR);
    }

    private void connect(Position position, MoveDirection dir, Position neighbor) {
        int toLayerIndex = getLayerIndexForDirection(dir);
        int fromLayerIndex = getLayerIndexForDirection(MoveDirectionUtil.getOpposite(dir));
        setValueInBoardConfig(toLayerIndex, position, CELL_PRESENT_INDICATOR);
        setValueInBoardConfig(fromLayerIndex, neighbor, CELL_PRESENT_INDICATOR);
    }

    private int getValueFromBoardConfig(int layer, Position position)
    {
        return boardConfig.getInt(layer, position.getRow(), position.getCol());
    }

    private void setValueInBoardConfig(int layer, Position position, int value) {
        boardConfig.putScalar(indexer(layer, position.getRow(), position.getCol()), value);
    }

    /**
     * Indicates if the cell at the given position is connected in the given direction.
     *
     * @param position the position of the cell
     * @param direction the direction of connection
     * @return true if the cell at the given position is connected to the neighbor in the given direction
     */
    boolean canMove(Position position, MoveDirection direction) {
        if (!position.isInBounds())
            return false;

        int layer = getLayerIndexForDirection(direction);

        return getValueFromBoardConfig(layer, position) > 0;
    }

    /**
     * Gets the layer index for the given MoveDirection.
     *
     * @param direction the MoveDirection
     * @return the layer-index for the MoveDirection
     */
    public static int getLayerIndexForDirection(MoveDirection direction) {
        switch (direction) {
            case TopLeft: return LAYER_CAN_MOVE_TOP_LEFT;
            case Top: return LAYER_CAN_MOVE_TOP;
            case Left: return LAYER_CAN_MOVE_LEFT;
            case Right: return LAYER_CAN_MOVE_RIGHT;
            case Bottom: return LAYER_CAN_MOVE_BOTTOM;
            case BottomRight: return LAYER_CAN_MOVE_BOTTOM_RIGHT;
        }

        throw new IllegalArgumentException("Not Part of Enum");
    }

    /**
     * Converts this BoardConfig-Object to a raw representation as an IDNArray
     *
     * @return the raw representation of this BoardConfig
     */
    INDArray getRawData() {
        return boardConfig.dup();
    }
}
