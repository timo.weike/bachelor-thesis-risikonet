/*
 *     bachelorthesis-risikonet
 *     Copyright (C) 2019  Timo Weike
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package de.uni_bielefeld.timo_weike.bachelorarbeit.training;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.nd4j.linalg.api.ndarray.INDArray;
import org.nd4j.linalg.api.rng.Random;
import org.nd4j.linalg.factory.Nd4j;

import de.uni_bielefeld.timo_weike.bachelorarbeit.game.GameBoard;
import de.uni_bielefeld.timo_weike.bachelorarbeit.game.IAttack;
import de.uni_bielefeld.timo_weike.bachelorarbeit.game.IReinforcement;
import de.uni_bielefeld.timo_weike.bachelorarbeit.util.INDArrayUtil;

import static de.uni_bielefeld.timo_weike.bachelorarbeit.util.INDArrayUtil.indexer;

public class RandomChooser implements IEvaluator {

    private final Random random;

    public RandomChooser() {
        this.random = Nd4j.getRandom();
    }

    @Override
    public EvalResult<IAttack> evaluateForAttackPhase(GameBoard gameBoard) {
        final List<IAttack> attacks = gameBoard.possibleAttacks();

        INDArray r = random.nextDouble(indexer(attacks.size()));
        INDArrayUtil.normalizei(r);

        Set<ActionProbability<IAttack>> set = new HashSet<>();

        int i = 0;
        for (IAttack attack : attacks) {
            set.add(ActionProbability.of(r.getDouble(i), attack));
            i++;
        }

        double areaPer = gameBoard.getAreaPercentForCurrentPlayer();
        double strengthPer = gameBoard.getStrengthPercentForCurrentPlayer();

        return new EvalResult<>(set, (areaPer + strengthPer)/2.0);
    }

    @Override
    public EvalResult<IReinforcement> evaluateForReinforcementPhase(GameBoard gameBoard) {
        final Set<IReinforcement> reinforcements = gameBoard.possibleReinforcements();

        INDArray r = random.nextDouble(indexer(reinforcements.size()));
        INDArrayUtil.normalizei(r);

        Set<ActionProbability<IReinforcement>> set = new HashSet<>();

        int i = 0;
        for (IReinforcement reinforcement : reinforcements) {
            set.add(ActionProbability.of(r.getDouble(i), reinforcement));
            i++;
        }

        double areaPer = gameBoard.getAreaPercentForCurrentPlayer();
        double strengthPer = gameBoard.getStrengthPercentForCurrentPlayer();

        return new EvalResult<>(set, (areaPer + strengthPer)/2.0);
    }
}
