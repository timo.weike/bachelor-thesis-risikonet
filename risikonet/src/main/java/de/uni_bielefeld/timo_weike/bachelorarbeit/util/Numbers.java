/*
 *     bachelorthesis-risikonet
 *     Copyright (C) 2019  Timo Weike
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package de.uni_bielefeld.timo_weike.bachelorarbeit.util;

/**
 * Uitl-Class for Numbers
 */
public class Numbers {
    private Numbers() {
    }

    /**
     * If value is between lowerBound and upperBound (both inclusive).
     * So just lowerBound <= value && value <= upperBound
     *
     * @param value the number to check
     * @param lowerBound lower bound
     * @param upperBound upper bound
     * @return if value is between the lower and upper bound
     */
    public static boolean inRange(int value, int lowerBound, int upperBound) {
        return lowerBound <= value && value <= upperBound;
    }

    /**
     * Performs a floating point division with the given integer values.
     * a / b
     *
     * @param a nominator
     * @param b denominator
     * @return
     */
    public static double div(int a, int b) {
        return ((double) a) / ((double) b);
    }

    /**
     * Calcs a * t + b * (1.0 -t).
     * But as b + ((a - b) * t)
     * because so there is only one multiplication.
     * @param a a
     * @param b b
     * @param t mixture term
     * @return mix a and b
     */
    public static double mix(double a, double b, double t) {
        return b + ((a - b) * t);
    }

    /**
     * Returns the value 1.0 if the given value is not a finite value or 0.0
     *
     * @param number
     * @return
     */
    public static double oneOr(double number) {
        if (Double.isFinite(number)) {
            if (number == .0) {
                return 1.0;
            } else {
                return number;
            }
        } else {
            return 1.0f;
        }
    }
}
