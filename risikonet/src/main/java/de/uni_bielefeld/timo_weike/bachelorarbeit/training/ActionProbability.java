/*
 *     bachelorthesis-risikonet
 *     Copyright (C) 2019  Timo Weike
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package de.uni_bielefeld.timo_weike.bachelorarbeit.training;

import java.util.Objects;

import de.uni_bielefeld.timo_weike.bachelorarbeit.game.IAction;

/**
 * Wrapper class for the probability of an Action
 *
 * @param <T> the Type of the Action
 */
public class ActionProbability<T extends IAction> {
    private final double probability;
    private final T action;

    private ActionProbability(double probability, T action) {
        this.probability = probability;
        this.action = action;
    }

    public static <T extends IAction> ActionProbability<T> of(double probability, T action) {
        return new ActionProbability<>(probability, action);
    }

    public double getProbability() {
        return probability;
    }

    public T getAction() {
        return action;
    }

    @Override
    public String toString() {
        return "ActionProbability{" +
                       "probability=" + probability +
                       ", action=" + action +
                       '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ActionProbability<?> that = (ActionProbability<?>) o;
        return Double.compare(that.getProbability(), getProbability()) == 0 &&
                       Objects.equals(getAction(), that.getAction());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getProbability(), getAction());
    }
}
