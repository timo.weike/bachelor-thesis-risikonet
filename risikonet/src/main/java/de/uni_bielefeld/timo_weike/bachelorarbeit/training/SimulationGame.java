/*
 *     bachelorthesis-risikonet
 *     Copyright (C) 2019  Timo Weike
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package de.uni_bielefeld.timo_weike.bachelorarbeit.training;

import java.util.Set;
import java.util.concurrent.CompletionService;
import java.util.concurrent.ExecutorCompletionService;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import de.uni_bielefeld.timo_weike.bachelorarbeit.game.*;
import de.uni_bielefeld.timo_weike.bachelorarbeit.util.DaemonThreadFactory;

import static de.uni_bielefeld.timo_weike.bachelorarbeit.game.GlobalGameConfig.getSimulationCount;

/**
 * An Evaluator that uses simulations to produces its results.
 */
public class SimulationGame implements IEvaluator {
    public static final int NUM_THREADS = 16;
    public static final double TAU_INFINITESIMAL = 100.0; // 1/0.001
    public static final double TAU_START = 1.0;
    private SimulationTree simulationTree;
    private IEvaluator evaluator;

    private double tau_inverse = TAU_START;

    private final ExecutorService threadPool = Executors.newFixedThreadPool(NUM_THREADS, DaemonThreadFactory.instance);
    private final CompletionService<Double> service = new ExecutorCompletionService<>(threadPool);

    public SimulationGame(IEvaluator evaluator) {
        this.evaluator = evaluator;

        this.simulationTree = null;
    }

    @Override
    public EvalResult<IAttack> evaluateForAttackPhase(GameBoard gameBoard) {
        final double actionValue = doSimulations(gameBoard);

        Set<ActionProbability<IAttack>> probs = simulationTree.makeAttackProbabilities(tau_inverse);

        return new EvalResult<>(probs, actionValue);
    }

    @Override
    public EvalResult<IReinforcement> evaluateForReinforcementPhase(GameBoard gameBoard) {
        final double actionValue = doSimulations(gameBoard);

        Set<ActionProbability<IReinforcement>> probs = simulationTree.makeReinforcementProbs(tau_inverse);

        return new EvalResult<>(probs, actionValue);
    }

    /**
     * Performes all Simulations.
     *
     * @param gameBoard
     * @return
     */
    private double doSimulations(final GameBoard gameBoard) {
        if (simulationTree == null || !simulationTree.getGameBoard().equals(gameBoard)) {
            this.simulationTree = new SimulationTree(gameBoard, evaluator);
        }

        for (int i = 0; i < getSimulationCount(); i++) {
            service.submit(() -> this.doTreeSearch(this.simulationTree));
        }
        for (int i = 0; i < getSimulationCount(); i++) {
            try {
                service.take();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

        return 0;
    }

    /**
     * Changes the root of the search-tree according to the given action.
     *
     * @param action
     * @param result
     */
    public void followTreeEdgeFor(IAction action, boolean result) {
        if (this.simulationTree != null) {
            ActionEdge child = null;

            Action _action = (Action) action;

            switch (_action.getActionType()) {
                case Attack:
                    child = simulationTree.getChild((IAttack) _action);
                    break;
                case Reinforcement:
                    child = simulationTree.getChild((IReinforcement) _action);
                    break;
            }

            simulationTree = child.getChildTreeFor(result);
        }
    }

    /**
     * Performs one tree-search onton the given tree.
     * @param simulationTree
     * @return
     */
    private double doTreeSearch(SimulationTree simulationTree) {
        double actionValue;
        GameBoard gameBoard = simulationTree.getGameBoard();

        if (!gameBoard.isOver().isOver()) {
            final TurnState turnState = gameBoard.getTurnState();

            ActionEdge actionEdge = null;

            boolean result = true;

            switch (turnState.getTurnPhase()) {
                case Attack:
                    final IAttack nextAttack = simulationTree.getNextAttack();
                    AttackResult boardAndResult = gameBoard.makeAttackFor(nextAttack);
                    gameBoard = boardAndResult.getResultingBoard();
                    result = boardAndResult.isSuccessful();
                    actionEdge = simulationTree.getChild(nextAttack);
                    break;
                case Reinforce:
                    final IReinforcement nextReinforcement = simulationTree.getNextReinforcement();
                    gameBoard = gameBoard.makeReinforcement(nextReinforcement);
                    actionEdge = simulationTree.getChild(nextReinforcement);
                    break;
            }

            SimulationTree childTree = actionEdge.getChildTreeFor(result);

            if (childTree != null) {
                actionValue = doTreeSearch(childTree);
            } else {
                final SimulationTree newTree = new SimulationTree(gameBoard, evaluator);
                actionEdge.addChildTree(result, newTree);
                actionValue = newTree.getTotalActionValue();
            }

            //noinspection SynchronizationOnLocalVariableOrMethodParameter
            synchronized (simulationTree) {
                simulationTree.incrementVisitCount();
                simulationTree.addToTotalActionValue(actionValue);
            }
        } else {
            //noinspection SynchronizationOnLocalVariableOrMethodParameter
            synchronized (simulationTree) {
                actionValue = simulationTree.getTotalActionValue();
            }
        }

        return actionValue;
    }

    public void setTauToInfinitesimal() {
        tau_inverse = TAU_INFINITESIMAL;
    }
}
