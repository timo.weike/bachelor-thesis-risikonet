/*
 *     bachelorthesis-risikonet
 *     Copyright (C) 2019  Timo Weike
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package de.uni_bielefeld.timo_weike.bachelorarbeit.game;

/*
 *  1 2
 *  3 x 4
 *    5 6
 *  1 - TopLeft
 *  2 - Top
 *  3 - Left
 *  4 - Right
 *  5 - 6
 */

import java.util.Objects;

/**
 * A Position on the Gameboard.
 */
public class Position {
    private final int row;
    private final int col;

    public Position(int row, int col) {
        this.row = row;
        this.col = col;
    }

    public static Position of(int row, int col) { return new Position(row, col); }

    public Position getNeighborInDirection(MoveDirection direction) {
        switch (direction) {
            case TopLeft:
                return new Position(getRow() - 1, getCol() - 1);
            case Top:
                return new Position(getRow() - 1, getCol());
            case Left:
                return new Position(getRow(), getCol() - 1);
            case Right:
                return new Position(getRow(), getCol() + 1);
            case Bottom:
                return new Position(getRow() + 1, getCol());
            case BottomRight:
                return new Position(getRow() + 1, getCol() + 1);
        }

        throw new IllegalArgumentException("Not Part of Enum");
    }

    public int getRow() {
        return row;
    }

    public int getCol() {
        return col;
    }

    @Override
    public int hashCode() {
        return Objects.hash(getRow(), getCol());
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Position position = (Position) o;
        return getRow() == position.getRow() &&
                       getCol() == position.getCol();
    }

    @Override
    public String toString() {
        return String.format("{c:%d, r:%d}", getCol(), getRow());
    }

    /**
     * Determines if this position is in the bounds of the Global set Row- and Col-Count.
     *
     * @return
     */
    public boolean isInBounds() {
        return 0 <= this.getRow() && this.getRow() < GlobalGameConfig.getRowCount()
                       && 0 <= this.getCol() && this.getCol() < GlobalGameConfig.getColCount();
    }
}
