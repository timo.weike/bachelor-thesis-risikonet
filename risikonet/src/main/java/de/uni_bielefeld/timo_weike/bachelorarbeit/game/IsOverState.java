/*
 *     bachelorthesis-risikonet
 *     Copyright (C) 2019  Timo Weike
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package de.uni_bielefeld.timo_weike.bachelorarbeit.game;

import java.util.Objects;

/**
 * Wrapper class for the IsOverState.
 */
public class IsOverState {
    private final boolean isOver;
    private final PlayerId winner;

    private IsOverState(boolean isOver, PlayerId winner) {
        this.isOver = isOver;
        this.winner = winner;
    }

    public static IsOverState of(boolean isOver, PlayerId winner) {
        return new IsOverState(isOver, winner);
    }

    public static IsOverState over(PlayerId winner) {
        return new IsOverState(true, winner);
    }

    @Override
    public int hashCode() {
        return Objects.hash(isOver(), getWinner());
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        IsOverState that = (IsOverState) o;
        return isOver() == that.isOver() &&
                       getWinner() == that.getWinner();
    }

    /**
     * Indicates if the game is over.
     *
     * @return
     */
    public boolean isOver() {
        return isOver;
    }

    /**
     * Gets the player with the highest relative area.
     *
     * @return
     */
    public PlayerId getWinner() {
        return winner;
    }

    @Override
    public String toString() {
        return "IsOverState{" +
                       "isOver=" + isOver +
                       ", winner=" + winner +
                       '}';
    }
}
