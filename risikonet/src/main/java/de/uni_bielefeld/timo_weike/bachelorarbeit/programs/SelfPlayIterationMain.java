package de.uni_bielefeld.timo_weike.bachelorarbeit.programs;

import java.io.File;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.*;

import org.apache.commons.lang3.time.StopWatch;
import org.deeplearning4j.nn.graph.ComputationGraph;
import org.jetbrains.annotations.NotNull;
import org.nd4j.linalg.api.ndarray.INDArray;
import org.nd4j.linalg.factory.Nd4j;
import org.nd4j.linalg.primitives.ImmutablePair;
import org.nd4j.linalg.primitives.ImmutableTriple;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import de.uni_bielefeld.timo_weike.bachelorarbeit.game.GameBoard;
import de.uni_bielefeld.timo_weike.bachelorarbeit.game.PlayerId;
import de.uni_bielefeld.timo_weike.bachelorarbeit.training.*;
import de.uni_bielefeld.timo_weike.bachelorarbeit.util.DaemonThreadFactory;

import static org.nd4j.linalg.primitives.ImmutablePair.of;

public class SelfPlayIterationMain {
    private static final Logger log = LoggerFactory.getLogger(SelfPlayIterationMain.class);

    private static final int NUM_THREADS = 2;

    private final static int GAME_COUNT = 5;

    public static void main(String[] args) {
        try {
            final ExecutorService threadPool = Executors.newFixedThreadPool(NUM_THREADS, DaemonThreadFactory.instance);
            final CompletionService<ImmutablePair<Double, ImmutableTriple<Integer, PlayerId, GameBoard>>> service = new ExecutorCompletionService<>(threadPool);
            final File bestModelFile = Utils.getBestModel();

            final ComputationGraph computationGraph = NeuralNetBuilder.getComputationGraph(bestModelFile);

            final NeuralNetEvaluator neuralNetEvaluator = new NeuralNetEvaluator(computationGraph);

            StopWatch stopWatch = new StopWatch();
            stopWatch.reset();

            stopWatch.start();
            for (int i = 0; i < GAME_COUNT; i++) {
                int finalI = i;
                service.submit(() -> playGame(neuralNetEvaluator, finalI));
            }

            List<ImmutablePair<Double, ImmutableTriple<Integer, PlayerId, GameBoard>>> gameOutput = new LinkedList<>();
            for (int i = 0; i < GAME_COUNT; i++) {
                try {
                    gameOutput.add(service.take().get());
                } catch (InterruptedException | ExecutionException e) {
                    e.printStackTrace();
                }
            }
            stopWatch.stop();

            double[] time = gameOutput.stream().mapToDouble(t -> t.getFirst()).toArray();
            INDArray indArray = Nd4j.create(time);
            double mean = indArray.meanNumber().doubleValue();
            double std = indArray.sub(mean).stdNumber().doubleValue();

            double winsP1 = 0;
            double winsP2 = 0;
            double winsP3 = 0;
            double winsP4 = 0;
            for (ImmutablePair<Double, ImmutableTriple<Integer, PlayerId, GameBoard>> r : gameOutput) {
                switch (r.getSecond().getSecond()) {
                    case FirstPlayer:
                        winsP1++;
                        break;
                    case SecondPlayer:
                        winsP2++;
                        break;
                    case ThirdPlayer:
                        winsP3++;
                        break;
                    case FourthPlayer:
                        winsP4++;
                        break;
                }
            }

            System.out.println(String.format("RES:The section took %gm to finish %d games", stopWatch.getTime(TimeUnit.SECONDS) / 60.0, GAME_COUNT));
            System.out.println(String.format("RES:The mean time is %gm with a std of %gm", mean, std));
            System.out.println(String.format("RES:Win rates p1:%g, p2:%g, p3:%g, p4:%g", winsP1 / GAME_COUNT, winsP2 / GAME_COUNT, winsP3 / GAME_COUNT, winsP4 / GAME_COUNT));
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        System.exit(0);
    }

    @NotNull
    private static ImmutablePair<Double, ImmutableTriple<Integer, PlayerId, GameBoard>> playGame(NeuralNetEvaluator neuralNetEvaluator, int gameNumber) {
        StopWatch st = new StopWatch();
        st.reset();

        final SimulationGame simulationGame = new SimulationGame(neuralNetEvaluator);
        AiPlayer p1 = new AiPlayer(PlayerId.FirstPlayer, simulationGame);
        AiPlayer p2 = new AiPlayer(PlayerId.SecondPlayer, simulationGame);
        AiPlayer p3 = new AiPlayer(PlayerId.ThirdPlayer, simulationGame);
        AiPlayer p4 = new AiPlayer(PlayerId.FourthPlayer, simulationGame);

        GameBoard gb = new GameBoard();

        gb.init();

        TrainingGame tg = new TrainingGame(p1, p2, p3, p4, gb);

        log.info(String.format("Starting the game %d", gameNumber));
        st.start();
        final ImmutableTriple<Integer, PlayerId, GameBoard> play = tg.play(simulationGame, gameNumber);
        st.stop();
        System.out.println(String.format("RES:Game %d took %gm and %d turns, %s won and it looked like %n%s", gameNumber, ((double) st.getTime(TimeUnit.SECONDS) / 60.0), play.getFirst(), play.getSecond(), play.getThird().print(null, true)));

        return of(st.getTime(TimeUnit.SECONDS) / 60.0, play);
    }
}
