/*
 *     bachelorthesis-risikonet
 *     Copyright (C) 2019  Timo Weike
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package de.uni_bielefeld.timo_weike.bachelorarbeit.training;

import java.io.File;
import java.io.IOException;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.apache.commons.io.FilenameUtils;
import org.jetbrains.annotations.NotNull;
import org.nd4j.linalg.api.ndarray.INDArray;
import org.nd4j.linalg.dataset.MultiDataSet;
import org.nd4j.linalg.factory.Nd4j;
import org.nd4j.linalg.indexing.conditions.IsNaN;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import de.uni_bielefeld.timo_weike.bachelorarbeit.game.*;
import de.uni_bielefeld.timo_weike.bachelorarbeit.util.MyFileUtils;

import static de.uni_bielefeld.timo_weike.bachelorarbeit.training.Utils.*;

public class AiPlayer {
    private static final Logger log = LoggerFactory.getLogger(AiPlayer.class);
    private final PlayerId playerId;
    private IEvaluator simulationGame;
    private List<BoardActionProbSetPair<IAttack>> stateListAttacks = new LinkedList<>();
    private IAction lastAction;
    private List<BoardActionProbSetPair<IReinforcement>> stateListReinforcements = new LinkedList<>();

    public AiPlayer(PlayerId playerId, IEvaluator simulationGame) {
        this.playerId = playerId;
        this.simulationGame = simulationGame;

        this.lastAction = null;
    }

    public static void lookForNaN(org.nd4j.linalg.dataset.api.MultiDataSet multiDataSet) {
        INDArray[] features = multiDataSet.getFeatures();
        INDArray[] labels = multiDataSet.getLabels();

        if (features[0].cond(new IsNaN()).sumNumber().intValue() > 0) {
            System.out.println("NaN detected in features");
        }
        if (labels[0].cond(new IsNaN()).sumNumber().intValue() > 0) {
            System.out.println("NaN detected in values");
        }
        if (labels[1].cond(new IsNaN()).sumNumber().intValue() > 0) {
            System.out.println("NaN detected in attack");
        }
        if (labels[2].cond(new IsNaN()).sumNumber().intValue() > 0) {
            System.out.println("NaN detected in reinforcement");
        }
    }

    //    @Override
    public IAttack nextMove(GameBoard board) {
        Set<ActionProbability<IAttack>> posMov2 = simulationGame.evaluateForAttackPhase(board).getActionProbabilitySet();

        stateListAttacks.add(BoardActionProbSetPair.of(board, posMov2));

        Optional<ActionProbability<IAttack>> bestAttack = posMov2.stream().max(Comparator.comparingDouble(t -> t.getProbability()));

        if (bestAttack.isPresent()) {
            this.lastAction = bestAttack.get().getAction();
            return (IAttack) lastAction;
        } else {
            return null;
        }
    }

    //    @Override
    public IReinforcement whereToReinforce(GameBoard board) {
        final Set<ActionProbability<IReinforcement>> probReinforcementPairs
                = simulationGame.evaluateForReinforcementPhase(board).getActionProbabilitySet();

        this.stateListReinforcements.add(BoardActionProbSetPair.of(board, probReinforcementPairs));

        final Optional<ActionProbability<IReinforcement>> bestReinforcement
                = probReinforcementPairs
                          .stream()
                          .max(Comparator.comparingDouble(t -> t.getProbability()));

        if (bestReinforcement.isPresent()) {
            //log
            this.lastAction = bestReinforcement.get().getAction();
            return (IReinforcement) lastAction;
        } else {
            throw new IllegalStateException("Control should not be here");
        }
    }

    //    @Override
    public PlayerId getPlayerId() {
        return playerId;
    }

    public void saveTrainingData(GameBoard gameBoard) {
        MultiDataSet attackDataSet = getAttackDataSet(gameBoard, playerId);

        MultiDataSet reinforceDataSet = getReinforceDataSet(gameBoard, playerId);

        MultiDataSet multiDataSet = null;

        if (attackDataSet != null && reinforceDataSet != null)
            multiDataSet = MultiDataSet.merge(Stream.of(attackDataSet, reinforceDataSet).collect(Collectors.toList()));
        else if (attackDataSet != null)
            multiDataSet = attackDataSet;
        else if (reinforceDataSet != null)
            multiDataSet = reinforceDataSet;

        if (multiDataSet != null) {
            multiDataSet.shuffle();

            lookForNaN(multiDataSet);

            final String fileName = MyFileUtils.getTimeStampedFile(GAME_DATA_FILEBASE_NAME, GAME_DATA_EXTENSION);
            final String filePath = FilenameUtils.concat(DATA_DIR, fileName);

            try {
                File file = new File(filePath);
                log.debug(String.format("DataSet gets saved in %s", file.getCanonicalPath()));

                multiDataSet.save(file);
            } catch (IOException e) {
                e.printStackTrace();
            }

            try {
                File file = new File(filePath);
                MultiDataSet multiDataSet1 = new MultiDataSet();
                multiDataSet1.load(file);

                lookForNaN(multiDataSet1);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    private MultiDataSet getAttackDataSet(GameBoard gameBoard, PlayerId playerId) {
        final int numData = stateListAttacks.size();

        if (numData <= 0) {
            log.info("attack list is empty");
            return null;
        }

        final INDArray actionValue = GameBoardUtil.createAttackActionValue(gameBoard, playerId);

        final INDArray game_boards_attack = Nd4j.stack(0, stateListAttacks.stream()
                                                                  .map(t -> t.getGameBoard())
                                                                  .map(gb -> gb.packFor(this.getPlayerId()))
                                                                  .toArray(INDArray[]::new));
        final INDArray attackPredictions = Nd4j.stack(0, stateListAttacks.stream()
                                                                 .map(t1 -> t1.getActionProbabilitySet())
                                                                 .map(s1 -> GameBoardUtil.createAttackPrediction(s1))
                                                                 .toArray(INDArray[]::new));
        final INDArray actionValues = Nd4j.stack(0, stateListAttacks.stream()
                                                            .map(t -> actionValue)
                                                            .toArray(INDArray[]::new));

        final INDArray reinforcementPredictionsProxi = GameBoardUtil.createReinforcementMask(numData, false);

        final INDArray attackMask = GameBoardUtil.createAttackMask(numData, true);
        final INDArray reinforcementMask = GameBoardUtil.createReinforcementMask(numData, false);
        final INDArray actionValueMask = GameBoardUtil.createActionValueMask(numData, true);

        log.info("Attack DataSet creation");
        return createMultiDataSet(game_boards_attack, attackPredictions, actionValues, reinforcementPredictionsProxi, attackMask, reinforcementMask, actionValueMask);
    }

    private MultiDataSet getReinforceDataSet(GameBoard gameBoard, PlayerId playerId) {
        final int numData = stateListReinforcements.size();

        if (numData <= 0) {
            log.info("reinforcment list is empty");
            return null;
        }

        final INDArray actionValue = GameBoardUtil.createReinforcementActionValue(gameBoard, playerId);

        final INDArray gameBoards = Nd4j.stack(0, stateListReinforcements.stream()
                                                          .map(t -> t.getGameBoard())
                                                          .map(gb -> gb.packFor(this.getPlayerId()))
                                                          .toArray(INDArray[]::new));
        final INDArray reinforcementPredictions = Nd4j.stack(0, stateListReinforcements.stream()
                                                                        .map(t1 -> t1.getActionProbabilitySet())
                                                                        .map(s1 -> GameBoardUtil.createReinforcementPrediction(s1))
                                                                        .toArray(INDArray[]::new));
        final INDArray actionValues = Nd4j.stack(0, stateListReinforcements.stream()
                                                            .map(s -> actionValue)
                                                            .toArray(INDArray[]::new));

        final INDArray attackPredictionsProxi = GameBoardUtil.createAttackMask(numData, false);

        final INDArray attackMask = GameBoardUtil.createAttackMask(numData, false);
        final INDArray reinforcementMask = GameBoardUtil.createReinforcementMask(numData, true);
        final INDArray actionValueMask = GameBoardUtil.createActionValueMask(numData, true);

        log.info("Reinforcement DataSet creation");
        return createMultiDataSet(gameBoards, attackPredictionsProxi, actionValues, reinforcementPredictions, attackMask, reinforcementMask, actionValueMask);
    }

    @NotNull
    private MultiDataSet createMultiDataSet(INDArray gameBoards,
                                            INDArray attackPredictions, INDArray actionValues, INDArray reinforcementPredictions,
                                            INDArray attackMask, INDArray reinforcementMask, INDArray actionValueMask) {
        MultiDataSet dataSet = new MultiDataSet();

        dataSet.setFeatures(new INDArray[]{gameBoards});
        dataSet.setLabels(new INDArray[]{actionValues, attackPredictions, reinforcementPredictions});

        dataSet.setLabelsMaskArray(new INDArray[]{actionValueMask, attackMask, reinforcementMask});

        log.info("pre shuffle");
        lookForNaN(dataSet);

        return dataSet;
    }

    private static class BoardActionProbSetPair<T extends IAction> {
        private final GameBoard gameBoard;
        private final Set<ActionProbability<T>> actionProbabilitySet;

        private BoardActionProbSetPair(GameBoard gameBoard, Set<ActionProbability<T>> actionProbabilitySet) {
            this.gameBoard = gameBoard;
            this.actionProbabilitySet = actionProbabilitySet;
        }

        public static <R extends IAction> BoardActionProbSetPair<R> of(GameBoard gameBoard, Set<ActionProbability<R>> actionProbabilitySet) {
            return new BoardActionProbSetPair<>(gameBoard, actionProbabilitySet);
        }

        public GameBoard getGameBoard() {
            return gameBoard;
        }

        public Set<ActionProbability<T>> getActionProbabilitySet() {
            return actionProbabilitySet;
        }
    }
}
