/*
 *     bachelorthesis-risikonet
 *     Copyright (C) 2019  Timo Weike
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package de.uni_bielefeld.timo_weike.bachelorarbeit.util;

import java.util.function.Supplier;

/**
 * A class that caches the result of a function.
 * This class assumes that the function is pure, that means has no side effects and its return value does not change
 * over time.
 *
 * @param <T> the type of value that should be cached.
 */
public class Cache<T> {
    private final Supplier<T> func;
    private T cachedValue;

    public Cache(Supplier<T> func) {
        this.func = func;
        cachedValue = null;
    }


    public T get() {
        if (this.cachedValue == null) {
            this.cachedValue = func.get();
        }

        return this.cachedValue;
    }
}
