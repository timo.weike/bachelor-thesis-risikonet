/*
 *     bachelorthesis-risikonet
 *     Copyright (C) 2019  Timo Weike
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package de.uni_bielefeld.timo_weike.bachelorarbeit.training;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import de.uni_bielefeld.timo_weike.bachelorarbeit.game.*;

import static de.uni_bielefeld.timo_weike.bachelorarbeit.util.Numbers.oneOr;

/**
 * This is an Evaluator that uses an algorithm to determine the probabilities for all possible actions.
 */
public class DesignedEvaluator implements IEvaluator {
    @Override
    public EvalResult<IAttack> evaluateForAttackPhase(GameBoard gameBoard) {
        final List<IAttack> iAttacks = gameBoard.possibleAttacks();
        final List<ActionProbability<IAttack>> attackProbs = new LinkedList<>();

        float actionValueSum = 1.0f;

        for (IAttack attack : iAttacks) {
            if (attack.isPhaseEnding()) {
                attackProbs.add(ActionProbability.of(1.0, attack));
            } else {
                final int attackValue = gameBoard.getCell(attack.getPosition()).getValue();
                final Cell defenseCell = gameBoard.getCell(attack.getAttackedPosition());
                final int defenseValue = defenseCell.getValue();

                final int factor = defenseCell.getOwner() == PlayerId.Neutral ? 1 : 10;

                float actionValue = (attackValue - defenseValue + 1) * factor;
                actionValueSum += actionValue;

                attackProbs.add(ActionProbability.of(actionValue, attack));
            }
        }

        final double finalActionValueSum = oneOr(actionValueSum);
        final Set<ActionProbability<IAttack>> actionProbabilitySet
                = attackProbs.stream()
                          .map(t -> ActionProbability.of(t.getProbability() / finalActionValueSum, t.getAction()))
                          .collect(Collectors.toSet());

        return new EvalResult<>(actionProbabilitySet, 0);
    }

    @Override
    public EvalResult<IReinforcement> evaluateForReinforcementPhase(final GameBoard gameBoard) {
        final Set<IReinforcement> possibleReinforcements = gameBoard.possibleReinforcements();
        final List<ActionProbability<IReinforcement>> reinforcementPorbs = new LinkedList<>();

        double actionValueSum = 0.5;

        for (IReinforcement reinforcement : possibleReinforcements) {
            if (reinforcement.isPhaseEnding()) {
                reinforcementPorbs.add(ActionProbability.of(0.5, reinforcement));
            } else {
                final Position position = reinforcement.getPosition();
                final Cell cell = gameBoard.getCell(position);

                final double actionValue = Math.max(0, Arrays.stream(MoveDirection.values()).mapToInt(m -> {
                    Cell c = gameBoard.getCell(position.getNeighborInDirection(m));
                    if (gameBoard.canMove(position, m) && c.getOwner() != gameBoard.getTurnState().getPlayerId()) {
                        return c.getValue() + 1;
                    } else {
                        return 0;
                    }
                }).sum() - cell.getValue());

                reinforcementPorbs.add(ActionProbability.of(actionValue, reinforcement));

                actionValueSum += actionValue;
            }
        }

        final double finalActionValueSum = oneOr(actionValueSum);
        final Set<ActionProbability<IReinforcement>> collect
                = reinforcementPorbs.stream()
                          .map(t -> ActionProbability.of(t.getProbability() / finalActionValueSum, t.getAction()))
                          .collect(Collectors.toSet());

        return new EvalResult<>(collect, 0);
    }
}
