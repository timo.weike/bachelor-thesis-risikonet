/*
 *     bachelorthesis-risikonet
 *     Copyright (C) 2019  Timo Weike
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package de.uni_bielefeld.timo_weike.bachelorarbeit.game;

/**
 * Util-Class for PlayerId.
 */
public final class PlayerUtil {
    private PlayerUtil() {}

    /**
     * Determines which is the next player to play.
     *
     * @param playerId
     * @return
     * @throws IllegalArgumentException if playerId it PlayerId.Neutral
     */
    public static PlayerId next(PlayerId playerId) {
        switch (playerId) {
            case Neutral:
                throw new IllegalArgumentException(String.format("%s ist not a playable PlayerID", playerId));
            case FirstPlayer:
                return PlayerId.SecondPlayer;
            case SecondPlayer:
                return PlayerId.ThirdPlayer;
            case ThirdPlayer:
                return PlayerId.FourthPlayer;
            case FourthPlayer:
                return PlayerId.FirstPlayer;
        }

        throw new IllegalStateException(String.format("Control should not be here: %s", playerId));
    }

    /**
     * Gets an index for the player.
     *
     * @param playerId
     * @return
     */
    public static int getPlayerNum(PlayerId playerId) {
        switch (playerId) {
            case Neutral: return 0;
            case FirstPlayer: return 1;
            case SecondPlayer: return 2;
            case ThirdPlayer: return 3;
            case FourthPlayer: return 4;
        }
        return 0;
    }
}
