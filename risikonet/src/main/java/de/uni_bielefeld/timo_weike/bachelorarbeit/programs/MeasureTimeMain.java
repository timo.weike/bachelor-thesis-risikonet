package de.uni_bielefeld.timo_weike.bachelorarbeit.programs;

import java.io.File;

import org.apache.commons.lang3.time.StopWatch;
import org.deeplearning4j.nn.graph.ComputationGraph;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import de.uni_bielefeld.timo_weike.bachelorarbeit.game.GameBoard;
import de.uni_bielefeld.timo_weike.bachelorarbeit.game.GlobalGameConfig;
import de.uni_bielefeld.timo_weike.bachelorarbeit.game.PlayerId;
import de.uni_bielefeld.timo_weike.bachelorarbeit.training.*;

public class MeasureTimeMain {
    private static final Logger log = LoggerFactory.getLogger(SelfPlayIterationMain.class);

    private final static int GAME_COUNT = 5;

    public static void main(String[] args) {
        try {
            final File bestModelFile = Utils.getBestModel();

            final ComputationGraph computationGraph = NeuralNetBuilder.getComputationGraph(bestModelFile);

            final NeuralNetEvaluator neuralNetEvaluator = new NeuralNetEvaluator(computationGraph);

            long timeWithoutTreeSearch = measureTimeWithoutTreeSearch(neuralNetEvaluator, GAME_COUNT);
            long timeRandomChooser = measureTimeRandomChooser(GAME_COUNT);

            log.info(String.format("RES:Nanoseconds per action without Treesearch:       %19d", timeWithoutTreeSearch));
            log.info(String.format("RES:Nanoseconds per action for random chooser:       %19d", timeRandomChooser));

            GlobalGameConfig.setSimulationCount(50);
            long timeWithTreeSearch50Sims = measureTimeWithTreeSearch(neuralNetEvaluator, GAME_COUNT);
            log.info(String.format("RES:Nanoseconds per action with Treesearch  50 Sims: %19d", timeWithTreeSearch50Sims));

            GlobalGameConfig.setSimulationCount(100);
            long timeWithTreeSearch100Sims = measureTimeWithTreeSearch(neuralNetEvaluator, GAME_COUNT);
            log.info(String.format("RES:Nanoseconds per action with Treesearch 100 Sims: %19d", timeWithTreeSearch100Sims));

            GlobalGameConfig.setSimulationCount(200);
            long timeWithTreeSearch200Sims = measureTimeWithTreeSearch(neuralNetEvaluator, GAME_COUNT);
            log.info(String.format("RES:Nanoseconds per action with Treesearch 200 Sims: %19d", timeWithTreeSearch200Sims));
        } catch (Exception e) {
            e.printStackTrace();
        }

        System.exit(0);
    }

    private static long measureTimeWithTreeSearch(NeuralNetEvaluator neuralNetEvaluator, int gameCount) {
        SimulationGame simulationGame = new SimulationGame(neuralNetEvaluator);

        return runMeasurement(gameCount, simulationGame);
    }

    private static long runMeasurement(int gameCount, IEvaluator simulationGame) {
        Res result = Res.ZERO;
        for (int i = 0; i < gameCount; i++) {
            result = result.add(playGame(simulationGame, i));
        }

        return result.getNanoTime() / result.getActionCount();
    }

    private static long measureTimeWithoutTreeSearch(NeuralNetEvaluator neuralNetEvaluator, int gameCount) {
        return runMeasurement(gameCount, neuralNetEvaluator);
    }

    private static long measureTimeRandomChooser(int gameCount) {
        return runMeasurement(gameCount, new RandomChooser());
    }

    private static Res playGame(IEvaluator evaluator, int gameNumber) {
        StopWatch st = new StopWatch();
        st.reset();

        SimulationGame simulationGame = null;
        if (evaluator instanceof SimulationGame) {
            simulationGame = (SimulationGame) evaluator;
        }

        AiPlayer p1 = new AiPlayer(PlayerId.FirstPlayer, evaluator);
        AiPlayer p2 = new AiPlayer(PlayerId.SecondPlayer, evaluator);
        AiPlayer p3 = new AiPlayer(PlayerId.ThirdPlayer, evaluator);
        AiPlayer p4 = new AiPlayer(PlayerId.FourthPlayer, evaluator);
        GameBoard gb = new GameBoard();

        gb.init();

        TrainingGame tg = new TrainingGame(p1, p2, p3, p4, gb);

        st.start();
        tg.measurePlay(simulationGame, gameNumber);
        st.stop();

        return Res.of(tg.getActionCount(), st.getNanoTime());
    }

    private static class Res {
        public static final Res ZERO = new Res(0L,0L);
        private final long actionCount;
        private final long nanoTime;

        private Res(long actionCount, long nanoTime) {
            this.actionCount = actionCount;
            this.nanoTime = nanoTime;
        }

        public static Res of(long actionCount, long nanoTime) {
            return new Res(actionCount, nanoTime);
        }

        public Res add(Res b) {
            return new Res(this.getActionCount() + b.getActionCount(),
                    this.getNanoTime() + b.getNanoTime());
        }

        public long getActionCount() {
            return actionCount;
        }

        public long getNanoTime() {
            return nanoTime;
        }
    }
}
