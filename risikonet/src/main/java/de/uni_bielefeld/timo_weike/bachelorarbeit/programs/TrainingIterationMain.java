package de.uni_bielefeld.timo_weike.bachelorarbeit.programs;

import java.io.File;
import java.util.concurrent.TimeUnit;

import org.apache.commons.io.FilenameUtils;
import org.deeplearning4j.api.storage.StatsStorage;
import org.deeplearning4j.datasets.iterator.file.FileMultiDataSetIterator;
import org.deeplearning4j.nn.graph.ComputationGraph;
import org.deeplearning4j.optimize.listeners.CheckpointListener;
import org.deeplearning4j.optimize.listeners.PerformanceListener;
import org.deeplearning4j.ui.api.UIServer;
import org.deeplearning4j.ui.stats.StatsListener;
import org.deeplearning4j.ui.storage.FileStatsStorage;
import org.nd4j.evaluation.regression.RegressionEvaluation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import de.uni_bielefeld.timo_weike.bachelorarbeit.training.NeuralNetBuilder;
import de.uni_bielefeld.timo_weike.bachelorarbeit.util.MyFileUtils;

import static de.uni_bielefeld.timo_weike.bachelorarbeit.training.Utils.*;

public class TrainingIterationMain {
    private static final Logger log = LoggerFactory.getLogger(TrainingIterationMain.class);
    private static final boolean USE_UI_SERVER = false;

    public static void main(String[] args) {
        final ComputationGraph computationGraph;
        try {
            computationGraph = NeuralNetBuilder.getComputationGraph(getBestModel());

            //Configure where the network information (gradients, score vs. time etc) is to be stored. Here: store in memory.
            String statsFileName = MyFileUtils.getTimeStampedFile(STATS_FILE_BASE_NAME, STATS_FILE_EXTENSION);
            String statsFilePath = FilenameUtils.concat(STATS_DIR, statsFileName);
            final File statsFile = new File(statsFilePath);
            statsFile.getParentFile().mkdirs();
            StatsStorage statsStorage = new FileStatsStorage(statsFile); // new InMemoryStatsStorage();         //Alternative: new FileStatsStorage(File), for saving and loading later
            
            if (USE_UI_SERVER) {
                UIServer uiServer = UIServer.getInstance();
                //Attach the StatsStorage instance to the UI: this allows the contents of the StatsStorage to be visualized
                uiServer.attach(statsStorage);
            }

            //Then add the StatsListener to collect this information from the network, as it trains
            computationGraph.setListeners(
                    new StatsListener(statsStorage),
                    new PerformanceListener(5, true),
                    new CheckpointListener.Builder(CHECKPOINTS_DIR)
                            .keepLast(2)
                            .deleteExisting(true)
                            .logSaving(true)
                            .saveEvery(5, TimeUnit.MINUTES)
                            .build()
                    );

            final FileMultiDataSetIterator fileMultiDataSetIterator = new FileMultiDataSetIterator(new File(DATA_DIR), 32, GAME_DATA_EXTENSION);

            computationGraph.fit(fileMultiDataSetIterator, 1);

            RegressionEvaluation[] regressionEvaluations = computationGraph.doEvaluation(fileMultiDataSetIterator, new RegressionEvaluation());

            log.info("\n" + regressionEvaluations[0].stats());

            saveComputationGraph(computationGraph);

            zipData();

            System.out.println("end");

        } catch (Exception e) {
            e.printStackTrace();
        }

        System.exit(0);
    }


}
