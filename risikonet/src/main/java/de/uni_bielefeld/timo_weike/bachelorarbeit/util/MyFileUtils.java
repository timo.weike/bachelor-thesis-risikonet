/*
 *     bachelorthesis-risikonet
 *     Copyright (C) 2019  Timo Weike
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package de.uni_bielefeld.timo_weike.bachelorarbeit.util;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Util-Class for File-Handling
 */
public class MyFileUtils {

    private final static SimpleDateFormat SIMPLE_DATE_FORMAT = new SimpleDateFormat("yyyy-MM-dd-HH-mm-ss-SSS");

    /**
     * creates a filename containing the current timestamp
     *
     * @param fileName name for the file
     * @param extension extension for the file
     * @return
     */
    public static String getTimeStampedFile(String fileName, String extension) {
        final StringBuilder stringBuilder = new StringBuilder();

        stringBuilder.append(fileName).append("-").append(SIMPLE_DATE_FORMAT.format(new Date()));

        if (extension != null && !extension.trim().isEmpty()) {
            if (extension.charAt(0) != '.') {
                stringBuilder.append('.');
            }
            stringBuilder.append(extension);
        }

        return stringBuilder.toString();
    }
}
