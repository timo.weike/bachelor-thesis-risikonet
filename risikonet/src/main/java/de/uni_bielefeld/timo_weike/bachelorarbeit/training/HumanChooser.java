/*
 *     bachelorthesis-risikonet
 *     Copyright (C) 2019  Timo Weike
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package de.uni_bielefeld.timo_weike.bachelorarbeit.training;

import java.util.Scanner;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.jetbrains.annotations.NotNull;

import de.uni_bielefeld.timo_weike.bachelorarbeit.game.*;

public class HumanChooser implements IEvaluator {

    private final Scanner inputStreamReader = new Scanner(System.in);

    @Override
    public EvalResult<IAttack> evaluateForAttackPhase(GameBoard gameBoard) {
        System.out.println(String.format("Its your turn %s:%n%s", gameBoard.getTurnState().getPlayerId(), gameBoard.print(null, true)));
        while (true) {
            System.out.print("From where do you want to attack(r,c)? ");


            String line = inputStreamReader.nextLine();

            if (line.contains("pass")) {
                return makeEvalResult(Action.getPassAttack());
            }

            String[] split = line.trim().split(",");
            int row = Integer.parseInt(split[0]);
            int col = Integer.parseInt(split[1]);

            Position position = new Position(row, col);

            System.out.println("In which direction do you want to attack");
            System.out.println(gameBoard.print(position, true));
            System.out.println("1  2   \n" +
                                       "3  x  4\n" +
                                       "   5  6\n> ");
            line = inputStreamReader.nextLine().trim();
            MoveDirection dir = translate(Integer.parseInt(line));

            IAttack attack = Action.getAttack(position, dir);

            if (gameBoard.canDo(attack)) {
                return makeEvalResult(attack);
            } else {
                System.out.println(String.format("The attack %s is not possible, try again", attack));
            }
        }
    }

    @Override
    public EvalResult<IReinforcement> evaluateForReinforcementPhase(GameBoard gameBoard) {
        System.out.println(String.format("Its your turn %s:%n%s", gameBoard.getTurnState().getPlayerId(), gameBoard.print(null, true)));
        while (true) {
            System.out.print(String.format("Where dor you want to reinforce (remaining = %d)(r,c)? ", gameBoard.remainingReinforcementPoints));

            String line = inputStreamReader.nextLine();

            if (line.contains("pass")) {
                return makeEvalResult(Action.getPassReinforcement());
            }

            String[] split = line.trim().split(",");
            int row = Integer.parseInt(split[0]);
            int col = Integer.parseInt(split[1]);

            Position position = new Position(row, col);

            IReinforcement reinforcement = Action.getReinforcement(position);

            if (gameBoard.canPerform(reinforcement)) {
                return makeEvalResult(reinforcement);
            } else {
                System.out.println(String.format("The reinforcement %s is not possible, try again", reinforcement));
            }
        }
    }

    @NotNull
    private <T extends IAction> EvalResult<T> makeEvalResult(T action) {
        Set<ActionProbability<T>> set = Stream
                                                .of(ActionProbability.of(1.0, action))
                                                .collect(Collectors.toSet());
        return new EvalResult<>(set, 0);
    }

    private MoveDirection translate(int i) {
        switch ((i)) {
            case 1:
                return MoveDirection.TopLeft;
            case 2:
                return MoveDirection.Top;
            case 3:
                return MoveDirection.Left;
            case 4:
                return MoveDirection.Right;
            case 5:
                return MoveDirection.Bottom;
            case 6:
                return MoveDirection.BottomRight;
        }

        throw new RuntimeException();
    }
}
