/*
 *     bachelorthesis-risikonet
 *     Copyright (C) 2019  Timo Weike
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package de.uni_bielefeld.timo_weike.bachelorarbeit.game;

import java.util.Objects;

/**
 * Wrapper for the state of a turn.
 */
public class TurnState {

    /**
     * Which player is to play
     */
    private PlayerId playerId;
    /**
     * In which phase is the turn
     */
    private TurnPhase turnPhase;

    private int turnCount;

    public TurnState(PlayerId playerId, TurnPhase turnPhase) {
        this.playerId = playerId;
        this.turnPhase = turnPhase;
        this.turnCount = 0;
    }

    public int getTurnCount() {
        return turnCount;
    }

    public TurnState dup() {
        final TurnState turnState = new TurnState(this.playerId, this.turnPhase);
        turnState.turnCount = this.turnCount;
        return turnState;
    }

    /**
     * Move this Turn state to the next phase or the next player.
     */
    void forward() {
        switch (this.getTurnPhase()) {
            case Attack:
                this.turnPhase = TurnPhase.Reinforce;
                break;
            case Reinforce:
                this.turnPhase = TurnPhase.Attack;
                this.playerId = PlayerUtil.next(this.getPlayerId());
                this.turnCount++;
                break;
        }
    }

    public TurnPhase getTurnPhase() {
        return turnPhase;
    }

    public PlayerId getPlayerId() {
        return playerId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        TurnState turnState = (TurnState) o;
        return getPlayerId() == turnState.getPlayerId() &&
                       getTurnPhase() == turnState.getTurnPhase();
    }

    @Override
    public int hashCode() {
        return Objects.hash(getPlayerId(), getTurnPhase());
    }
}
