/*
 *     bachelorthesis-risikonet
 *     Copyright (C) 2019  Timo Weike
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package de.uni_bielefeld.timo_weike.bachelorarbeit.game;

/**
 * A Util-Class for the MoveDirections
 */
final class MoveDirectionUtil {
    private  MoveDirectionUtil(){}

    /**
     * Gets the opposite direction of the given direction.
     *
     * @param moveDirection
     * @return
     */
    static MoveDirection getOpposite(MoveDirection moveDirection)
    {
        switch (moveDirection)
        {
            case TopLeft: return MoveDirection.BottomRight;
            case Top: return MoveDirection.Bottom;
            case Left: return MoveDirection.Right;
            case Right: return MoveDirection.Left;
            case Bottom: return MoveDirection.Top;
            case BottomRight: return MoveDirection.TopLeft;
        }

        throw new IllegalArgumentException("Not Part of Enum");
    }
}
