/*
 *     bachelorthesis-risikonet
 *     Copyright (C) 2019  Timo Weike
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package de.uni_bielefeld.timo_weike.bachelorarbeit.game;

import java.util.Objects;

/**
 * This class is used to model instances if IAttack and IReinforcement
 */
public class Action implements IAttack, IReinforcement {
    private final ActionType actionType;
    private Position position;
    private boolean phaseEnding;

    // Attack Eigenschaften
    private MoveDirection attackDirection;

    private Action(Position position) {
        this(ActionType.Reinforcement, false);
        this.position = position;
    }

    private Action(ActionType actionType, boolean phaseEnding) {
        this.actionType = actionType;
        this.phaseEnding = phaseEnding;
        this.position = null;
        this.attackDirection = null;
    }

    private Action(Position position, MoveDirection attackDirection) {
        this(ActionType.Attack, false);
        this.position = position;
        this.attackDirection = attackDirection;
    }

    public static IAttack getAttack(Position position, MoveDirection moveDirection) {
        if (position != null) {
            return new Action(position, moveDirection);
        } else {
            return getPassAttack();
        }
    }

    public static IAttack getPassAttack() {
        return new Action(ActionType.Attack, true);
    }

    public static IReinforcement getReinforcement(Position position) {
        if (position != null) {
            return new Action(position);
        } else {
            return getPassReinforcement();
        }
    }

    public static IReinforcement getPassReinforcement() {
        return new Action(ActionType.Reinforcement, true);
    }

    @Override
    public Position getAttackedPosition() {
        if (actionType == ActionType.Attack) {
            return position.getNeighborInDirection(this.getAttackDirection());
        } else {
            throw new IllegalStateException("Can use Attack-Methode on Reinforcement-Action");
        }
    }

    @Override
    public MoveDirection getAttackDirection() {
        if (actionType == ActionType.Attack) {
            return attackDirection;
        } else {
            throw new IllegalStateException("Can't use Attack-Methode on Reinforcement-Action");
        }
    }

    @Override
    public int hashCode() {
        return Objects.hash(actionType, getPosition(), isPhaseEnding(), attackDirection);
    }

    @Override
    public boolean isPhaseEnding() {
        return phaseEnding;
    }

    @Override
    public Position getPosition() {
        return position;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Action action = (Action) o;

        if (getActionType() == action.getActionType()) {
            switch (getActionType()) {
                case Attack:
                    return isPhaseEnding() == action.isPhaseEnding()
                                   && Objects.equals(getPosition(), action.getPosition())
                                   && getAttackDirection() == action.getAttackDirection();
                case Reinforcement:
                    return isPhaseEnding() == action.isPhaseEnding()
                                   && Objects.equals(getPosition(), action.getPosition());
            }
        }

        return false;
    }

    public ActionType getActionType() {
        return actionType;
    }

    @Override
    public String toString() {
        switch (this.actionType) {
            case Attack:
                if (this.isPhaseEnding()) {
                    return "Attack: Pass";
                } else {
                    return String.format("Attack: from %s in dir %s", this.getPosition(), this.getAttackDirection());
                }
            case Reinforcement:
                if (this.isPhaseEnding()) {
                    return "Reinforcement: Pass";
                } else {
                    return String.format("Reinforcement: on %s", this.getPosition());
                }
        }

        throw new IllegalStateException(String.format("Control should not be here: %s", this.actionType));
    }
}
