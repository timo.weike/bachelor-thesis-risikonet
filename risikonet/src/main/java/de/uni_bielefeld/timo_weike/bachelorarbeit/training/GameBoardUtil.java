/*
 *     bachelorthesis-risikonet
 *     Copyright (C) 2019  Timo Weike
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package de.uni_bielefeld.timo_weike.bachelorarbeit.training;

import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import org.nd4j.linalg.api.ndarray.INDArray;
import org.nd4j.linalg.factory.Nd4j;
import org.nd4j.linalg.indexing.NDArrayIndex;

import de.uni_bielefeld.timo_weike.bachelorarbeit.game.*;
import de.uni_bielefeld.timo_weike.bachelorarbeit.util.INDArrayUtil;
import de.uni_bielefeld.timo_weike.bachelorarbeit.util.Numbers;

import static de.uni_bielefeld.timo_weike.bachelorarbeit.game.GlobalGameConfig.*;
import static de.uni_bielefeld.timo_weike.bachelorarbeit.util.INDArrayUtil.indexer;
import static de.uni_bielefeld.timo_weike.bachelorarbeit.util.Numbers.oneOr;

public final class GameBoardUtil {
    private GameBoardUtil() {
    }

    public static Set<ActionProbability<IAttack>> createAttackPrediction(INDArray attackOut, GameBoard gameBoard, boolean filter) {
        List<ActionProbability<IAttack>> attackProbs = new LinkedList<>();

        final INDArray indArray = attackOut.get(NDArrayIndex.interval(0, 6 * getRowCount() * getColCount())).reshape(6, getRowCount(), getColCount());
        final double passProb = attackOut.getDouble(6 * getRowCount() * getColCount());

        attackProbs.add(ActionProbability.of(passProb, Action.getPassAttack()));

        double probSum = passProb;

        for (int layer = 0; layer < 6; layer++) {
            for (int row = 0; row < getRowCount(); row++) {
                for (int col = 0; col < getColCount(); col++) {
                    final Position pos = new Position(row, col);
                    final double prob = indArray.getDouble(layer, row, col);

                    final IAttack attack = Action.getAttack(pos, MoveDirection.values()[layer]);

                    if (!filter || gameBoard.canDo(attack)) {
                        attackProbs.add(ActionProbability.of(prob, attack));
                        probSum += prob;
                    }
                }
            }
        }

        final double finalProbSum = oneOr(probSum);

        return attackProbs.stream()
                       .map(t -> ActionProbability.of(t.getProbability() / finalProbSum, t.getAction()))
                       .collect(Collectors.toSet());

    }

    public static INDArray createAttackPrediction(Set<ActionProbability<IAttack>> attackProbs) {
        INDArray pred = Nd4j.zeros(6, getRowCount(), getColCount());
        INDArray passPred = Nd4j.zeros(1).reshape(1);

        for (ActionProbability<IAttack> tuple : attackProbs) {
            IAttack attack = tuple.getAction();

            double prob = tuple.getProbability();

            if (Double.isNaN(prob)) {
                System.out.println("NaN detected: attack");
                prob = 0.0;
            }

            if (attack.isPhaseEnding()) {
                passPred.putScalar(0, (float) prob);
            } else {
                int layer = attack.getAttackDirection().ordinal();
                int row = attack.getPosition().getRow();
                int col = attack.getPosition().getCol();

                pred.putScalar(indexer(layer, row, col), (float) prob);
            }
        }

        INDArray result = Nd4j.concat(0, pred.reshape(6 * getRowCount() * getColCount()), passPred);
        INDArrayUtil.normalizei(result);
        return result;
    }

    public static Set<ActionProbability<IReinforcement>> createReinforcementPrediction(INDArray reinforcementOut, GameBoard gameBoard, boolean filter) {
        List<ActionProbability<IReinforcement>> reinforcementProbs = new LinkedList<>();

        final INDArray indArray = reinforcementOut.get(NDArrayIndex.interval(0, getRowCount() * getColCount())).reshape(getRowCount(), getColCount());
        final double passProb = reinforcementOut.getDouble(getRowCount() * getColCount());

        reinforcementProbs.add(ActionProbability.of(passProb, Action.getPassReinforcement()));

        double probSum = passProb;

        for (int row = 0; row < getRowCount(); row++) {
            for (int col = 0; col < getColCount(); col++) {
                final Position pos = new Position(row, col);
                final double prob = indArray.getDouble(row, col);

                final IReinforcement reinforcement = Action.getReinforcement(pos);

                if (!filter || gameBoard.canPerform(reinforcement)) {
                    reinforcementProbs.add(ActionProbability.of(prob, reinforcement));
                    probSum += prob;
                }
            }
        }

        final double finalProbSum = oneOr(probSum);

        if (finalProbSum != 0.0) {
            return reinforcementProbs.stream()
                           .map(t -> ActionProbability.of(t.getProbability() / finalProbSum, t.getAction()))
                           .collect(Collectors.toSet());
        } else {
            return new HashSet<>(reinforcementProbs);
        }
    }

    public static INDArray createReinforcementPrediction(Set<ActionProbability<IReinforcement>> reinforcementProbs) {
        INDArray pred = Nd4j.zeros(getRowCount(), getColCount());
        INDArray passPred = Nd4j.zeros(1).reshape(1);

        for (ActionProbability<IReinforcement> tuple : reinforcementProbs) {
            IReinforcement reinforcement = tuple.getAction();

            double prob = tuple.getProbability();
            if (Double.isNaN(prob)) {
                System.out.println("NaN detected: reinf");
                prob = 0.0;
            }

            if (reinforcement.isPhaseEnding()) {
                passPred.putScalar(0, prob);
            } else {
                final int row = reinforcement.getPosition().getRow();
                final int col = reinforcement.getPosition().getCol();

                pred.putScalar(row, col, prob);
            }
        }

        INDArray result = Nd4j.concat(0, pred.reshape(getRowCount() * getColCount()), passPred);

        INDArrayUtil.normalizei(result);

        return result;
    }

    public static INDArray createReinforcementMask(int numData, boolean use) {
        if (use) {
            return Nd4j.ones(numData, getRowCount() * getColCount() + 1);
        } else {
            return Nd4j.zeros(numData, getRowCount() * getColCount() + 1);
        }
    }

    public static INDArray createAttackMask(int numData, boolean use) {
        if (use) {
            return Nd4j.ones(numData, 6 * getRowCount() * getColCount() + 1);
        } else {
            return Nd4j.zeros(numData, 6 * getRowCount() * getColCount() + 1);
        }
    }

    public static INDArray createActionValueMask(int numData, boolean use) {
        if (use) {
            return Nd4j.ones(numData, 1);
        } else {
            return Nd4j.zeros(numData, 1);
        }
    }

    public static float getActionValue(INDArray valueOut) {
        return valueOut.getFloat(0);
    }

    public static INDArray createAttackActionValue(GameBoard gameBoard, PlayerId playerId) {
        final double attackMixTerm = getAttackMixTerm();
        return getActionValueWithMixTerm(gameBoard, playerId, attackMixTerm);
    }

    public static INDArray createReinforcementActionValue(GameBoard gameBoard, PlayerId playerId) {
        final double reinforcementMixTerm = getReinforcementMixTerm();
        return getActionValueWithMixTerm(gameBoard, playerId, 1-reinforcementMixTerm);
    }

    private static INDArray getActionValueWithMixTerm(GameBoard gameBoard, PlayerId playerId, double attackMixTerm) {
        double actionValue;
        if (isUseNoneBinaryEvaluation()) {
            double area = gameBoard.getAreaPercentFor(playerId);
            double strength = gameBoard.getStrengthPercent(playerId);
            double tmp = Numbers.mix(area, strength, attackMixTerm);

            actionValue = map(tmp); // converting from [0..1] to [-1..1]
        } else {
            PlayerId winningPlayer = gameBoard.getHighestArea();
            if (gameBoard.getTurnState().getPlayerId() == winningPlayer) {
                actionValue = 1.0;
            } else {
                actionValue = -1.0;
            }
        }

        return Nd4j.valueArrayOf(1, actionValue).reshape(1);
    }

    private static double map(double x) {
        // this is the simplified for of 2 * x^[ln(1/2)/ln(1/n)] - 1
        // where n is the number of players
        return 2.0 * Math.sqrt(x) - 1;
    }
}
