/*
 *     bachelorthesis-risikonet
 *     Copyright (C) 2019  Timo Weike
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package de.uni_bielefeld.timo_weike.bachelorarbeit.training;

import java.util.Comparator;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;

import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.NotNull;

import de.uni_bielefeld.timo_weike.bachelorarbeit.game.*;

import static de.uni_bielefeld.timo_weike.bachelorarbeit.util.Numbers.oneOr;

/**
 * The tree or sub-tree for the tree-search.
 */
public class SimulationTree {
    private int visitCount;
    private double totalActionValue;

    private GameBoard gameBoard;
    private IEvaluator evaluator;
    private Map<IAction, ActionEdge> actionEdges;

    public SimulationTree(GameBoard gameBoard, IEvaluator evaluator) {
        this.gameBoard = gameBoard;
        this.evaluator = evaluator;
        actionEdges = null;

        visitCount = 1;
        totalActionValue = 0.0;

        final TurnState turnState = gameBoard.getTurnState();

        if (turnState.getTurnPhase() == TurnPhase.Attack) {
            initAttackPhase();
        } else {
            initReinforcementPhase();
        }
    }

    private void initAttackPhase() {
        final EvalResult<IAttack> evalResult = evaluator.evaluateForAttackPhase(gameBoard);
        initActionEdgeMap(initEdgesWithProb(evalResult.getActionProbabilitySet()), evalResult.getActionValue());
    }

    private void initActionEdgeMap(Map<IAction, ActionEdge> actionEdgeMap, double actionValue) {
        actionEdges = actionEdgeMap;
        this.addToTotalActionValue(actionValue);
    }

    private void initReinforcementPhase() {
        final EvalResult<IReinforcement> evalResult = evaluator.evaluateForReinforcementPhase(gameBoard);
        initActionEdgeMap(initEdgesWithProb(evalResult.getActionProbabilitySet()), evalResult.getActionValue());
    }

    private <T extends IAction> Map<IAction, ActionEdge> initEdgesWithProb(Set<ActionProbability<T>> probAttackPairs) {
        if (!gameBoard.isOver().isOver()) {
            Map<T, ActionEdge> edgeMap = probAttackPairs.stream().collect(Collectors.toMap(t -> t.getAction(), t -> new ActionEdge(t.getProbability())));

            return new ConcurrentHashMap<>(edgeMap);
        } else {
            return null;
        }
    }

    IAttack getNextAttack() {
        IAction nextAction = getNextAction();
        if (nextAction instanceof IAttack) {
            return (IAttack) nextAction;
        } else {
            throw new RuntimeException();
        }
    }

    private synchronized IAction getNextAction() {
        synchronized (this) {
            final double sqrtOfSumOfVisitCounts = Math.sqrt(actionEdges.values().stream().mapToInt(a -> a.getVisitCount()).sum());

            return actionEdges.entrySet().stream()
                           .map(e -> ActionProbability.of(e.getValue().getMeanActionValue() + calcU_s_a(sqrtOfSumOfVisitCounts, e.getValue()), e.getKey()))
                           .max(Comparator.comparingDouble(t -> t.getProbability()))
                           .map(t -> t.getAction())
                           .orElse(null);
        }
    }

    @Contract(pure = true)
    private double calcU_s_a(double sqrtOfSumOfVisitCounts, @NotNull ActionEdge edge) {
        return GlobalGameConfig.getC_puctConstant() * edge.getPriorProbability() * (sqrtOfSumOfVisitCounts / (1 + edge.getVisitCount()));
    }

    IReinforcement getNextReinforcement() {
        IAction nextAction = getNextAction();
        if (nextAction instanceof IReinforcement) {
            return (IReinforcement) nextAction;
        } else {
            throw new RuntimeException();
        }
    }

    public GameBoard getGameBoard() {
        return gameBoard;
    }

    ActionEdge getChild(IAttack nextAttack) {
        return this.actionEdges.get(nextAttack);
    }

    ActionEdge getChild(IReinforcement nextReinforcement) {
        return this.actionEdges.get(nextReinforcement);
    }

    Set<ActionProbability<IAttack>> makeAttackProbabilities(double tau_inverse) {
        return this.<IAttack>makeActionProbabilities(actionEdges, tau_inverse)
                       .stream()
                       .filter(t -> gameBoard.canDo(t.getAction()))
                       .collect(Collectors.toSet());
    }

    private synchronized <T extends IAction> Set<ActionProbability<T>> makeActionProbabilities(Map<IAction, ActionEdge> edges, double tau_inverse) {
        final double sum = oneOr(edges.values().stream().mapToDouble(a -> Math.pow(a.getVisitCount(), tau_inverse)).sum());

        return edges.entrySet().stream()
                       .map(e -> {
                           double value = Math.pow(e.getValue().getVisitCount(), tau_inverse) / sum;

                           if (!Double.isFinite(value)) {
                               value = 0;
                           }

                           //noinspection unchecked
                           T action = (T) e.getKey();
                           return ActionProbability.of(value, action);
                       })
                       .collect(Collectors.toSet());
    }

    Set<ActionProbability<IReinforcement>> makeReinforcementProbs(double tau_inverse) {
        return this.<IReinforcement>makeActionProbabilities(actionEdges, tau_inverse)
                       .stream()
                       .filter(t -> gameBoard.canPerform(t.getAction()))
                       .collect(Collectors.toSet());
    }

    synchronized double getTotalActionValue() {
        return totalActionValue;
    }

    synchronized void addToTotalActionValue(double totalActionValue) {
        this.totalActionValue += totalActionValue;
    }

    synchronized int getVisitCount() {
        return visitCount;
    }

    synchronized void incrementVisitCount() {
        this.visitCount++;
    }
}
