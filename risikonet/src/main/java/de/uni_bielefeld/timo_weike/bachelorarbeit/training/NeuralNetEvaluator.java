/*
 *     bachelorthesis-risikonet
 *     Copyright (C) 2019  Timo Weike
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package de.uni_bielefeld.timo_weike.bachelorarbeit.training;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.concurrent.CompletableFuture;

import org.deeplearning4j.nn.graph.ComputationGraph;
import org.nd4j.linalg.api.ndarray.INDArray;
import org.nd4j.linalg.factory.Nd4j;

import de.uni_bielefeld.timo_weike.bachelorarbeit.game.GameBoard;
import de.uni_bielefeld.timo_weike.bachelorarbeit.game.IAttack;
import de.uni_bielefeld.timo_weike.bachelorarbeit.game.IReinforcement;
import de.uni_bielefeld.timo_weike.bachelorarbeit.util.BatchedEvaluator;

public class NeuralNetEvaluator implements IEvaluator {
    private final ComputationGraph computationGraph;
    private final BatchedEvaluator<INDArray, INDArray[]> evaluator;

    public NeuralNetEvaluator(ComputationGraph computationGraph) {
        this.computationGraph = computationGraph;
        this.evaluator = new BatchedEvaluator<>(32, l -> processData(l));
    }

    @Override
    public EvalResult<IAttack> evaluateForAttackPhase(GameBoard gameBoard) {
        final INDArray compiledState = gameBoard.packFor(gameBoard.getTurnState().getPlayerId());

        final INDArray[] output = this.evalParallel(compiledState).join();

        INDArray valueOut = output[0];
        INDArray attackOut = output[1];

        final float actionValue = GameBoardUtil.getActionValue(valueOut);

        final Set<ActionProbability<IAttack>> attackPrediction = GameBoardUtil.createAttackPrediction(attackOut, gameBoard, true);

        return new EvalResult<>(attackPrediction, actionValue);
    }

    public CompletableFuture<INDArray[]> evalParallel(INDArray input) {
        try {
            return evaluator.process(input);
        } catch (InterruptedException e) {
            e.printStackTrace();
            throw new RuntimeException(e);
        }
    }

    @Override
    public EvalResult<IReinforcement> evaluateForReinforcementPhase(GameBoard gameBoard) {
        final INDArray compiledState = gameBoard.packFor(gameBoard.getTurnState().getPlayerId());

        final INDArray[] output = this.evalParallel(compiledState).join();

        INDArray valueOut = output[0];
        INDArray reinforcementOut = output[2];

        final float actionValue = GameBoardUtil.getActionValue(valueOut);

        final Set<ActionProbability<IReinforcement>> reinforcementPrediction = GameBoardUtil.createReinforcementPrediction(reinforcementOut, gameBoard, true);

        return new EvalResult<>(reinforcementPrediction, actionValue);
    }

    private List<INDArray[]> processData(List<INDArray> in) {
        INDArray[] inArray = in.toArray(new INDArray[0]);

        INDArray stack = Nd4j.stack(0, inArray);
        INDArray[] output = computationGraph.output(stack);

        List<INDArray[]> outList = new ArrayList<>();

        if (in.size() <= 1) {
            INDArray valueOut = output[0];
            INDArray attackOut = output[1];
            INDArray reinforcementOut = output[2];

            compileResult(outList, valueOut, attackOut, reinforcementOut);
        } else {
            for (int i = 0; i < in.size(); i++) {
                INDArray valueOut = output[0].slice(i);
                INDArray attackOut = output[1].slice(i);
                INDArray reinforcementOut = output[2].slice(i);

                compileResult(outList, valueOut, attackOut, reinforcementOut);
            }
        }

        return outList;
    }

    private void compileResult(List<INDArray[]> outList, INDArray valueOut, INDArray attackOut, INDArray reinforcementOut) {
        valueOut = valueOut.reshape(valueOut.length());
        attackOut = attackOut.reshape(attackOut.length());
        reinforcementOut = reinforcementOut.reshape(reinforcementOut.length());

        INDArray[] outs = {valueOut, attackOut, reinforcementOut};

        outList.add(outs);
    }
}
