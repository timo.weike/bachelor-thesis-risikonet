/*
 *     bachelorthesis-risikonet
 *     Copyright (C) 2019  Timo Weike
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package de.uni_bielefeld.timo_weike.bachelorarbeit.programs;

import java.io.File;
import java.util.Comparator;
import java.util.Set;
import java.util.stream.IntStream;

import org.apache.commons.io.FilenameUtils;
import org.deeplearning4j.nn.graph.ComputationGraph;
import org.nd4j.linalg.api.ndarray.INDArray;
import org.nd4j.linalg.primitives.ImmutablePair;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import de.uni_bielefeld.timo_weike.bachelorarbeit.game.*;
import de.uni_bielefeld.timo_weike.bachelorarbeit.training.*;
import de.uni_bielefeld.timo_weike.bachelorarbeit.util.MyFileUtils;
import de.uni_bielefeld.timo_weike.bachelorarbeit.util.Numbers;

import static de.uni_bielefeld.timo_weike.bachelorarbeit.training.Utils.*;
import static org.nd4j.linalg.primitives.ImmutablePair.of;

public class ModelEvaluationGameMain {
    private static final Logger log = LoggerFactory.getLogger(ModelEvaluationGameMain.class);

    private static final int GAME_COUNT = 100;
    private static final int TEST_COUNT_MEASURE_CORRECTNESS = 1000;

    public static void main(String[] args) {
        final File currentlyBestModel = getBestModel();
        final File currentlyLatestModel = getLatestModel();

        if (currentlyBestModel == null && currentlyLatestModel != null) {
            log.info("no currently best found => make latest best");

            String fileName = MyFileUtils.getTimeStampedFile(MODEL_BASE_NAME, BEST_MODEL_EXTENSION);
            String filePath = FilenameUtils.concat(MODELS_DIR, fileName);

            makeLatestBest(new File(filePath), currentlyLatestModel);
        } else if (currentlyLatestModel == null) {
            log.error("Can not find a latest model");
        } else {
            log.info(String.format("Loaded %s as best model", currentlyBestModel.getAbsolutePath()));
            final ComputationGraph bestModel = NeuralNetBuilder.getComputationGraph(currentlyBestModel);

            log.info(String.format("Loaded %s as latest model", currentlyLatestModel.getAbsolutePath()));
            final ComputationGraph latestModel = NeuralNetBuilder.getComputationGraph(currentlyLatestModel);

            final NeuralNetEvaluator bestNetEvaluator = new NeuralNetEvaluator(bestModel);
            final NeuralNetEvaluator latestNetEvaluator = new NeuralNetEvaluator(latestModel);

            int winsLatest = playAgainstBest(bestNetEvaluator, latestNetEvaluator);

            log.info(String.format("RES:The best model has won %d out of %d Games", GAME_COUNT - winsLatest, GAME_COUNT));
            log.info(String.format("RES:The latest model has won %d out of %d Games", winsLatest, GAME_COUNT));

            measureCorrectness(bestNetEvaluator, latestNetEvaluator);

            if (GAME_COUNT - winsLatest <= winsLatest) {
                log.info("RES:So the latest model has won");
                makeLatestBest(currentlyBestModel, currentlyLatestModel);
            }
        }

        System.exit(0);
    }

    public static int playAgainstBest(NeuralNetEvaluator bestNetEvaluator, NeuralNetEvaluator latestNetEvaluator) {
        int winsLatest = 0;

        for (int i = 0; i < GAME_COUNT; i++) {
            log.info(String.format("Starting Game %d", i));
            boolean bestWon = playGame(bestNetEvaluator, latestNetEvaluator, i);
            log.info(String.format("Finished Game %d", i));

            if (!bestWon) {
                winsLatest++;
            }
        }
        return winsLatest;
    }

    private static void measureCorrectness(final NeuralNetEvaluator bestNetEvaluator,
                                           final NeuralNetEvaluator latestNetEvaluator) {
        ImmutablePair<Res, Res> reduce
                = IntStream.range(0, TEST_COUNT_MEASURE_CORRECTNESS)
                          .mapToObj(i -> {
                              GameBoard gameBoard = new GameBoard();
                              gameBoard.init();

                              INDArray input = gameBoard.packFor(PlayerId.FirstPlayer);
                              Res resBest = testWith(input, bestNetEvaluator, gameBoard);
                              Res resLatest = testWith(input, latestNetEvaluator, gameBoard);

                              return of(resBest, resLatest);
                          })
                          .reduce(of(Res.ZERO, Res.ZERO),
                                  (a, b) -> of(Res.add(a.getFirst(), b.getFirst()), Res.add(a.getSecond(), b.getSecond())));

        Res resultBest = reduce.getFirst();
        int attackAccBest = resultBest.correctAttacks;
        int passAttacksBest = resultBest.passAttackCount;
        int reinfAccBest = resultBest.correctReinforcements;
        int passReinfBest = resultBest.passReinforcementCount;
        double avgProbMassOfCorrectAttacksBest = resultBest.probMassOfCorrectAttacks / TEST_COUNT_MEASURE_CORRECTNESS;
        double avgProbMassOfCorrectReinforcementBest = resultBest.probMassOfCorrectReinforcements / TEST_COUNT_MEASURE_CORRECTNESS;

        Res resultLatest = reduce.getSecond();
        int attackAccLatest = resultLatest.correctAttacks;
        int passAttacksLatest = resultLatest.passAttackCount;
        int reinfAccLatest = resultLatest.correctReinforcements;
        int passReinfLatest = resultLatest.passReinforcementCount;
        double avgProbMassOfCorrectAttacksLatest = resultLatest.probMassOfCorrectAttacks / TEST_COUNT_MEASURE_CORRECTNESS;
        double avgProbMassOfCorrectReinforcementLatest = resultLatest.probMassOfCorrectReinforcements / TEST_COUNT_MEASURE_CORRECTNESS;

        log.info(String.format("RES:bestNet   Attack acc        %6d of %d correct %.4g and %6d passes of %6d attacks",
                attackAccBest, TEST_COUNT_MEASURE_CORRECTNESS, Numbers.div(attackAccBest, TEST_COUNT_MEASURE_CORRECTNESS), passAttacksBest, attackAccBest));
        log.info(String.format("RES:bestNet   Reinforcement acc %6d of %d correct %.4g and %6d passes of %6d reinforcements",
                reinfAccBest, TEST_COUNT_MEASURE_CORRECTNESS, Numbers.div(reinfAccBest, TEST_COUNT_MEASURE_CORRECTNESS), passReinfBest, reinfAccBest));
        log.info(String.format("RES:bestNet   AvgProbMass for correct attacks        %.4g", avgProbMassOfCorrectAttacksBest));
        log.info(String.format("RES:bestNet   AvgProbMass for correct reinforcements %.4g", avgProbMassOfCorrectReinforcementBest));
        log.info(String.format("RES:latestNet Attack acc        %6d of %d correct %.4g and %6d passes of %6d attacks",
                attackAccLatest, TEST_COUNT_MEASURE_CORRECTNESS, Numbers.div(attackAccLatest, TEST_COUNT_MEASURE_CORRECTNESS), passAttacksLatest, attackAccLatest));
        log.info(String.format("RES:latestNet Reinforcement acc %6d of %d correct %.4g and %6d passes of %6d reinforcements",
                reinfAccLatest, TEST_COUNT_MEASURE_CORRECTNESS, Numbers.div(reinfAccLatest, TEST_COUNT_MEASURE_CORRECTNESS), passReinfLatest, reinfAccLatest));
        log.info(String.format("RES:latestNet AvgProbMass for correct attacks        %.4g", avgProbMassOfCorrectAttacksLatest));
        log.info(String.format("RES:latestNet AvgProbMass for correct reinforcements %.4g", avgProbMassOfCorrectReinforcementLatest));
    }

    /**
     * Lets the both NeuralNetEvaluators play a game against each other and return true if the bestNetEvaluator
     * won the game.
     *
     * @param bestNetEvaluator
     * @param latestNetEvaluator
     * @return
     */
    private static boolean playGame(NeuralNetEvaluator bestNetEvaluator, NeuralNetEvaluator latestNetEvaluator, int gameNumber) {
        AiPlayer best1 = new AiPlayer(PlayerId.FirstPlayer, bestNetEvaluator);
        AiPlayer best2 = new AiPlayer(PlayerId.SecondPlayer, bestNetEvaluator);
        AiPlayer latest1 = new AiPlayer(PlayerId.ThirdPlayer, latestNetEvaluator);
        AiPlayer latest2 = new AiPlayer(PlayerId.FourthPlayer, latestNetEvaluator);

        AiPlayer[] playerArray = {best1, best2, latest1, latest2};
        int[] idxArray = {0, 1, 2, 3};
        shuffleArray(idxArray);

        GameBoard gb = new GameBoard();

        gb.init();

        log.info(String.format("Players:%n\tFirstPlayer %s%n\tSecondPlayer %s%n\tThirdPlayer %s%n\tFourthPlayer %s",
                idxArray[0] <= 1 ? "best" : "latest",
                idxArray[1] <= 1 ? "best" : "latest",
                idxArray[2] <= 1 ? "best" : "latest",
                idxArray[3] <= 1 ? "best" : "latest"));

        final TrainingGame tg = new TrainingGame(
                playerArray[idxArray[0]],
                playerArray[idxArray[1]],
                playerArray[idxArray[2]],
                playerArray[idxArray[3]],
                gb);

        final PlayerId winner = tg.play(gameNumber);

        return idxArray[PlayerUtil.getPlayerNum(winner) - 1] <= 1;
    }

    private static Res testWith(INDArray input, NeuralNetEvaluator evaluator, GameBoard board) {
        INDArray[] output = evaluator.evalParallel(input).join();

        Set<ActionProbability<IAttack>> attackPrediction = GameBoardUtil.createAttackPrediction(output[1], board, false);
        IAttack bestAttack = attackPrediction
                                     .stream()
                                     .max(Comparator.comparingDouble(t -> t.getProbability()))
                                     .map(t -> t.getAction())
                                     .get();

        double probMassOfCorrectAttacks = attackPrediction.stream()
                                                  .filter(p -> board.canDo(p.getAction()))
                             .mapToDouble(p -> p.getProbability())
                             .sum();

        Set<ActionProbability<IReinforcement>> reinforcementPrediction = GameBoardUtil.createReinforcementPrediction(output[2], board, false);
        IReinforcement bestReinforcement = reinforcementPrediction
                                                   .stream()
                                                   .max(Comparator.comparingDouble(t -> t.getProbability()))
                                                   .map(t -> t.getAction())
                                                   .get();

        double probMassOfCorrectReinforcements = reinforcementPrediction.stream()
                                                         .filter(p -> board.canPerform(p.getAction()))
                             .mapToDouble(p -> p.getProbability())
                             .sum();

        int isAttackCorrect = board.canDo(bestAttack) ? 1 : 0;
        int isReinforcementCorrect = board.canPerform(bestReinforcement) ? 1 : 0;
        int isPassAttack = bestAttack.isPhaseEnding() ? 1 : 0;
        int isPassReinforcement = bestReinforcement.isPhaseEnding() ? 1 : 0;

        return new Res(isAttackCorrect, isReinforcementCorrect, isPassAttack, isPassReinforcement, probMassOfCorrectAttacks, probMassOfCorrectReinforcements);
    }

    private static class Res {
        public static final Res ZERO = new Res(0, 0, 0, 0, 0, 0);
        public final int correctAttacks;
        public final int correctReinforcements;
        public final int passAttackCount;
        public final int passReinforcementCount;
        private final double probMassOfCorrectAttacks;
        private final double probMassOfCorrectReinforcements;

        public Res(int isAttackCorrect, int isReinforcementCorrect, int isPassAttack, int isPassReinforcement, double probMassOfCorrectAttacks, double probMassOfCorrectReinforcements) {
            this.correctAttacks = isAttackCorrect;
            this.correctReinforcements = isReinforcementCorrect;
            this.passAttackCount = isPassAttack;
            this.passReinforcementCount = isPassReinforcement;
            this.probMassOfCorrectAttacks = probMassOfCorrectAttacks;
            this.probMassOfCorrectReinforcements = probMassOfCorrectReinforcements;
        }

        public static Res add(Res a, Res b) {
            return new Res(a.correctAttacks + b.correctAttacks,
                    a.correctReinforcements + b.correctReinforcements,
                    a.passAttackCount + b.passAttackCount,
                    a.passReinforcementCount + b.passReinforcementCount,
                    a.probMassOfCorrectAttacks + b.probMassOfCorrectAttacks,
                    a.probMassOfCorrectReinforcements + b.probMassOfCorrectReinforcements);
        }
    }

}
