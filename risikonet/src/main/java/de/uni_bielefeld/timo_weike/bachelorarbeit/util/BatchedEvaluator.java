/*
 *     bachelorthesis-risikonet
 *     Copyright (C) 2019  Timo Weike
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package de.uni_bielefeld.timo_weike.bachelorarbeit.util;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.CompletableFuture;
import java.util.function.Function;

/**
 * This class enables batched evaluation of incoming data from multiple threads of a given function.
 *
 * @param <I> the type of the incoming data
 * @param <O> the type of the outgoing data
 */
public class BatchedEvaluator<I, O> {
    private final BlockingQueue<IOPair> inputOutputPairs;
    private final Runner runner;
    private final Thread runnerThread;

    /**
     * Creates a BatchedEvaluator for the given function and a maximal queue size.
     * The order of the data in the input and output lists of the function must be a one to one correspondence.
     * If the data point d is at position i in the input-list, this class expectes the correspoding result to be at
     * position i in the output-list.
     *
     * @param size maximal size of the input-queue
     * @param func the function to evaluate
     */
    public BatchedEvaluator(int size, Function<List<I>, List<O>> func) {
        inputOutputPairs = new ArrayBlockingQueue<>(size);

        runner = new Runner(func);
        runnerThread = new Thread(runner, "RunnerThread");
        runnerThread.setDaemon(true);
        runnerThread.start();
    }

    /**
     * Creates a Futere which will be complete as soon the given data are processed.
     *
     * @param input
     * @return
     * @throws InterruptedException if the evaluation thread is interrupted
     */
    public CompletableFuture<O> process(I input) throws InterruptedException {
        CompletableFuture<O> future = new CompletableFuture<>();

        inputOutputPairs.put(new IOPair(input, future));

        return future;
    }

    private class Runner implements Runnable {
        private final Function<List<I>, List<O>> func;

        Runner(Function<List<I>, List<O>> func) {
            this.func = func;
        }

        @Override
        public void run() {
            try {
                //noinspection InfiniteLoopStatement
                while (true) {
                    // waits for one item to process
                    IOPair take = inputOutputPairs.take();

                    // takes all items that should be processed
                    List<IOPair> list = new ArrayList<>();
                    inputOutputPairs.drainTo(list);
                    list.add(take);

                    List<I> inputs = new ArrayList<>();
                    List<CompletableFuture<O>> futures = new ArrayList<>();

                    for (IOPair ioPair : list) {
                        inputs.add(ioPair.getInput());
                        futures.add(ioPair.getFuture());
                    }

                    // processes the data
                    List<O> outputs = func.apply(inputs);

                    // completes the corresponding Future objects.
                    for (int i = 0; i < list.size(); i++) {
                        futures.get(i).complete(outputs.get(i));
                    }
                }
            } catch (InterruptedException e) {
                Thread.currentThread().interrupt();
            }
        }
    }

    /**
     * Wrapper class for input data and its corresponding Future object.
     */
    private class IOPair {
        private final I input;
        private final CompletableFuture<O> future;

        public IOPair(I in, CompletableFuture<O> future) {
            this.input = in;
            this.future = future;
        }

        public I getInput() {
            return input;
        }

        public CompletableFuture<O> getFuture() {
            return future;
        }
    }
}
