/*
 *     bachelorthesis-risikonet
 *     Copyright (C) 2019  Timo Weike
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package de.uni_bielefeld.timo_weike.bachelorarbeit.game;

/**
 * A cell of the game board.
 */
public class Cell {

    private final int value;
    private final PlayerId owner;

    public Cell(int value, PlayerId owner) {
        this.value = value;
        this.owner = owner;
    }

    public static Cell of(int value, PlayerId owner) {
        return new Cell(value, owner);
    }

    /**
     * Gets the value of the cell.
     *
     * @return the value of the cell
     */
    public int getValue() {
        return value;
    }

    /**
     * Gets the PlayerId of the owner
     *
     * @return the PlayerId of the owner
     */
    public PlayerId getOwner() {
        return owner;
    }

    @Override
    public String toString() {
        return "Cell{" +
                       "value=" + value +
                       ", owner=" + owner +
                       '}';
    }
}
