/*
 *     bachelorthesis-risikonet
 *     Copyright (C) 2019  Timo Weike
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package de.uni_bielefeld.timo_weike.bachelorarbeit.training;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import org.deeplearning4j.nn.conf.ComputationGraphConfiguration;
import org.deeplearning4j.nn.conf.ConvolutionMode;
import org.deeplearning4j.nn.conf.InputPreProcessor;
import org.deeplearning4j.nn.conf.NeuralNetConfiguration;
import org.deeplearning4j.nn.conf.graph.ElementWiseVertex;
import org.deeplearning4j.nn.conf.inputs.InputType;
import org.deeplearning4j.nn.conf.layers.*;
import org.deeplearning4j.nn.conf.preprocessor.CnnToFeedForwardPreProcessor;
import org.deeplearning4j.nn.graph.ComputationGraph;
import org.deeplearning4j.nn.weights.WeightInit;
import org.nd4j.linalg.activations.Activation;
import org.nd4j.linalg.learning.config.Sgd;
import org.nd4j.linalg.lossfunctions.LossFunctions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class NeuralNetBuilder {
    private static final Logger log = LoggerFactory.getLogger(NeuralNetBuilder.class);

    public static final int NODE_COUNT = 256;
    private final int rowCount;
    private final int colCount;
    private ComputationGraphConfiguration.GraphBuilder conf;
    private int[] strides;
    private int[] kernelSize;
    private ConvolutionMode convolutionMode;

    public NeuralNetBuilder(int layerCount, int rowCount, int colCount, int[] kernel, int[] strides, ConvolutionMode mode) {
        this.rowCount = rowCount;
        this.colCount = colCount;
        this.kernelSize = kernel;
        this.strides = strides;
        this.convolutionMode = mode;

        this.conf = new NeuralNetConfiguration.Builder()
                            .updater(new Sgd())
                            .l2(0.0001)
                            .weightInit(WeightInit.LECUN_NORMAL)
                            .miniBatch(true)
                            .graphBuilder().setInputTypes(InputType.convolutional(rowCount, colCount, layerCount));
    }

    public void addInputs(String name) {
        conf.addInputs(name);
    }

    public void addOutputs(String... names) {
        conf.setOutputs(names);
    }

    public ComputationGraphConfiguration buildAndReturn() {
        return conf.build();
    }

    /**
     * Building a tower of residual blocks.
     */
    public String addResidualTower(int numBlocks, String inName, String prefix) {
        String name = inName;
        for (int i = 0; i < numBlocks; i++) {
            name = addResidualBlock(i, name, prefix);
        }
        return name;
    }

    /**
     * Residual block for AGZ. Takes two conv-bn-relu blocks
     * and adds them to the original input.
     */
    public String addResidualBlock(int blockNumber, String inName, String prefix) {
        String firstBlock = prefix + "-residual-1-" + blockNumber;
        String secondBlock = prefix + "-residual-2-" + blockNumber;
        String mergeBlock = prefix + "-add-" + blockNumber;
        String actBlock = prefix + "-relu-" + blockNumber;

        String firstBnOut =
                addConvBatchNormBlock(firstBlock, inName, NODE_COUNT, true, prefix);
        String secondBnOut =
                addConvBatchNormBlock(secondBlock, firstBnOut, NODE_COUNT, false, prefix);
        conf.addVertex(mergeBlock, new ElementWiseVertex(ElementWiseVertex.Op.Add), firstBnOut, secondBnOut);
        conf.addLayer(actBlock, new ActivationLayer.Builder().activation(Activation.RELU).build(), mergeBlock);
        return actBlock;
    }

    /**
     * Building block for AGZ residual blocks.
     * conv2d -> batch norm -> ReLU
     */
    public String addConvBatchNormBlock(String blockName, String inName, int nIn,
                                        boolean useActivation, String prefix) {
        String convName = prefix + "-conv-" + blockName;
        String bnName = prefix + "-batch-norm-" + blockName;
        String actName = prefix + "-relu-" + blockName;

        conf.addLayer(convName,
                new ConvolutionLayer.Builder()
                        .kernelSize(kernelSize)
                        .stride(strides)
                        .convolutionMode(convolutionMode)
                        .nIn(nIn)
                        .nOut(NODE_COUNT)
                        .build(),
                inName);
        conf.addLayer(bnName,
                new BatchNormalization.Builder()
                        .nOut(NODE_COUNT)
                        .build(),
                convName);

        if (useActivation) {
            conf.addLayer(actName,
                    new ActivationLayer.Builder()
                            .activation(Activation.RELU)
                            .build(),
                    bnName);
            return actName;
        } else
            return bnName;
    }

    public String addAttackHead(String inName) {
        final String attack_head = "attack-head-";

        final String convName = attack_head + "conv";
        final String bnName = attack_head + "norm";
        final String actName = attack_head + "relu";
        final String denseName = attack_head + "output";

        conf.addLayer(convName,
                new ConvolutionLayer.Builder(kernelSize)
                        .stride(strides)
                        .convolutionMode(convolutionMode)
                        .nOut(2)
                        .nIn(NODE_COUNT)
                        .build()
                , inName);
        conf.addLayer(bnName,
                new BatchNormalization.Builder()
                        .nOut(2)
                        .build()
                , convName);
        conf.addLayer(actName,
                new ActivationLayer.Builder()
                        .activation(Activation.RELU)
                        .build(),
                bnName);
        conf.addLayer(denseName,
                new OutputLayer.Builder(new CrossEntropyLoss())
                        .activation(Activation.SOFTMAX)
                        .nIn(2 * rowCount * colCount)
                        .nOut(6 * rowCount * colCount + 1)
                        .build(),
                actName);

        Map<String, InputPreProcessor> preProcessorMap = new HashMap<>();
        preProcessorMap.put(denseName, new CnnToFeedForwardPreProcessor(rowCount, colCount, 2));
        conf.setInputPreProcessors(preProcessorMap);
        return denseName;
    }

    public String addReinforcementHead(String inName) {
        final String reinforcement_head = "reinforcement-head-";

        final String convName = reinforcement_head + "conv";
        final String bnName = reinforcement_head + "norm";
        final String actName = reinforcement_head + "relu";
        final String denseName = reinforcement_head + "output";

        conf.addLayer(convName,
                new ConvolutionLayer.Builder(kernelSize)
                        .stride(strides)
                        .convolutionMode(convolutionMode)
                        .nOut(2)
                        .nIn(NODE_COUNT)
                        .build()
                , inName);
        conf.addLayer(bnName,
                new BatchNormalization.Builder()
                        .nOut(2)
                        .build()
                , convName);
        conf.addLayer(actName,
                new ActivationLayer.Builder()
                        .activation(Activation.RELU)
                        .build(),
                bnName);
        conf.addLayer(denseName,
                new OutputLayer.Builder(new CrossEntropyLoss())
                        .activation(Activation.SOFTMAX)
                        .nIn(2 * rowCount * colCount)
                        .nOut(rowCount * colCount + 1)
                        .build(),
                actName);

        Map<String, InputPreProcessor> preProcessorMap = new HashMap<>();
        preProcessorMap.put(denseName, new CnnToFeedForwardPreProcessor(rowCount, colCount, 2));
        conf.setInputPreProcessors(preProcessorMap);
        return denseName;
    }

    public String addValueHead(String inName) {
        final String value_head_ = "value-head-";

        final String convName = value_head_ + "conv-";
        final String bnName = value_head_ + "batch-norm-";
        final String actName = value_head_ + "relu-";
        final String denseName = value_head_ + "dense-";
        final String outputName = value_head_ + "output-";

        conf.addLayer(convName,
                new ConvolutionLayer.Builder(kernelSize)
                        .stride(strides)
                        .convolutionMode(convolutionMode)
                        .nOut(1)
                        .nIn(NODE_COUNT)
                        .build(),
                inName);
        conf.addLayer(bnName,
                new BatchNormalization.Builder()
                        .nOut(1)
                        .build(),
                convName);
        conf.addLayer(actName,
                new ActivationLayer.Builder()
                        .activation(Activation.RELU)
                        .build(),
                bnName);
        conf.addLayer(denseName,
                new DenseLayer.Builder()
                        .nIn(rowCount * colCount)
                        .nOut(NODE_COUNT)
                        .build(),
                actName);

        conf.addLayer(outputName,
                new OutputLayer.Builder(LossFunctions.LossFunction.MSE)
                        .activation(Activation.TANH)
                        .nIn(NODE_COUNT)
                        .nOut(1)
                        .build(),
                denseName);

        Map<String, InputPreProcessor> preProcessorMap = new HashMap<>();
        preProcessorMap.put(denseName, new CnnToFeedForwardPreProcessor(rowCount, colCount, 1));
        conf.setInputPreProcessors(preProcessorMap);

        return outputName;
    }

    public static ComputationGraph getComputationGraph(File file) {
        if (file != null && file.exists()) {
            try {
                log.info(String.format("Load model from file %s", file.getCanonicalPath()));
                return ComputationGraph.load(file, true);
            } catch (IOException e) {
                e.printStackTrace();
                System.exit(999);
                return null;
            }
        } else {
            log.info("file is null or does not exists");
            System.exit(1);
            throw new IllegalStateException("Should not be here");
        }
    }
}
