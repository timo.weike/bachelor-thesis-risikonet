/*
 *     bachelorthesis-risikonet
 *     Copyright (C) 2019  Timo Weike
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package de.uni_bielefeld.timo_weike.bachelorarbeit.training;

import java.io.File;

import org.apache.commons.lang3.time.StopWatch;
import org.deeplearning4j.nn.graph.ComputationGraph;

import de.uni_bielefeld.timo_weike.bachelorarbeit.game.GameBoard;
import de.uni_bielefeld.timo_weike.bachelorarbeit.game.PlayerId;

import static de.uni_bielefeld.timo_weike.bachelorarbeit.training.Utils.getBestModel;

public class TestGame {

    public static final int TEST_GAME_COUNT = 100;

    public static void main(String[] args) {
        StopWatch st = new StopWatch();
        st.reset();

        final File currentlyBestModel = getBestModel();
        final ComputationGraph bestModel = NeuralNetBuilder.getComputationGraph(currentlyBestModel);

        final NeuralNetEvaluator bestNetEvaluator = new NeuralNetEvaluator(bestModel);

        final DesignedEvaluator designedEvaluator = new DesignedEvaluator();

        int netWon = testAgainst(bestNetEvaluator, designedEvaluator);

        System.out.println(String.format("The nn won %d out of %d games against the designed AI", netWon, TEST_GAME_COUNT));

        final RandomChooser randomChooser = new RandomChooser();

        netWon = testAgainst(bestNetEvaluator, randomChooser);

        System.out.println(String.format("The nn won %d out of %d games against the random chooser", netWon, TEST_GAME_COUNT));

        System.exit(0);
    }

    private static int testAgainst(NeuralNetEvaluator neuralNetEvaluator, IEvaluator evaluator) {
        int netWon = 0;

        for (int i = 0; i < TEST_GAME_COUNT; i++) {
            AiPlayer p1 = new AiPlayer(PlayerId.FirstPlayer, neuralNetEvaluator);
            AiPlayer p2 = new AiPlayer(PlayerId.SecondPlayer, evaluator);
            AiPlayer p3 = new AiPlayer(PlayerId.ThirdPlayer, evaluator);
            AiPlayer p4 = new AiPlayer(PlayerId.FourthPlayer, evaluator);

            GameBoard gb = new GameBoard();

            gb.init();

//            gb.validate(gb, Action.getReinforcement(of(0,0)));

            TrainingGame tg = new TrainingGame(p1, p2, p3, p4, gb);

            final PlayerId winner = tg.play(i);

            if (winner == PlayerId.FirstPlayer) {
                netWon++;
            }
        }
        return netWon;
    }
}
