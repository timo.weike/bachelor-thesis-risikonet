/*
 *     bachelorthesis-risikonet
 *     Copyright (C) 2019  Timo Weike
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package de.uni_bielefeld.timo_weike.bachelorarbeit.game;

import java.util.List;
import java.util.Set;

import org.nd4j.linalg.api.ndarray.INDArray;
import org.nd4j.linalg.factory.Nd4j;

import de.uni_bielefeld.timo_weike.bachelorarbeit.util.Numbers;

import static de.uni_bielefeld.timo_weike.bachelorarbeit.game.Cell.of;
import static de.uni_bielefeld.timo_weike.bachelorarbeit.util.Numbers.oneOr;

public class PlayerArmys {
    private PlayerArmy neutralBoard;
    private PlayerArmy player1Board;
    private PlayerArmy player2Board;
    private PlayerArmy player3Board;
    private PlayerArmy player4Board;

    private PlayerArmys(PlayerArmys another) {
        neutralBoard = new PlayerArmy(another.neutralBoard);
        player1Board = new PlayerArmy(another.player1Board);
        player2Board = new PlayerArmy(another.player2Board);
        player3Board = new PlayerArmy(another.player3Board);
        player4Board = new PlayerArmy(another.player4Board);
    }

    public PlayerArmys()
    {
        neutralBoard = new PlayerArmy();
        player1Board = new PlayerArmy();
        player2Board = new PlayerArmy();
        player3Board = new PlayerArmy();
        player4Board = new PlayerArmy();

    }

    public Cell getValueFor(Position position)
    {
        if (!position.isInBounds())
            return null;

        int value = player1Board.getValueFor(position);

        if (0 < value)
            return of(value, PlayerId.FirstPlayer);
        value = player2Board.getValueFor(position);
        if (0 < value)
            return of(value, PlayerId.SecondPlayer);
        value = player3Board.getValueFor(position);
        if (0 < value)
            return of(value, PlayerId.ThirdPlayer);
        value = player4Board.getValueFor(position);
        if (0 < value)
            return of(value, PlayerId.FourthPlayer);

        value = neutralBoard.getValueFor(position);

        return of(value, PlayerId.Neutral);
    }

    public PlayerArmy getArmyForPlayer(PlayerId playerId)
    {
        switch (playerId) {
            case Neutral: return neutralBoard;
            case FirstPlayer: return player1Board;
            case SecondPlayer: return  player2Board;
            case ThirdPlayer: return player3Board;
            case FourthPlayer: return player4Board;
        }

        throw new IllegalArgumentException("not in enum");
    }

    void setValueForPlayerAt(int value, PlayerId playerId, Position position) {
        PlayerArmy playerBoard = getArmyForPlayer(playerId);

        playerBoard.setValueAt(position, value);
    }

    boolean isPlayerAlive(PlayerId playerId)
    {
        return getArmyForPlayer(playerId).isAlive();
    }

    boolean canPlayerAttack(PlayerId playerId) {
        return getArmyForPlayer(playerId).canAttack();
    }

    public List<IAttack> possibleAttacksFor(PlayerId playerId) {
        return this.getArmyForPlayer(playerId).calcPossibleMoves();
    }

    public int reinforcementPointsForPlayer(PlayerId playerId) {
        return this.getArmyForPlayer(playerId).reinforcementPoints();
    }

    public void reinforceRandomForPlayer(PlayerId playerId, int reinforcementPoints) {
        this.getArmyForPlayer(playerId).reinforceRandom(reinforcementPoints);
    }

    public INDArray packFor(PlayerId playerId) {
        INDArray[] armys = new INDArray[5];
        armys[0] = this.getArmyForPlayer(playerId).getRawData();
        armys[1] = this.getArmyForPlayer(PlayerId.Neutral).getRawData();

        PlayerId scd, thr, fth;
        scd = thr = fth = PlayerId.Neutral;

        switch (playerId) {
            case Neutral:
                throw new IllegalArgumentException("Neural can not make moves and should not call this methode");
            case FirstPlayer:
                scd = PlayerId.SecondPlayer;
                thr = PlayerId.ThirdPlayer;
                fth = PlayerId.FourthPlayer;
                break;
            case SecondPlayer:
                scd = PlayerId.ThirdPlayer;
                thr = PlayerId.FourthPlayer;
                fth = PlayerId.FirstPlayer;
                break;
            case ThirdPlayer:
                scd = PlayerId.FourthPlayer;
                thr = PlayerId.FirstPlayer;
                fth = PlayerId.SecondPlayer;
                break;
            case FourthPlayer:
                scd = PlayerId.FirstPlayer;
                thr = PlayerId.SecondPlayer;
                fth = PlayerId.ThirdPlayer;
                break;
        }

        armys[2] = this.getArmyForPlayer(scd).getRawData();
        armys[3] = this.getArmyForPlayer(thr).getRawData();
        armys[4] = this.getArmyForPlayer(fth).getRawData();

        return Nd4j.stack(0, armys).div(9);
    }

    public PlayerArmys dup() {
        return new PlayerArmys(this);
    }

    public Set<IReinforcement> possibleReinforcementsFor(PlayerId playerId) {
        return this.getArmyForPlayer(playerId).possibleReinforcements();
    }

    public double getAreaPercentFor(PlayerId playerId) {
        final double areaForP1 = this.reinforcementPointsForPlayer(PlayerId.FirstPlayer);
        final double areaForP2 = this.reinforcementPointsForPlayer(PlayerId.SecondPlayer);
        final double areaForP3 = this.reinforcementPointsForPlayer(PlayerId.ThirdPlayer);
        final double areaForP4 = this.reinforcementPointsForPlayer(PlayerId.FourthPlayer);

        return relativeFor(playerId, areaForP1, areaForP2, areaForP3, areaForP4);
    }

    public double getRelativeStrengthFor(PlayerId playerId) {
        final double strengthOfP1 = this.totalStrengthOf(PlayerId.FirstPlayer);
        final double strengthOfP2 = this.totalStrengthOf(PlayerId.SecondPlayer);
        final double strengthOfP3 = this.totalStrengthOf(PlayerId.ThirdPlayer);
        final double strengthOfP4 = this.totalStrengthOf(PlayerId.FourthPlayer);

        return relativeFor(playerId, strengthOfP1, strengthOfP2, strengthOfP3, strengthOfP4);
    }

    private static double relativeFor(PlayerId playerId, double valuePlayer1, double valuePlayer2, double valuePlayer3, double valuePlayer4) {
        final double sum = oneOr(valuePlayer1 + valuePlayer2 + valuePlayer3 + valuePlayer4);

        switch (playerId) {
            case FirstPlayer:
                return valuePlayer1 / sum;
            case SecondPlayer:
                return valuePlayer2 / sum;
            case ThirdPlayer:
                return valuePlayer3 / sum;
            case FourthPlayer:
                return valuePlayer4 / sum;
        }

        throw new IllegalStateException("Excetution should not be here");
    }

    private int totalStrengthOf(PlayerId playerId) {
        return this.getArmyForPlayer(playerId).getTotalStrength();
    }

    public boolean canPlayerReinforce(PlayerId playerId, IReinforcement reinforcement) {
        if (reinforcement.isPhaseEnding()) {
            return true;
        } else {
            final Cell cell = this.getValueFor(reinforcement.getPosition());

            return cell.getOwner() == playerId
                    && Numbers.inRange(cell.getValue(), 1, 8);
        }
    }

    public String printFor(PlayerId playerId) {
        return this.getArmyForPlayer(playerId).toString();
    }
}
