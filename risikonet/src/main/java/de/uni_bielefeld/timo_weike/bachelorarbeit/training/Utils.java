package de.uni_bielefeld.timo_weike.bachelorarbeit.training;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Iterator;
import java.util.Random;
import java.util.concurrent.ThreadLocalRandom;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import org.deeplearning4j.nn.graph.ComputationGraph;
import org.jetbrains.annotations.Nullable;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import de.uni_bielefeld.timo_weike.bachelorarbeit.util.MyFileUtils;

public final class Utils {
    private static final Logger log = LoggerFactory.getLogger(Utils.class);

    public static final String MODELS_DIR = "models";
    public static final String MODEL_BASE_NAME = "model";
    public static final String MODEL_EXTENSION = "nnm";
    public static final String BEST_MODEL_EXTENSION = "bnnm";
    public static final String DATA_DIR = "data";
    public static final String CHECKPOINTS_DIR = "checkpoints";
    public static final String GAME_DATA_FILEBASE_NAME = "game-data";
    public static final String GAME_DATA_EXTENSION = "rnd";
    public static final String STATS_DIR = "stats";
    public static final String STATS_FILE_BASE_NAME = "stats-store";
    public static final String STATS_FILE_EXTENSION = "sts";
    public static final String DATA_BACKUP_DIR = "dataBackup";
    public static final String ZIP_EXTENSION = "zip";

    static {
        try {
            Files.createDirectories(Paths.get(MODELS_DIR));
            Files.createDirectories(Paths.get(DATA_DIR));
            Files.createDirectories(Paths.get(CHECKPOINTS_DIR));
            Files.createDirectories(Paths.get(STATS_DIR));
            Files.createDirectories(Paths.get(DATA_BACKUP_DIR));
        } catch (IOException e) {
            e.printStackTrace();
            System.exit(369);
        }
    }

    private Utils() {
    }

    public static File getBestModel() {
        return getModelWth(BEST_MODEL_EXTENSION);
    }

    public static <T> void shuffleArray(T[] ar)
    {
        Random prng = ThreadLocalRandom.current();
        for (int i = ar.length - 1; i > 0; i--)
        {
            int index = prng.nextInt(i + 1);
            // Simple swap
            T a = ar[index];
            ar[index] = ar[i];
            ar[i] = a;
        }
    }

    public static void shuffleArray(int[] ar)
    {
        Random prng = ThreadLocalRandom.current();
        for (int i = ar.length - 1; i > 0; i--)
        {
            int index = prng.nextInt(i + 1);
            // Simple swap
            int a = ar[index];
            ar[index] = ar[i];
            ar[i] = a;
        }
    }

    public static void makeLatestBest(File currentlyBestModel, File currentlyLatestModel) {
        try {
            log.info(String.format("Overwriting %s \nwith %s ...", currentlyBestModel.getCanonicalPath(), currentlyLatestModel.getCanonicalPath()));
            FileUtils.copyFile(currentlyLatestModel, currentlyBestModel);
            log.info("Overwriting complete");
            log.info(String.format("Deleting %s ...", currentlyLatestModel.getCanonicalPath()));

            if (currentlyLatestModel.delete()) {
                log.info("deleted");
            } else {
                log.error("Could not be deleted");
            }
        } catch (IOException e) {
            log.error(String.format("Could not Overwrite %s", currentlyBestModel.getAbsolutePath()));
            e.printStackTrace();
        }
    }

    public static File saveComputationGraph(ComputationGraph graph) throws IOException {
        String fileName = MyFileUtils.getTimeStampedFile(MODEL_BASE_NAME, MODEL_EXTENSION);
        String filePath = FilenameUtils.concat(MODELS_DIR, fileName);

        final File file = new File(filePath);

        graph.save(file, true);

        log.info(String.format("model has been saved in %s", file.getCanonicalPath()));

        return file;
    }

    @Nullable
    private static File getModelWth(String bestModelExtension) {
        final File directory = new File(MODELS_DIR);

        final Iterator<File> fileIterator = FileUtils.iterateFiles(directory, new String[]{bestModelExtension}, false);

        File file = null;

        if (fileIterator.hasNext()) {
            file = fileIterator.next();

            while (fileIterator.hasNext()) {
                File tmp = fileIterator.next();

                if (FileUtils.isFileNewer(tmp, file)) {
                    file = tmp;
                }
            }
        }

        return file;
    }

    public static File getLatestModel() {
        return getModelWth(MODEL_EXTENSION);
    }

    public static void zipData() {
        String zipFileName = MyFileUtils.getTimeStampedFile(DATA_BACKUP_DIR, ZIP_EXTENSION);
        String zipFilePath = FilenameUtils.concat(DATA_BACKUP_DIR, zipFileName);

        try {
            Path p = Files.createFile(Paths.get(zipFilePath));
            try (ZipOutputStream zs = new ZipOutputStream(Files.newOutputStream(p))) {
                Iterator<File> fileIterator = FileUtils.iterateFiles(new File(DATA_DIR), new String[]{GAME_DATA_EXTENSION}, false);

                while (fileIterator.hasNext()) {
                    final File next = fileIterator.next();
                    String filename = next.getName();
                    ZipEntry ze = new ZipEntry(filename);
                    zs.putNextEntry(ze);
                    Files.copy(next.toPath(), zs);

                    zs.closeEntry();
                }
            }

            Iterator<File> fileIterator = FileUtils.iterateFiles(new File(DATA_DIR), new String[]{GAME_DATA_EXTENSION}, false);

            while (fileIterator.hasNext()) {
                fileIterator.next().delete();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
