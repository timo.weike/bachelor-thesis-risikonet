# RisikoNet

Run `mvn package` to build the program. 
This produces 10 shell scripts in the folder `target/appassembler/bin/`:

 * `createData` - runs 50 games between the designed AI and produces training data
 * `createSingleTowerModel` - creates a model where the heads share a single tower
 * `createThreeTowerModel` - creates a model where each head has its own tower
 * `measureAvgActionCount` - runs a measurement of the average action count for 10,000 games
 * `measureTime` - runs a measurement for the average time of an action, for multiple configuration
 * `modelEvaluationGame` - runs the first part of the evaluation-iteration of the trainings-pipeline
 * `play` - lets you play a game
 * `selfPlayIteration` - runs the self-play-iteration of the trainings-pipeline
 * `testGame` - runs the second part of the evaluation-iteration
 * `trainingIteration` - runs the trainings-main-iteration

## Dependencies

You need to install the OpenJDK-8 and Maven to build this program.