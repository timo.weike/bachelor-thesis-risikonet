\label{chap:basics}
Um ein System zu trainieren, welches das Brettspiel Risiko spielen kann, werden in dieser Arbeit vor allem zwei Ansätze verwendet. 
Einmal das Deep Learning als Methode für den Aufbau, die Auswertung und das Lernen von Funktionen, zum anderem das Reinforcement Learning als Methode, einen Agenten gewünschtes Verhalten erlernen zu lassen.
Im Folgenden wird auf die Grundlagen dieser beiden Methoden eingegangen.

\section{Deep Learning}

Deep Learning erlaubt es Modellen, die aus mehreren Verarbeitungsschichten bestehen, Repräsentationen für Daten lernen zu lassen. 
Deep Learning hat zu starken Verbesserungen in verschiedenen Gebieten geführt: unter anderem Spracherkennung und visuelle Objekterkennung \cite{lecun_deep_2015}.

Deep Learning macht Gebrauch von neuronalen Netzen, diese sind inspiriert von Netzwerken aus biologischen Neuronen.
Dabei besteht ein Deep-Neural-Network aus in Schichten organisierten \textit{\glspl{Unit}}.
Eine \gls{Unit} ist dabei über gewichtete Kanten mit den \glspl{Unit} aus der davor liegenden Schicht verbunden.
Diese Kanten sind in der biologischen Analogie die Axone und Dendriten von Neuronen.
Der Ausgabewert einer \gls{Unit} $u$ wird dabei als eine gewichtete Summe über die Ausgabewerte der verbundenen \glspl{Unit} bestimmt und auf diese Summe wird anschließend eine nichtlineare Aktivierungsfunktion $f$ angewendet.
Sei $h^{\eK{l}}_u$ der Ausgabewert der \gls{Unit} $u$ in der Schicht $l$ und sei $w^{\eK{l}}_{i,u}$ das Gewicht der Verbindung von der \gls{Unit} $i$ zur \gls{Unit} $u$ in der Schicht $l$, dann ist $h^{\eK{l}}_u$:
\begin{align}
 h^{\eK{l}}_u = f\rK{\sum_i w^{\eK{l}}_{i,u} \cdot h^{\eK{l-1}}_i}
\end{align}
Dabei ist die ReLU-Funktion (rectified linear unit) eine sehr beliebte Wahl für $f$.
\begin{align}
 \text{relu}\rK{x} = \max\rK{0,x}
\end{align}
Weitere in der Arbeit verwendete Aktivierungsfunktionen sind:
\begin{itemize}
 \item $\tanh\rK{x}$
 \item Softmax: $\sigma_i\rK{\hat{h}^{\eK{l}}} = \dfrac{\exp\rK{h_i^{\eK{l}}}}{\sum_{j \in U} \exp\rK{h_j^{\eK{l}}} }$, dabei ist $\hat{h}^{\eK{l}}$ ein Vektor über die Ausgaben aller \glspl{Unit} der Schicht $l$, $U$ ist die Menge aller \glspl{Unit} in der Schicht $l$ und $\sigma_i\rK{\hat{h}^{\eK{l}}}$ ist die Aktivierung der \gls{Unit} $i$.
 Die Softmax-Aktivierungsfunktion wandelt die nicht normalisierten Ausgabewerte der Schicht $l$ zu einer \gls{probDist} um, wobei dann $\sum_{i \in U} \sigma_i\rK{\hat{h}^{\eK{l}}} = 1$ ist.
\end{itemize}

Während des Trainings werden die Gewichte $w^{\eK{l}}_{i,u}$ mittels Backprogagation aktualisiert.
Sei nun $\hat{x} \in \R^{n}$ ein Eingabedatenpunkt und $\hat{y} \in \R^m$ der zugehörige Ausgabedatenpunkt.
Wird nun das neuronale Netz auf $\hat{x}$ angewendet, soll $\bar{y}$ die erzeugte Ausgabe sein.
Mit $\hat{y}$ und $\bar{y}$ wird dann ein Fehlerwert $E$ bestimmt und aus diesem wird ein \gls{loss} (engl. loss) $L$ bestimmt. 
Mittels der Kettenregel werden für alle Gewichte $w^{\eK{l}}_{i,u}$ Aktualisierungswerte $\Delta w^{\eK{l}}_{i,u}$ aus $L$ bestimmt, die genutzt werden, um die Gewichte zu aktualisieren.

In der Arbeit verwendete \gls{loss}-Funktionen sind:
\begin{itemize}
 \item MSE (Mean-Squared-Error): 
 \[
    \text{MSE} = \frac{1}{n} \sum_{i=1}^n \rK{\hat{y}_i - \bar{y}_i}^2 
 \]
 wobei $n$ die Anzahl an Ausgabe-\glspl{Unit} in der letzten Schicht des Netzwerkes ist.
 \item Cross-Entropy-Error:
 \[
   \text{CEE} = \sum_{i=1}^n \hat{y}_i \cdot \ln\rK{\bar{y}_i}
 \]
\end{itemize}


Um zu verhindern, dass die Gewichte beim Backpropagieren zu große Werte annehmen, werden Regularisierungswerte zum \gls{loss} hinzugenommen.
Eine in dieser Arbeit genutzte Regularisierung ist die L2-Regularisierung
\marginnote{$\mathbf{W}$ ist dabei die Menge aller Gewichte }
\begin{align}
 R\rK{\mathbf{W}} = \sum_{w \in \mathbf{W}} w^2
\end{align}
welche eine Summe der Quadrate aller Gewichte im Netzwerk ist.
Diese wird dann einfach zum Fehler $E$ addiert
\begin{align}
 L = E + R\rK{\mathbf{W}}
\end{align}

\subsection{Convolution}

Eine Ergänzung zu Deep-Neural-Networks sind Convolution- und Pooling-Schichten.
Diese Typen von Schichten sind entworfen worden, um Daten mit lokalen Abhängigkeiten effizienter zu verarbeiten. 
Dabei sind Convolution- und Pooling-Schichten von den neuronalen Strukturen im visuellen Kortex von Tieren inspiriert.
Eine Convolution-Schicht besteht dabei aus einer Menge von Feature-Maps.
Die \glspl*{Unit} einer Feature-Map sind dabei mit einer lokalen Nachbarschaft in der vorgehenden Schicht verbunden, die Gewichte für die Verbindungen werden dabei von allen \glspl*{Unit} einer Feature-Map geteilt.
\begin{figure}
  \centering
  \includegraphics[width=10cm]{convolution-image.png}
  \caption{
    Abbildung einer Convolution-Schicht. \cite{briliant.org_convolutional_nodate} 
  }  
\end{figure}
Eine Pooling-Schicht berechnet dann für lokale Bereiche in diesen Feature-Maps einen neuen Wert.
% --- --- ---
Beispielsweise wird beim Max-Pooling in einer zweidimensionalen Feature-Map für jeden Teilbereich das Maximum bestimmt und als Ausgabewert verwendet.
% --- --- ---
% Beispielsweise wird in einer Zweidimensionalen Feature-Map beim Max-Pooling für Teilbereiche einer gewissen Dimension (die als Hyperparameter spezifiziert wird) das Maximum der Werte in diesem Teilbereich ausgegeben.
Dies wird für jede mögliche Position in der Feature-Map bestimmt und bildet eine neue Feature-Map, diese ist in der Regel von einer kleineren Dimension. \cite{lecun_deep_2015}

\subsubsection{Batchnormalisierung}

Zudem wird in dieser Arbeit Batchnormalisierung hinter die Convolution geschaltet.
Batchnormalisierung wird in Verbindung mit Mini-Batch-Learning verwendent.
Beim Mini-Batch-Learning wird nicht das gesamte Datenset pro Trainingsiteration verwendet sondern nur Teile davon. 
Das Ziel der Batchnormalisierung ist die Eingabe der nächsten Schicht (\bzw die Ausgabe der vorherigen Schicht) auf die Normalverteilung zu normalisieren.
Die Normalisierung findet dabei pro \gls{Unit} statt.
\cite{ioffe_batch_2015}

\subsection{Residual Learning}

Eine weitere Ergänzung sind Residual-Blöcke.
Diese bestehen aus zwei oder mehr Schichten, wobei der Input $\hat{x}$ des Residual-Blocks eine Skip-Verbindung zum Output des Residual-Blocks hat.
Sei $\mathcal{F}\rK{\hat{x}}$ die Abbildung modelliert von den Schichten des Residual-Blocks, dann ist der Output des Blocks definiert durch:
\marginnote{Dabei ist $f$ in der Regel die letzte nicht-lineare Funktion des Blockes, die auf das Endergebnis angewendet wird}
\begin{align}
 \mathcal{H}\rK{\hat{x}} = f\rK{\mathcal{F}\rK{\hat{x}} + \hat{x}} \label{eqn:resBlock}
\end{align}
Wie man in \ref{eqn:resBlock} sieht, ist die Skip-Verbindung lediglich eine Identitätsfunktion, die komponentenweise mit der Ausgabe des Blocks addiert wird.

\begin{figure}
  \centering 
  \begin{tikzpicture}[node distance = 0.6cm, auto, scale=0.8]
    \usetikzlibrary{fit}
    \node (in1) { };
    \node[draw, below=of in1] (in2) {Eingabe};
    \node[draw, below=of in2] (layer1) {Schicht 1};
    \node[below=of layer1] (dots) {$\ldots$};
    \node[draw, below=of dots] (layern) {Schicht $n$};
    \node[draw, circle, below=of layern] (add) {$+$};
    \node[below=of add] (out) {};

    \node[left=of add] (eqn) {$\mathcal{F}\rK{\hat{x}} + \hat{x}$}; 
    \node[draw, fit=(layer1) (layern) ] (box) {};
    \node[left=of box] (f) {$\mathcal{F}\rK{\hat{x}}$};

    \path[->]
      (in1) edge (in2)
      (in2) edge node[left] {$\hat{x}$} (layer1)
      (layer1) edge node[right] {$f_1$} (dots)
      (dots) edge node[right] {$f_{n-1}$} (layern)
      (layern) edge (add)
      (add) edge node[right] {$f_n$} (out)
      (in2) edge[bend left=90] node [right] {Identität $\hat{x}$} (add);
  \end{tikzpicture}
  \caption{
    Dieses Diagramm zeigt einen Residual-Block bestehend aus $n$ Schichten. Dabei ist jeweils $f_k$ die nicht-lineare Funktion, die zur Schicht $k$ gehört. Zu beachten ist hier, dass $f_n$ hinter der Komposition mit der Skip-Verbindung liegt. In dieser Arbeit werden Residual-Blöcke verwendet, bei denen eine Schicht übersprungen wird. (Nachbildung eines Diagramms aus \cite{he_deep_2016})
  }
\end{figure}
Diese Skip-Verbindungen fügen weder Parameter zum Modell hinzu noch erhöhen sie die Rechenkomplexität \cite{he_deep_2016}.
Derartige Residual-Blöcke ermöglichen es, Netzwerke mit einer größeren Anzahl von Schichten zu entwerfen, die nicht nur das \textit{Degradation}-Problem umgehen, sondern auch eine höhere Genauigkeit (engl. accuracy) erzielen \cite{he_deep_2016}.

\section{Reinforcement Learning}
\label{chap:basics-RL}

Reinforcement Learning ist das Lernen von Verhaltensweisen, also das Lernen einer Abbildung zwischen Situationen und Aktionen.
% gucken wie man in latex deutsche Fälle im glossar behandeln kann
Dabei wird dem \glsmgen{Agent} nicht vorgeschrieben, welche Aktionen positiv sind, sondern der Agent soll selbst herausfinden, welche Aktionen ihm die meiste \textit{\gls{reward}} bringen.
Die Aktionen, die der Agent dabei ausführt, können dabei nicht nur die direkte \gls{reward} beeinflussen, sondern auch die folgenden Zustände der Umgebung und damit auch die zukünftigen \glspl{reward}.
Dies bildet die zwei wichtigsten Charakteristiken von Reinforcement-Learning\cite{sutton_reinforcement_2018}:
\begin{itemize}
 \item Trial-and-error-Suche
 \item verzögerte \gls{reward}
\end{itemize}

Damit Reinforcement-Learning auf ein Problem angewendet werden kann, muss es einige Anforderungen erfüllen.
Das Problem benötigt eine Umwelt, in der ein Agent Aktionen durchführen kann. 
Dieser Agent benötigt ein oder mehrere Ziele, die mit dem Zustand der Umgebung in Verbindung stehen. 
Zudem muss der Agent die Umgebung über \textit{Sensoren} wahrnehmen können (dies kann entweder vollkommen oder partiell sein).
Wenn diese Anforderungen erfüllt sind, kann das Problem durch einen \textit{Markov-Decision-Process} formalisiert werden.
Dazu werden der Zustand, die Aktionen und die Ziele \bzw das Ziel in den Markov-Decision-Process mit einbezogen. \cite{sutton_reinforcement_2018}

Neben der Umgebung und dem Agenten gibt es vier weitere grundlegende Bestandteile von Reinforcement-Learning: eine \textit{\gls{policy}}, ein \textit{\glswiths{reward}-Signal}, eine \textit{\gls{valueFunction}} und optional ein Modell der Umgebung \cite{sutton_reinforcement_2018}.
Im Folgenden wird auf die ersten drei genauer eingegangen.

Die \gls{policy} definiert das Verhalten eines Agenten in einem bestimmten Zustand der Umgebung.
Im weiteren Sinne ist die \gls{policy} eine Abbildung eines Zustandes zu einer Aktion, die der Agent durchführen soll.
Je nach dem was das konkrete Problem ist, ist die \gls{policy} eine einfache Funkion, die mittels einer Wertetabelle implementiert ist, oder sie bildet einen Zustand auf eine \gls{probDist} über alle möglichen Aktionen ab.\cite{sutton_reinforcement_2018}

Über das \glswiths{reward}-Signal wird das Ziel des Agenten definiert.
Nach jeder Aktion schickt die Umgebung einen skalaren Wert $\reward$ zum Agenten, dies ist die \gls{reward}.
Die Aufgabe des Agenten ist dann lediglich, die Summe über alle erhaltenen \glspl{reward} zu maximieren.
Somit werden gute ($\reward > 0$) und schlechte ($\reward < 0$) Aktionen über die \gls{reward} definiert.
Ein biologische Analogie für die \gls{reward} sind Schmerz und Freude, die Lebewesen empfinden.
Das \glswiths{reward}-Signal ist die primäre Quelle für Anpassungen der \gls{policy}, wenn die \gls{policy} eine Aktion auswählt, die eine geringe \gls{reward} bringt kann die \gls{policy} dahingehend angepasst werden, dass im selben Zustand nicht nochmal die gleiche Aktion ausgewählt wird (\bzw die \gls{prob} sinkt, dass die gleiche Aktion erneut gewählt wird).
Generell ist das \glswiths{reward}-Signal eine Funktion, abhängig von dem Zustand und der gewählten Aktion. \cite{sutton_reinforcement_2018}

Die \gls{valueFunction} ist eine Bewertungs-Funktion für einen Zustand.
Im Gegensatz zur \gls{reward}, die eine direkte Bewertung einer Aktion ermöglicht, gibt die \gls{value} an, welche Zustände insgesamt zu einem guten (End)-Ergebnis führen.
Dabei ist die \gls{value} von einem Zustand die Summe der zu erwartenden \glspl{reward} für die Folgezustände des aktuellen Zustandes. \cite{sutton_reinforcement_2018}

Obwohl die \gls{value} eine Vorhersage für zukünftige \glspl{reward} ist, spielt die \gls{valueFunction} für das Reinforcement-Learning eine wichtigere Rolle als die \gls{reward}. 
Der Agent soll die Aktionen ausführen, die die höchste \gls{value} besitzen (also langfristig die größte Summe von \glspl{reward} bringen) und nicht die Aktion, die aktuell die größte \gls{reward} bringt.
Daher ist das Vorhersagen der \gls{value} eines Zustandes die zentrale Aufgabe eines Reinforcement-Learning-Algorithmus.\cite{sutton_reinforcement_2018}

\subsection{Deep Reinforcement-Learning}

Ein mögliche Verknüpfung von Deep Learning und Reinforcement-Learning ist es, die \gls{policy} sowie die \gls{valueFunction} als neuronale Netze zu modellieren.
Dabei wird dann im Trainingsprozess ein Fehlersignal für die \gls{value} \bzw die vorhergesagte \gls{probDist} erzeugt, um die Gewichte der Netzwerke anzupassen und so die optimale \gls{policy} und \gls{valueFunction} zu lernen \cite{silver_mastering_2017}.











