#/bin/python3

import numpy as np
import matplotlib.pyplot as plt

fileName = "actionCountData.dat"

data = np.loadtxt(fileName)

mean = np.mean(data)
std = np.std(data)
print("mean=%g, std=%g" % (mean,std))

plt.hist(data, int(np.sqrt(len(data))))
plt.show()
