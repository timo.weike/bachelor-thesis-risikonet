# Creating the PDF

Run the command `make` in this directory to create the PDF-file for this thesis. If you want a printfriendly version uncomment the command 
```
\hypersetup{hidelinks}
```
in the file thesis.tex and run `make`.