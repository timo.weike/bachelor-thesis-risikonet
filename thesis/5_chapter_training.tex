\label{chap:training} 

Ziel des Trainings ist es, die \glspl{policy} $\policyAttack$ und $\policyReinforcement$ zu optimieren, sodass ein Agent unter Verwendung der \glspl{policy} das Spiel Risiko spielen kann.
Dabei wird ebenfalls die \gls{valueFunction} optimiert und während des Trainings verwendet, um die Ausgaben der \glspl{policy} $\policyAttack$ und $\policyReinforcement$ zu verbessern.

Für das Training wurde eine Trainingspipeline entworfen, die aus 3 Teilen besteht:
\begin{itemize}
 \item Self-Play-Iteration
 \item Trainings-Hauptiteration
 \item Evaluierungs-Iteration
\end{itemize}
In der Self-Play-Iteration spielt das bis dato beste Netzwerk gegen sich selbst, um Daten über Aktionen, die zu einer guten \bzw schlechten Spielbrettkonfiguration führen, zu generieren.
In der Trainings-Hauptiteration werden anschließend die Daten, die in der Self-Play-Iteration generiert wurden, verwendet, um aus dem bis dato besten Netzwerk eine neue Version zu erstellen.
In der Evaluierungs-Iteration wird letztlich das in der Trainings-Hauptiteration neu erzeugte Netzwerk gegen das bis dato beste Netzwerk evaluiert und falls es besser ist für den nächsten Durchlauf der Pipeline als das neue beste Netzwerk verwendet.

\section{Daten durch Self-Play}

Im Folgenden wird erläutert, wie die Trainingsdaten für die Trainings-Hauptiteration erzeugt werden.

In jeder Self-Play-Iteration werden fünf Spiele gespielt, bis entweder ein Spieler gewinnt oder die maximale Zuganzahl erreicht ist. 
Während dieser Spiele muss jeder Spieler viele Male entscheiden, welche Aktion ausgeführt werden soll.
Dazu wird eine Variation der Monte-Carlo-Tree-Search (MCTS), die Silver \etal für AlphaGo Zero verwendet haben, benutzt \cite{silver_mastering_2017}.
Die MCTS kann als ein Operator für die Verbesserung der \glspl{policy} angesehen werden \cite{silver_mastering_2017}.

Für die MCTS wird ein Baum aufgebaut. 
Dabei beinhalten die Knoten einen Spielbrettzustand $\state$ und Kanten $\eK{\state, a}$ für alle legalen Aktionen $a \in \actionSet\rK{\state}$.
Wenn $a$ eine Angriffsaktion ist, verbindet die Kante $\eK{\state, a}$ den Zustand $\state$ mit beiden möglichen Folgezuständen, wenn $a$ eine Verstärkungsaktion ist, verbindet die Kante den Zustand mit dem einzigen Folgezustand.
Sei folgend $a_i\rK{\state}$ der $i$-te mögliche Folgezustand für den Zustand $\state$ unter der Aktion $a$, wobei $n$ die Anzahl der möglichen Folgezustände ist.
Dann führt jede  Kante zusätzlich eine Statistik
\[
 \eK{N_1\rK{\state,a}, W_1\rK{\state,a}, \ldots, N_n\rK{\state,a}, W_n\rK{\state,a}, P\rK{\state,a}}
\]
Dabei ist: 
\begin{itemize}
 \item $N_i\rK{\state,a}$ ein Besuchszähler für eine Bewegung vom Zustand $\state$ über die Kante $\eK{\state,a}$ zu dem Folgezustand $a_i\rK{\state}$
 \item $W_i\rK{\state,a}$ die Summe der \gls{value} aller Kindknoten von $a_i\rK{\state}$ plus der \gls{value} $\mvalue\rK{a_i\rK{\state}}$, genannt Aktionsgüte
 \item $P\rK{\state,a}$ die Priorwahrscheinlichkeit, dass die Kante $\eK{\state, a}$ während der Suche ausgewählt wird
\end{itemize}

Aus dieser Statistik können dann
\begin{align}
  N\rK{\state, a} &= \sum_{i=1}^n N_i\rK{\state,a} \\
  W\rK{\state, a} &= \sum_{i=1}^n W_i\rK{\state,a} \\
  Q_i\rK{\state, a} &= \frac{W_i\rK{\state, a}}{N_i\rK{\state, a}} \\
  Q\rK{\state, a} &= \frac{W\rK{\state, a}}{N\rK{\state, a}} 
                   = \sum_{i=1}^n \underbrace{\frac{N_i\rK{\state, a}}{N\rK{\state, a}}}_{\text{\gls{prob} das $a$ zu $a_i\rK{s}$ führt}} \cdot Q_i\rK{\state, a}\\
                  &= \sum_{i=1}^n \frac{W_i\rK{\state, a}}{N\rK{\state, a}} \nonumber
                   = \frac{1}{N\rK{\state, a}} \sum_{i=1}^n W_i\rK{\state, a}
\end{align}
berechnet werden, wobei $Q_i\rK{\state, a}$ die mittlere Aktionsgüte für den Folgezustand $a_i\rK{\state}$ ist und $Q\rK{\state, a}$ die mittlere Aktionsgüte für alle Folgezustände von $s$ und der Aktion $a$.

Wenn nun ein Agent eine Aktion wählen muss, werden parallel 100 Simulationsspiele mit Hilfe des oben beschriebenen Baumes durchgeführt.
Dabei startet jede Simulation in der Wurzel des Baumes, dabei ist die Wurzel der momentane Zustand des Spielbrettes.
Die Simulation bewegt sich dann im Baum nach unten bis ein Blattknoten $\state_L$ zum Zeitpunkt $L$ erreicht wird.
Sei dabei 
\[
\text{seq} =\state_0, a^0, \state_1, a^1, \ldots, \state_{L-1}, a^{L-1}, \state_L
\] 
die Sequenz von Zuständen und Aktionen, die zu dem Zustand $\state_L$ führen.
Hierbei ist ein Blattknoten ein Knoten, der noch nie von einer Simulation besucht wurde.

Dabei wird zu jedem Zeitpunkt $\timePoint < L$ die Aktion $a^t$ mithilfe der Statistik des Baumes über folgende Formel bestimmt
\begin{align}
  a^\timePoint = \argmax_{a}\rK{Q\rK{\state_\timePoint} + U\rK{\state_\timePoint, a}}
\end{align}
wobei $U$ die Variante des PUCT-Algorithmus ist, die von Silver \etal für AlphaGo (Zero) verwendet wurde \cite{silver_mastering_2017, silver_mastering_2016}
\begin{align}
  U\rK{\state_\timePoint, a} = c_{\text{puct}} P\rK{\state_\timePoint, a} 
  \frac
  {\sqrt{\sum_bN\rK{\state_\timePoint, b}}}
  {1 + N\rK{\state_\timePoint, a}}
\end{align}
Dabei wurde in dieser Arbeit $c_{\text{puct}} = 1$ verwendet.

Der Zustand $\state_L$ wird dann in eine Warteschlange zur Evaluierung durch das neuronale Netz eingetragen und die Statistiken für die Kanten $\eK{\state_L, a}$ werden initialisiert.
Dieser Vorgang wird als Expandieren bezeichnet.
\marginnote{
  Die Zustände in der Warteschlange werden dann in Batches vom Netzwerk evaluiert. Dabei werden für jeden Batch alle in der Warteschlange befindlichen Zustände gleichzeitig evaluiert. 
}

Sei dabei $\mathrm{v} = \mvalue\rK{\state_L}$ die Güte, die das Netzwerk für $\state_L$ vorhersagt und sei $\mathbf{p}$ die kombinierte \gls{probDist} der beiden \glspl{policy} $\policyAttack\rK{\state_L}$ und $\policyReinforcement\rK{\state_L}$.
Dann werden die Kanten $\eK{\state_L, a}$ wie folgt initialisiert:
\begin{align}
  N_i\rK{\state_L, a} &= 0 \forall i \\
  W_i\rK{\state_L, a} &= 0 \forall i \\
  P\rK{\state_L, a}   &= \mathbf{p}\rK{a}
\end{align}
Dabei ist $\mathbf{p}\rK{a}$ die \gls{prob} dafür, dass $a$ ausgeführt wird.

Nachdem der Knoten $\state_L$ expandiert wurde, werden die Statistiken aller $\state_t$ aus der Sequenz $\text{seq}$ mit $\timePoint < L$ aktualisiert, dabei werden die Besuchszähler $N_i\rK{\state_\timePoint, a^\timePoint}$ um eins erhöht und die \gls{value} $\mathrm{v}$ wird zu $W_i\rK{\state_\timePoint, a^\timePoint}$ hinzu addiert, also:
\begin{align}
  N_i\rK{\state_\timePoint, a^\timePoint} &= N_i\rK{\state_\timePoint, a^\timePoint} + 1 \\
  W_i\rK{\state_\timePoint, a^\timePoint} &= W_i\rK{\state_\timePoint, a^\timePoint} + \mathrm{v}
\end{align}
wobei $i = \arg_{i}\rK{a^t_i(\state_\timePoint) = \state_{\timePoint+1}}$ ist.

Nachdem alle 100 Simulationen fertig sind, wird eine verbesserte \gls{probDist} $\pi\rK{a|\state_0}$ über alle Aktionen gebildet, die in der aktuellen Phase gespielt werden können.
Dabei ist
\begin{align}
  \pi\rK{a|\state_0} = \ddfrac{N\rK{\state_0, a}^{\nicefrac{1}{\tau}}}{\sum_b N\rK{\state_0, b}^{\nicefrac{1}{\tau}}}
\end{align}
wobei $\tau$ ein Temperatur-Parameter ist. Dieser ist am Anfang jedes Spiels $\tau = 1$ und wird nach dem dreißigsten Zug auf $\tau = 0.01$ gesetzt.

Der Baum wird bei der nächsten Auswahl einer Aktion wiederverwendet. 
Dazu wird der Kindknoten, der zu dem aus der ausgewählten Aktion resultierenden Zustand gehört, zur neuen Wurzel des Baumes gemacht.

Die so erzeugte verbesserte \gls{probDist} $\pi$ wird zusammen mit dem Zustand $\state_0$ als Trainings-Datenpunkt abgespeichert. Sobald das Spiel beendet ist, werden alle so gesammelten Datenpunkte um den Wert der \gls{reward} $\reward$ erweitert. 
Dieser wird, wie in \autoref{eqn:reward_function} beschrieben, für den Endzustand $\state_E$ und den Spieler $\playerId$, der die Aktion durchgeführt hat, berechnet.
Damit haben wir dann nach dem Spiel einen Datensatz bestehend aus Datenpunkten
\[
 \qK{\state_t, \pi\rK{. | \state_t}, \reward_\playerId}
\]
für jede Aktion, die ein Spieler während des Spiels ausgeführt hat.

Nachdem die fünf Spiele der Self-Play-Iteration beendet sind und alle Daten gesammelt wurden, werden die Daten verwendet, um das neuronale Netz zu aktualisieren.

\begin{figure}
  \centering
  \begin{tikzpicture}[node distance = 1cm, auto, scale=1]
    \node[draw, align=center] (sp) {
      Self-Play-Iteration: \\
      5 Spiele
    };
    \node[draw, below=of sp, align=center] (ti) {
      Trainings-Hauptiteration
    };
    \node[draw, below=of ti, align=center] (eva) {
      Evaluierungs-Iteration: \\
      100 Evaluationsspiele 
    };
    
    \path[->]
      (sp) edge (ti) 
      (ti) edge (eva) 
      (eva) edge[bend right=90] (sp);
  \end{tikzpicture}
  \caption{
    Dieses Diagramm zeigt den Aufbau der Trainingspipeline.
  }
\end{figure} 

\subsection{Evaluierung}

In der Evaluierungs-Iteration wird geprüft, ob das in der Trainings-Hauptiteration neu erstellte Netzwerk eine bessere Performance besitzt als das bis dato beste Netzwerk.
Dazu spielen die beiden Netzwerke 100 Spiele gegeneinander.
Dabei nutzen je zwei der \glspl{Agent} das eine \bzw das andere Netzwerk, um Aktionen auszuwählen.
Welcher der vier Spieler welches Netzwerk verwendet wird für jedes Spiel zufällig bestimmt.
Wenn dabei die Spieler, die das neue Netzwerk verwenden, 50 oder mehr der Spiele gewinnen, wird das neue Netzwerk zum neuen Besten erklärt.

Zusätzlich wird die Korrektheit der Vorhersagen der \glspl{policy} evaluiert. Denn da das Netzwerk zu jedem Zeitpunkt eine \gls{probDist} über alle denkbaren Aktionen erzeugt, können auch nicht legale Aktionen eine echt positive \gls{prob} haben. Dafür werden zwei Dinge bestimmt:
\begin{itemize}
 \item die Aktion mit der höchsten \gls{prob}
 \item die Summe der \gls{prob} aller legalen Aktionen
\end{itemize}
Diese wird für beide Netzwerke und beide Zugphasen bestimmt.
Dazu werden 1000 zufällige Spielbretter erzeugt und von beiden \glspl{policy} der beiden Netzwerke evaluiert.

Nachdem feststeht, welches das neue beste Netzwerk ist, werden zudem 200 weitere Spiele gespielt.
Bei diesen Spielen nutzt je einer der \glspl{Agent} das Netzwerk für die Aktionswahl.
In den ersten 100 der 200 Spielen wählen die verbleibenden drei \glspl{Agent} ihre Aktionen zufällig aus.
In den restlichen 100 Spielen nutzen die drei verbleibenden \glspl{Agent} eine händisch erstellte KI für die Aktionswahl.
\begin{itemize}
 \item 100 Spiele gegen drei \glspl{Agent}, die zufällig spielen
 \item 100 Spiele gegen drei \glspl{Agent}, die intelligent spielen 
\end{itemize}

\begin{table}
  \centering
  \caption{Übersichtstabelle mit den Werten für die Hyperparameter der Trainingspipeline}
  \begin{tabular}{ll}
    Hyperparameter & Wert \\
    \hline
    Self-Play-Spiele & 5 \\
    Evaluationsspiele & 100 \\
    Spielzustände für Korrektheitsevaluation & 1000 \\
    Spiele gegen zufälligen Agent & 100 \\
    Spiele gegen intelligenten Agent & 100 \\
    \hline
  \end{tabular}
\end{table}







